package fr.khelan.commons.math.matrix.impl;

import fr.khelan.commons.math.matrix.Matrix;
import fr.khelan.commons.math.matrix.exception.SingularMatrixException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DoubleMatrixTest {
	private static final Double PRECISION = 0.000001;
	private final DoubleMatrix squareMatrix3 = new DoubleMatrix(//
																new double[] { -1.0, 2.0, -1.0 }, //
																new double[] { 0.0, -1.0, 2.0 }, //
																new double[] { 2.0, -1.0, 0.0 });
	private DoubleMatrix matrix1, matrix2, matrix3, squareMatrix, squareMatrix2;

	@BeforeEach
	void init() {
		matrix1 = new DoubleMatrix(4, 3);
		matrix1.set(1.0, 0, 0);
		matrix1.set(2.0, 0, 1);
		matrix1.set(3.0, 0, 2);
		matrix1.set(4.0, 1, 0);
		matrix1.set(5.0, 1, 1);
		matrix1.set(6.0, 1, 2);
		matrix1.set(7.0, 2, 0);
		matrix1.set(8.0, 2, 1);
		matrix1.set(9.0, 2, 2);
		matrix1.set(10.0, 3, 0);
		matrix1.set(11.0, 3, 1);
		matrix1.set(12.0, 3, 2);

		matrix2 = new DoubleMatrix(4, 3);
		matrix2.set(21.0, 0, 0);
		matrix2.set(23.0, 0, 1);
		matrix2.set(25.0, 0, 2);
		matrix2.set(27.0, 1, 0);
		matrix2.set(29.0, 1, 1);
		matrix2.set(31.0, 1, 2);
		matrix2.set(33.0, 2, 0);
		matrix2.set(35.0, 2, 1);
		matrix2.set(37.0, 2, 2);
		matrix2.set(39.0, 3, 0);
		matrix2.set(41.0, 3, 1);
		matrix2.set(43.0, 3, 2);

		matrix3 = new DoubleMatrix(//
								   new double[] { 1.0, -1.0, 2.0, 5.0 }, //
								   new double[] { 3.0, 2.0, 1.0, 10.0 }, //
								   new double[] { 2.0, -3.0, -2.0, -10.0 });

		squareMatrix = new DoubleMatrix(3, 3);
		squareMatrix.set(99.0, 0, 0);
		squareMatrix.set(88.0, 0, 1);
		squareMatrix.set(77.0, 0, 2);
		squareMatrix.set(66.0, 1, 0);
		squareMatrix.set(55.0, 1, 1);
		squareMatrix.set(44.0, 1, 2);
		squareMatrix.set(33.0, 2, 0);
		squareMatrix.set(22.0, 2, 1);
		squareMatrix.set(11.0, 2, 2);

		squareMatrix2 = new DoubleMatrix(4, 4);
		squareMatrix2.set(4.0, 0, 0);
		squareMatrix2.set(4.0, 0, 1);
		squareMatrix2.set(2.0, 0, 2);
		squareMatrix2.set(1.0, 0, 3);
		squareMatrix2.set(7.0, 1, 0);
		squareMatrix2.set(5.0, 1, 1);
		squareMatrix2.set(3.0, 1, 2);
		squareMatrix2.set(1.0, 1, 3);
		squareMatrix2.set(2.0, 2, 0);
		squareMatrix2.set(1.0, 2, 1);
		squareMatrix2.set(2.0, 2, 2);
		squareMatrix2.set(3.0, 2, 3);
		squareMatrix2.set(1.0, 3, 0);
		squareMatrix2.set(3.0, 3, 1);
		squareMatrix2.set(2.0, 3, 2);
		squareMatrix2.set(1.0, 3, 3);
	}

	@Test
	void getSetMatrixTest() {
		Assertions.assertEquals(Integer.valueOf(4), matrix1.getRowNumber());
		Assertions.assertEquals(Integer.valueOf(3), matrix1.getColumnNumber());
		Assertions.assertEquals(7.0, matrix1.get(2, 0), PRECISION);
	}

	@Test
	void addScalarTest() {
		final Matrix<Double> result = matrix1.add(11.0);

		Assertions.assertEquals(3.0, matrix1.get(0, 2), PRECISION);

		Assertions.assertEquals(14.0, result.get(0, 2), PRECISION);
		Assertions.assertEquals(16.0, result.get(1, 1), PRECISION);
		Assertions.assertEquals(17.0, result.get(1, 2), PRECISION);
		Assertions.assertEquals(22.0, result.get(3, 1), PRECISION);
		Assertions.assertEquals(23.0, result.get(3, 2), PRECISION);
	}

	@Test
	void addTest() {
		final Matrix<Double> result = matrix1.add(matrix2);

		Assertions.assertEquals(3.0, matrix1.get(0, 2), PRECISION);
		Assertions.assertEquals(25.0, matrix2.get(0, 2), PRECISION);

		Assertions.assertEquals(28.0, result.get(0, 2), PRECISION);
		Assertions.assertEquals(34.0, result.get(1, 1), PRECISION);
		Assertions.assertEquals(37.0, result.get(1, 2), PRECISION);
		Assertions.assertEquals(52.0, result.get(3, 1), PRECISION);
		Assertions.assertEquals(55.0, result.get(3, 2), PRECISION);
	}

	@Test
	void subtractScalarTest() {
		final Matrix<Double> result = matrix1.subtract(8.0);

		Assertions.assertEquals(3.0, matrix1.get(0, 2), PRECISION);

		Assertions.assertEquals(-5.0, result.get(0, 2), PRECISION);
		Assertions.assertEquals(-3.0, result.get(1, 1), PRECISION);
		Assertions.assertEquals(-2.0, result.get(1, 2), PRECISION);
		Assertions.assertEquals(3.0, result.get(3, 1), PRECISION);
		Assertions.assertEquals(4.0, result.get(3, 2), PRECISION);
	}

	@Test
	void subtractTest() {
		final Matrix<Double> result = matrix1.subtract(matrix2);

		Assertions.assertEquals(3.0, matrix1.get(0, 2), PRECISION);
		Assertions.assertEquals(25.0, matrix2.get(0, 2), PRECISION);

		Assertions.assertEquals(-22.0, result.get(0, 2), PRECISION);
		Assertions.assertEquals(-24.0, result.get(1, 1), PRECISION);
		Assertions.assertEquals(-25.0, result.get(1, 2), PRECISION);
		Assertions.assertEquals(-30.0, result.get(3, 1), PRECISION);
		Assertions.assertEquals(-31.0, result.get(3, 2), PRECISION);
	}

	@Test
	void scalarProductTest() {
		final Matrix<Double> m1 = new DoubleMatrix(3, 1);
		m1.set(3.0, 0, 0);
		m1.set(5.0, 1, 0);
		m1.set(7.0, 2, 0);

		final Matrix<Double> m2 = new DoubleMatrix(1, 3);
		m2.set(2.0, 0, 0);
		m2.set(4.0, 0, 1);
		m2.set(6.0, 0, 2);

		Assertions.assertEquals(68.0, m2.scalarProduct(m1), PRECISION);
	}

	@Test
	void multiplyScalarTest() {
		final Matrix<Double> result = matrix1.multiply(5.0);

		Assertions.assertEquals(5.0, result.get(0, 0), PRECISION);
		Assertions.assertEquals(10.0, result.get(0, 1), PRECISION);
		Assertions.assertEquals(15.0, result.get(0, 2), PRECISION);
		Assertions.assertEquals(20.0, result.get(1, 0), PRECISION);
		Assertions.assertEquals(25.0, result.get(1, 1), PRECISION);
		Assertions.assertEquals(30.0, result.get(1, 2), PRECISION);
		Assertions.assertEquals(35.0, result.get(2, 0), PRECISION);
		Assertions.assertEquals(40.0, result.get(2, 1), PRECISION);
		Assertions.assertEquals(45.0, result.get(2, 2), PRECISION);
	}

	@Test
	void multiplyTest() {
		final Matrix<Double> result = matrix1.multiply(squareMatrix);

		Assertions.assertEquals(330.0, result.get(0, 0), PRECISION);
		Assertions.assertEquals(264.0, result.get(0, 1), PRECISION);
		Assertions.assertEquals(198.0, result.get(0, 2), PRECISION);
		Assertions.assertEquals(924.0, result.get(1, 0), PRECISION);
		Assertions.assertEquals(759.0, result.get(1, 1), PRECISION);
		Assertions.assertEquals(594.0, result.get(1, 2), PRECISION);
		Assertions.assertEquals(1518.0, result.get(2, 0), PRECISION);
		Assertions.assertEquals(1254.0, result.get(2, 1), PRECISION);
		Assertions.assertEquals(990.0, result.get(2, 2), PRECISION);
		Assertions.assertEquals(2112.0, result.get(3, 0), PRECISION);
		Assertions.assertEquals(1749.0, result.get(3, 1), PRECISION);
		Assertions.assertEquals(1386.0, result.get(3, 2), PRECISION);
	}

	@Test
	void transposeTest() {
		final Matrix<Double> result = matrix1.transpose();

		Assertions.assertEquals(1.0, result.get(0, 0), PRECISION);
		Assertions.assertEquals(4.0, result.get(0, 1), PRECISION);
		Assertions.assertEquals(7.0, result.get(0, 2), PRECISION);
		Assertions.assertEquals(10.0, result.get(0, 3), PRECISION);
		Assertions.assertEquals(2.0, result.get(1, 0), PRECISION);
		Assertions.assertEquals(5.0, result.get(1, 1), PRECISION);
		Assertions.assertEquals(8.0, result.get(1, 2), PRECISION);
		Assertions.assertEquals(11.0, result.get(1, 3), PRECISION);
		Assertions.assertEquals(3.0, result.get(2, 0), PRECISION);
		Assertions.assertEquals(6.0, result.get(2, 1), PRECISION);
		Assertions.assertEquals(9.0, result.get(2, 2), PRECISION);
		Assertions.assertEquals(12.0, result.get(2, 3), PRECISION);
	}

	@Test
	void determinantTest() {
		Assertions.assertEquals(1.0, DoubleMatrix.identityMatrix(2).determinant(), PRECISION);
		Assertions.assertEquals(1.0, DoubleMatrix.identityMatrix(3).determinant(), PRECISION);
		Assertions.assertEquals(1.0, DoubleMatrix.identityMatrix(4).determinant(), PRECISION);
		Assertions.assertEquals(1.0, DoubleMatrix.identityMatrix(5).determinant(), PRECISION);

		Assertions.assertEquals(0.0, squareMatrix.determinant(), PRECISION);

		Assertions.assertEquals(23.0, squareMatrix2.determinant(), PRECISION);

		Assertions.assertEquals(4.0, squareMatrix3.determinant(), PRECISION);
	}

	@Test
	void gaussianEliminationTest() {
		Matrix<Double> g = squareMatrix3.gaussianElimination();
		Assertions.assertEquals(1, g.get(0, 0), PRECISION);
		Assertions.assertEquals(1, g.get(1, 1), PRECISION);
		Assertions.assertEquals(1, g.get(2, 2), PRECISION);
		Assertions.assertEquals(0, g.get(0, 1), PRECISION);
		Assertions.assertEquals(0, g.get(0, 2), PRECISION);
		Assertions.assertEquals(0, g.get(1, 0), PRECISION);
		Assertions.assertEquals(0, g.get(1, 2), PRECISION);
		Assertions.assertEquals(0, g.get(2, 0), PRECISION);
		Assertions.assertEquals(0, g.get(2, 1), PRECISION);

		g = matrix3.gaussianElimination();
		Assertions.assertEquals(1.0, g.get(0, 0), PRECISION);
		Assertions.assertEquals(1.0, g.get(1, 1), PRECISION);
		Assertions.assertEquals(1.0, g.get(2, 2), PRECISION);
		Assertions.assertEquals(0.0, g.get(0, 1), PRECISION);
		Assertions.assertEquals(0.0, g.get(0, 2), PRECISION);
		Assertions.assertEquals(0.0, g.get(1, 0), PRECISION);
		Assertions.assertEquals(0.0, g.get(1, 2), PRECISION);
		Assertions.assertEquals(0.0, g.get(2, 0), PRECISION);
		Assertions.assertEquals(0.0, g.get(2, 1), PRECISION);
		Assertions.assertEquals(1.0, g.get(0, 3), PRECISION);
		Assertions.assertEquals(2.0, g.get(1, 3), PRECISION);
		Assertions.assertEquals(3.0, g.get(2, 3), PRECISION);
	}

	@Test
	void inverseTest() {
		final Matrix<Double> inverse2 = squareMatrix2.inverse().multiply(23.0);
		Assertions.assertEquals(0.0, inverse2.get(0, 0), PRECISION);
		Assertions.assertEquals(4.0, inverse2.get(0, 1), PRECISION);
		Assertions.assertEquals(1.0, inverse2.get(0, 2), PRECISION);
		Assertions.assertEquals(-7.0, inverse2.get(0, 3), PRECISION);
		Assertions.assertEquals(23.0, inverse2.get(1, 0), PRECISION);
		Assertions.assertEquals(-12.0, inverse2.get(1, 1), PRECISION);
		Assertions.assertEquals(-3.0, inverse2.get(1, 2), PRECISION);
		Assertions.assertEquals(-2.0, inverse2.get(1, 3), PRECISION);
		Assertions.assertEquals(-46.0, inverse2.get(2, 0), PRECISION);
		Assertions.assertEquals(23.0, inverse2.get(2, 1), PRECISION);
		Assertions.assertEquals(0.0, inverse2.get(2, 2), PRECISION);
		Assertions.assertEquals(23.0, inverse2.get(2, 3), PRECISION);
		Assertions.assertEquals(23.0, inverse2.get(3, 0), PRECISION);
		Assertions.assertEquals(-14.0, inverse2.get(3, 1), PRECISION);
		Assertions.assertEquals(8.0, inverse2.get(3, 2), PRECISION);
		Assertions.assertEquals(-10.0, inverse2.get(3, 3), PRECISION);

		final Matrix<Double> inverse3 = squareMatrix3.inverse();
		Assertions.assertEquals(0.5, inverse3.get(0, 0), PRECISION);
		Assertions.assertEquals(0.25, inverse3.get(0, 1), PRECISION);
		Assertions.assertEquals(0.75, inverse3.get(0, 2), PRECISION);
		Assertions.assertEquals(1.0, inverse3.get(1, 0), PRECISION);
		Assertions.assertEquals(0.5, inverse3.get(1, 1), PRECISION);
		Assertions.assertEquals(0.5, inverse3.get(1, 2), PRECISION);
		Assertions.assertEquals(0.5, inverse3.get(2, 0), PRECISION);
		Assertions.assertEquals(0.75, inverse3.get(2, 1), PRECISION);
		Assertions.assertEquals(0.25, inverse3.get(2, 2), PRECISION);
	}

	@Test
	void inverseTestSingularMatrix() {
		Assertions.assertThrows(SingularMatrixException.class, () -> {
			squareMatrix.inverse();
		});
	}

	@Test
	void toStringTest() {
		Assertions.assertEquals("|  1.0  2.0  3.0 |\n" +
								"|  4.0  5.0  6.0 |\n" +
								"|  7.0  8.0  9.0 |\n" +
								"| 10.0 11.0 12.0 |\n", matrix1.toString());
		Assertions.assertEquals("| 99.0 88.0 77.0 |\n" +
								"| 66.0 55.0 44.0 |\n" +
								"| 33.0 22.0 11.0 |\n", squareMatrix.toString());
	}
}
