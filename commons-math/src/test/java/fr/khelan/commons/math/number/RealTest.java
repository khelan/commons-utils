package fr.khelan.commons.math.number;

import java.math.BigDecimal;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class RealTest {
	private final Real zero;

	private final Real one;

	private final Real two;

	private final Real three;

	private final Real four;

	private final Real oneHalf;

	private final Real oneThird;

	private final Real pi;

	RealTest() {
		zero = Real.from(new BigDecimal("0.0"));
		one = Real.from(new BigDecimal("1.0"));
		two = Real.from(new BigDecimal("2.0"));
		three = Real.from(3.0);
		four = Real.from(new BigDecimal("4.0"));
		oneHalf = Real.from(new BigDecimal("0.5"));
		oneThird = one.divide(three);
		pi = Real.from(Math.PI);
	}

	@Test
	void addTest() {
		Assertions.assertEquals(1, one.add(zero).intValue());
		Assertions.assertEquals(2, one.add(one).intValue());
		Assertions.assertEquals(4, one.add(three).intValue());
		Assertions.assertEquals(1, oneHalf.add(oneHalf).intValue());
	}

	@Test
	void subtractTest() {
		Assertions.assertEquals(1, one.subtract(zero).intValue());
		Assertions.assertEquals(0, one.subtract(one).intValue());
		Assertions.assertEquals(2, three.subtract(one).intValue());
		Assertions.assertEquals(0, oneHalf.subtract(oneHalf).intValue());
		Assertions.assertEquals(-1, zero.subtract(one).intValue());
		Assertions.assertEquals(-2, one.subtract(three).intValue());
	}

	@Test
	void negateTest() {
		Assertions.assertEquals(0, zero.negate().intValue());
		Assertions.assertEquals(-1, one.negate().intValue());
		Assertions.assertEquals(-3, three.negate().intValue());
	}

	@Test
	void multiplyTest() {
		Assertions.assertEquals(0, one.multiply(zero).intValue());
		Assertions.assertEquals(0, zero.multiply(one).intValue());
		Assertions.assertEquals(1, one.multiply(one).intValue());
		Assertions.assertEquals(3, three.multiply(one).intValue());
		Assertions.assertEquals(3, one.multiply(three).intValue());
		Assertions.assertEquals(9, three.multiply(three).intValue());
		Assertions.assertEquals(-9, three.negate().multiply(three).intValue());
	}

	@Test
	void divideTest() {
		Assertions.assertEquals(0, zero.divide(one).intValue());
		Assertions.assertEquals(1, one.divide(one).intValue());
		Assertions.assertEquals(3, three.divide(one).intValue());
		Assertions.assertEquals(1, three.divide(three).intValue());
		Assertions.assertEquals(-1, three.negate().divide(three).intValue());
	}

	@Test
	void hasSameValueOfTest() {
		Assertions.assertTrue(one.hasSameValueOf(Numbers.ONE));
		Assertions.assertTrue(oneHalf.hasSameValueOf(Numbers.ONE_HALF));
		Assertions.assertTrue(three.hasSameValueOf(Numbers.TWO.add(Numbers.ONE)));
	}

	@Test
	void compareTo() {
		Assertions.assertEquals(0, one.compareTo(Numbers.ONE));
		Assertions.assertEquals(0, oneHalf.compareTo(Numbers.ONE_HALF));
		Assertions.assertEquals(0, three.compareTo(Numbers.TWO.add(Numbers.ONE)));

		Assertions.assertEquals(1, one.compareTo(zero));
		Assertions.assertEquals(1, one.compareTo(oneHalf));
		Assertions.assertEquals(1, three.compareTo(two));

		Assertions.assertEquals(-1, zero.compareTo(one));
		Assertions.assertEquals(-1, oneHalf.compareTo(one));
		Assertions.assertEquals(-1, oneThird.compareTo(oneHalf));
		Assertions.assertEquals(-1, two.compareTo(three));
	}

	@Test
	void inverseTest() {
		Assertions.assertEquals(1, one.inverse().intValue());
		Assertions.assertEquals(2, oneHalf.inverse().intValue());
		Assertions.assertTrue(two.inverse().hasSameValueOf(oneHalf));
		Assertions.assertEquals(1, three.divide(three).intValue());
		Assertions.assertEquals(-1, three.negate().divide(three).intValue());
	}

	@Test
	void powTest() {
		Assertions.assertEquals(0, zero.pow(Numbers.TEN).intValue());
		Assertions.assertEquals(1, one.pow(Numbers.TEN).intValue());
		Assertions.assertEquals(256, two.pow(Integer.from(8L)).intValue());
		Assertions.assertTrue(oneHalf.pow(Numbers.TWO).hasSameValueOf(Numbers.ONE.divide(Integer.from(4L))));
	}

	@Test
	void sqrtTest() {
		Assertions.assertEquals(2, four.sqrt().intValue());
	}

	@Test
	void complexResultsTest() {
		Assertions.assertTrue((zero.real().hasSameValueOf(zero)));
		Assertions.assertTrue((one.real().hasSameValueOf(one)));
		Assertions.assertTrue((oneHalf.real().hasSameValueOf(oneHalf)));
		Assertions.assertTrue((three.negate().real().hasSameValueOf(Real.from(new BigDecimal("-3.0")))));

		Assertions.assertTrue((zero.imaginary().hasSameValueOf(Numbers.ZERO)));
		Assertions.assertTrue((one.imaginary().hasSameValueOf(Numbers.ZERO)));
		Assertions.assertTrue((oneHalf.imaginary().hasSameValueOf(Numbers.ZERO)));
		Assertions.assertTrue((three.negate().imaginary().hasSameValueOf(Numbers.ZERO)));

		Assertions.assertEquals(4, two.absoluteSquare().intValue());
		Assertions.assertEquals(4, two.negate().absoluteSquare().intValue());

		Assertions.assertEquals(3, three.modulus().intValue());
		Assertions.assertEquals(3, three.negate().modulus().intValue());

		Assertions.assertTrue(zero.argument().hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(zero.argumentPi().hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(four.argument().hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(four.argumentPi().hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(three.negate().argument().hasSameValueOf(Numbers.PI));
		Assertions.assertTrue(three.negate().argumentPi().hasSameValueOf(Numbers.ONE));
	}

	@Test
	void equalsAndHashcodeTest() {
		Assertions.assertEquals(zero, zero);
		Assertions.assertEquals(one, one);
		Assertions.assertEquals(oneHalf, oneHalf);

		Assertions.assertNotEquals(zero, one);
		Assertions.assertNotEquals(one, oneHalf);
		Assertions.assertNotEquals(oneHalf, one);

		Assertions.assertEquals(zero.hashCode(), zero.hashCode());
		Assertions.assertEquals(one.hashCode(), one.hashCode());
		Assertions.assertEquals(oneHalf.hashCode(), oneHalf.hashCode());

		Assertions.assertFalse(zero.equals(null));
		Assertions.assertFalse(one.equals(null));
		Assertions.assertFalse(oneHalf.equals(null));
		Assertions.assertFalse(one.equals(Numbers.ONE));
	}

	@Test
	void primitiveValues() {
		Assertions.assertEquals(0, zero.intValue());
		Assertions.assertEquals(1, one.intValue());
		Assertions.assertEquals(3, three.intValue());
		Assertions.assertEquals(0, oneHalf.intValue());
		Assertions.assertEquals(2, oneHalf.inverse().intValue());

		Assertions.assertEquals(0L, zero.longValue());
		Assertions.assertEquals(1L, one.longValue());
		Assertions.assertEquals(3L, three.longValue());
		Assertions.assertEquals(0L, oneHalf.longValue());
		Assertions.assertEquals(2L, oneHalf.inverse().longValue());

		Assertions.assertEquals(0.0, zero.floatValue());
		Assertions.assertEquals(1.0, one.floatValue());
		Assertions.assertEquals(3.0, three.floatValue());
		Assertions.assertEquals(0.5, oneHalf.floatValue());
		Assertions.assertEquals(2.0, oneHalf.inverse().floatValue());

		Assertions.assertEquals(0.0D, zero.doubleValue());
		Assertions.assertEquals(1.0D, one.doubleValue());
		Assertions.assertEquals(3.0D, three.doubleValue());
		Assertions.assertEquals(0.5D, oneHalf.doubleValue());
		Assertions.assertEquals(2.0D, oneHalf.inverse().doubleValue());
	}

	@Test
	void toStringTest() {
		Assertions.assertEquals("0.0", zero.toString());
		Assertions.assertEquals("1.0", one.toString());
		Assertions.assertEquals("2.0", two.toString());
		Assertions.assertEquals("0.5", oneHalf.toString());
		Assertions.assertEquals("-0.5", oneHalf.negate().toString());
		Assertions.assertEquals("3.0", three.toString());
		Assertions.assertEquals("4.0", four.toString());
		Assertions.assertEquals("-4.0", four.negate().toString());
	}
}
