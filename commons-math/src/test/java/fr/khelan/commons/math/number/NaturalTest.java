package fr.khelan.commons.math.number;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class NaturalTest {
	private static final float FLOAT_PRECISION = 0.001F;
	private static final double DOUBLE_PRECISION = 0.001;

	private final Natural zero;

	private final Natural one;
	private final Natural two;
	private final Natural three;
	private final Natural four;
	private final Natural five;
	private final Natural ten;
	private final Natural eightyFive;
	private final Natural longValue;
	private final Natural bigInteger;

	NaturalTest() {
		zero = Natural.from(0);
		one = Natural.from(1);
		two = Natural.from(2);
		three = Natural.from(3);
		four = Natural.from(4);
		five = Natural.from(5);
		ten = Natural.from(10L);
		eightyFive = Natural.from(85);
		longValue = Natural.from(7654825048765065875L);
		bigInteger = Natural.from(new BigInteger("44348653431046868743201020678603668736200"));
	}

	@Test
	void hasSameValueOfTest() {
		Assertions.assertTrue(zero.hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(zero.hasSameValueOf(Complex.from(Numbers.ZERO)));

		Assertions.assertTrue(one.hasSameValueOf(Numbers.ONE));
		Assertions.assertTrue(one.hasSameValueOf(Natural.from(BigInteger.ONE)));
		Assertions.assertTrue(ten.hasSameValueOf(Natural.from(BigInteger.TEN)));
		Assertions.assertTrue(longValue.hasSameValueOf(Natural.from("7654825048765065875")));
		Assertions.assertTrue(bigInteger.hasSameValueOf(Natural.from("44348653431046868743201020678603668736200")));

		Assertions.assertFalse(one.hasSameValueOf(two));
		Assertions.assertFalse(bigInteger.hasSameValueOf(Natural.from("44348653431046820678603668736201")));
	}

	@Test
	void primitiveValuesTest() {
		Assertions.assertEquals(0, zero.intValue());
		Assertions.assertEquals(1, one.intValue());
		Assertions.assertEquals(85, eightyFive.intValue());

		Assertions.assertEquals(0L, zero.longValue());
		Assertions.assertEquals(5L, five.longValue());
		Assertions.assertEquals(7654825048765065875L, longValue.longValue());

		Assertions.assertEquals(0.0F, zero.floatValue(), FLOAT_PRECISION);
		Assertions.assertEquals(3.0F, three.floatValue(), FLOAT_PRECISION);

		Assertions.assertEquals(0.0, zero.doubleValue(), DOUBLE_PRECISION);
		Assertions.assertEquals(4.0, four.doubleValue(), DOUBLE_PRECISION);
	}

	@Test
	void parentValuesTest() {
		Assertions.assertEquals(new BigDecimal("0"), zero.decimalValue());
		Assertions.assertEquals(new BigDecimal("85"), eightyFive.decimalValue());
		Assertions.assertEquals(new BigDecimal("44348653431046868743201020678603668736200"), bigInteger.decimalValue());

		Assertions.assertEquals(zero, zero.numerator());
		Assertions.assertEquals(two, two.numerator());
		Assertions.assertEquals(bigInteger, bigInteger.numerator());

		Assertions.assertTrue(one.hasSameValueOf(zero.denominator()));
		Assertions.assertTrue(one.hasSameValueOf(ten.denominator()));
		Assertions.assertTrue(one.hasSameValueOf(bigInteger.denominator()));
	}

	@Test
	void absSignumTest() {
		Assertions.assertEquals(zero, zero.abs());
		Assertions.assertEquals(one, one.abs());
		Assertions.assertEquals(four, four.abs());
		Assertions.assertEquals(longValue, longValue.abs());

		Assertions.assertEquals(0, zero.signum());
		Assertions.assertEquals(1, three.signum());
		Assertions.assertEquals(1, bigInteger.signum());
	}

	@Test
	void addTest() {
		Assertions.assertEquals(zero, zero.add(zero));
		Assertions.assertEquals(ten, ten.add(zero));
		Assertions.assertEquals(ten, zero.add(ten));

		Assertions.assertEquals(five, two.add(three));
		Assertions.assertEquals(five, four.add(one));
	}

	@Test
	void subtractTest() {
		Assertions.assertEquals(zero, zero.subtract(zero));
		Assertions.assertEquals(ten, ten.subtract(zero));

		Assertions.assertEquals(three, five.subtract(two));
	}

	@Test
	void subtractTestError() {
		Assertions.assertThrows(ArithmeticException.class, () -> {
			two.subtract(five);
		});
	}

	@Test
	void negateTest() {
		Assertions.assertEquals(zero, zero.negate());
	}

	@Test
	void negateTestError() {
		Assertions.assertThrows(ArithmeticException.class, () -> {
			five.negate();
		});
	}

	@Test
	void multiplyTest() {
		Assertions.assertEquals(zero, zero.multiply(eightyFive));

		Assertions.assertEquals(eightyFive, one.multiply(eightyFive));

		Assertions.assertEquals(Natural.from(15L), three.multiply(five));
	}

	@Test
	void divideTest() {
		Assertions.assertTrue(Numbers.ZERO.hasSameValueOf(zero.divide(two)));
		Assertions.assertTrue(Numbers.ONE.hasSameValueOf(one.divide(one)));
		Assertions.assertTrue(Numbers.ONE.hasSameValueOf(five.divide(five)));

		Assertions.assertTrue(Numbers.ONE_HALF.hasSameValueOf(one.divide(two)));
		Assertions.assertTrue(four.hasSameValueOf(four.divide(one)));
		Assertions.assertTrue(two.hasSameValueOf(four.divide(two)));

		Assertions.assertTrue(
				Rational.from("2424520948523583258523582346724953455224", "32353253580950232").hasSameValueOf(
						Natural.from("2424520948523583258523582346724953455224")
							   .divide(Natural.from("32353253580950232"))));
	}

	@Test
	void divideToIntegralValueTest() {
		Assertions.assertTrue(Numbers.ZERO.hasSameValueOf(zero.divideToIntegralValue(two)));
		Assertions.assertTrue(Numbers.ONE.hasSameValueOf(one.divideToIntegralValue(one)));
		Assertions.assertTrue(Numbers.ONE.hasSameValueOf(five.divideToIntegralValue(five)));

		Assertions.assertTrue(Numbers.ZERO.hasSameValueOf(one.divideToIntegralValue(two)));
		Assertions.assertTrue(four.hasSameValueOf(four.divideToIntegralValue(one)));
		Assertions.assertTrue(two.hasSameValueOf(four.divideToIntegralValue(two)));

		Assertions.assertTrue(Natural.from("221743267155234343716005103393018343681")
									 .hasSameValueOf(bigInteger.divideToIntegralValue(Natural.from(200L))));
	}

	@Test
	void remainderTest() {
		Assertions.assertTrue(Numbers.ZERO.hasSameValueOf(zero.remainder(two)));
		Assertions.assertTrue(Numbers.ONE.hasSameValueOf(one.remainder(two)));

		Assertions.assertEquals(Natural.from("32"), Natural.from("432534259038752").remainder(Natural.from("64")));
		Assertions.assertEquals(Natural.from("2"), Natural.from("432534259038752").remainder(Natural.from("33")));
	}

	@Test
	void inverseTest() {
		Assertions.assertEquals(three.inverse(), one.divide(three));
		Assertions.assertEquals(bigInteger.inverse(), one.divide(bigInteger));
	}

	@Test
	void powTest() {
		Assertions.assertEquals(one, zero.pow(zero));
		Assertions.assertEquals(one, eightyFive.pow(zero));
		Assertions.assertEquals(Natural.from("1024"), two.pow(ten));
	}

	@Test
	void gcdTest() {
		Assertions.assertTrue(Numbers.ONE.hasSameValueOf(five.gcd(one)));
		Assertions.assertTrue(eightyFive.hasSameValueOf(eightyFive.gcd(eightyFive)));
		Assertions.assertTrue(two.hasSameValueOf(four.gcd(two)));

		Assertions.assertTrue(Natural.from(12L).hasSameValueOf(Natural.from(60L).gcd(Natural.from(84L))));
		Assertions.assertTrue(Natural.from(12L).hasSameValueOf(Natural.from(84L).gcd(Natural.from(60L))));

		Assertions.assertTrue(Natural.from(1L).hasSameValueOf(Natural.from(21L).gcd(Natural.from(10L))));
		Assertions.assertTrue(Natural.from(6L).hasSameValueOf(Natural.from(285648L).gcd(Natural.from(1654122L))));
		Assertions.assertTrue(Natural.from(24L).hasSameValueOf(Natural.from(1272L).gcd(Natural.from(1128L))));
	}

	@Test
	void lcmTest() {
		Assertions.assertTrue(Natural.from(5).hasSameValueOf(five.lcm(one)));
		Assertions.assertTrue(Natural.from(5).hasSameValueOf(one.lcm(five)));

		Assertions.assertTrue(Natural.from(0L).hasSameValueOf(Natural.from(0L).lcm(Natural.from(63L))));
		Assertions.assertTrue(Natural.from(0L).hasSameValueOf(Natural.from(63L).lcm(Natural.from(0L))));

		Assertions.assertTrue(Natural.from(24L).hasSameValueOf(Natural.from(6L).lcm(Natural.from(8L))));
		Assertions.assertTrue(Natural.from(24L).hasSameValueOf(Natural.from(8L).lcm(Natural.from(6L))));
	}

	@Test
	void equalsAndHashcodeTest() {
		Assertions.assertEquals(zero, zero);
		Assertions.assertEquals(zero.hashCode(), zero.hashCode());
		Assertions.assertEquals(one, Natural.from("1"));
		Assertions.assertEquals(Natural.from("1").hashCode(), one.hashCode());

		Assertions.assertEquals(eightyFive, Natural.from("85"));
		Assertions.assertEquals(Natural.from("85").hashCode(), eightyFive.hashCode());

		Assertions.assertEquals(Natural.from((BigInteger) null), Natural.from((BigInteger) null));

		Assertions.assertNotEquals(one, zero);
		Assertions.assertNotEquals(zero, one);

		Assertions.assertNotEquals(null, eightyFive);
		Assertions.assertNotEquals(Natural.from((BigInteger) null), eightyFive);
	}
}
