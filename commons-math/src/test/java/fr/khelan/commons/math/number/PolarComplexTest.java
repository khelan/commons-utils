package fr.khelan.commons.math.number;

import java.math.BigDecimal;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PolarComplexTest {
	private static final double PRECISION = 0.000001D;

	private final PolarComplex zero;
	private final PolarComplex one;
	private final PolarComplex i;
	private final PolarComplex a1;
	private final PolarComplex a2;
	private final PolarComplex b1;
	private final PolarComplex b2;
	private final PolarComplex c1;
	private final PolarComplex c2;
	private final PolarComplex d1;
	private final PolarComplex e1;
	private final PolarComplex f1;
	private final PolarComplex h1;

	PolarComplexTest() {
		zero = new PolarComplex(Numbers.ZERO, Numbers.ZERO);
		one = new PolarComplex(Numbers.ONE, Numbers.ZERO);
		i = new PolarComplex(realValue("1"), realValue("0.5"));
		a1 = new PolarComplex(Integer.from(5L), Numbers.ONE.divide(realValue("4")));
		a2 = new PolarComplex(realValue("5"), realValue("0.25"));
		b1 = new PolarComplex(realValue(5), realValue("-0.75"));
		b2 = new PolarComplex(realValue("5.0"), realValue("-0.75"));
		c1 = new PolarComplex(realValue(5), realValue(0));
		c2 = new PolarComplex(realValue("5"), Numbers.ZERO);
		d1 = new PolarComplex(realValue("1"), realValue("1"));
		e1 = new PolarComplex(realValue("7"), realValue("0.5"));
		f1 = new PolarComplex(realValue("7"), realValue("-0.5"));
		h1 = new PolarComplex(realValue("658978.556"), realValue("0.75"));
	}

	private static Real realValue(final int integer) {
		return Real.from(new BigDecimal(integer));
	}

	private static Real realValue(final String decimal) {
		return Real.from(new BigDecimal(decimal));
	}

	@Test
	void fromTest() {
		Complex c = Complex.fromPolarRepresentation(Numbers.ONE, Numbers.ZERO);
		Assertions.assertTrue(c.hasSameValueOf(Numbers.ONE));
	}

	@Test
	void hasSameValueOfTest() {
		Assertions.assertTrue(zero.hasSameValueOf(Numbers.ZERO));

		Assertions.assertTrue(a1.hasSameValueOf(a2));
		Assertions.assertTrue(a2.hasSameValueOf(a1));

		Assertions.assertTrue(b1.hasSameValueOf(b2));

		Assertions.assertTrue(c1.hasSameValueOf(c2));

		Assertions.assertTrue(d1.hasSameValueOf(Numbers.ONE.negate()));

		Assertions.assertTrue(i.hasSameValueOf(Numbers.I));

		Assertions.assertFalse(zero.hasSameValueOf(Numbers.ONE));
		Assertions.assertFalse(zero.hasSameValueOf(Numbers.I));
		Assertions.assertFalse(i.hasSameValueOf(e1));
		Assertions.assertFalse(a1.hasSameValueOf(b1));
		Assertions.assertFalse(a2.hasSameValueOf(b1));
		Assertions.assertFalse(a1.hasSameValueOf(c1));
		Assertions.assertFalse(a1.hasSameValueOf(d1));
		Assertions.assertFalse(a1.hasSameValueOf(e1));
		Assertions.assertFalse(a1.hasSameValueOf(f1));
		Assertions.assertFalse(a1.hasSameValueOf(f1));
		Assertions.assertFalse(b1.hasSameValueOf(c1));
		Assertions.assertFalse(b1.hasSameValueOf(d1));
		Assertions.assertFalse(b1.hasSameValueOf(e1));
		Assertions.assertFalse(b1.hasSameValueOf(f1));
		Assertions.assertFalse(c1.hasSameValueOf(d1));
		Assertions.assertFalse(c1.hasSameValueOf(e1));
		Assertions.assertFalse(d1.hasSameValueOf(e1));
		Assertions.assertFalse(d1.hasSameValueOf(f1));
		Assertions.assertFalse(e1.hasSameValueOf(f1));
		Assertions.assertFalse(d1.hasSameValueOf(zero));
		Assertions.assertFalse(d1.hasSameValueOf(Numbers.I));
		Assertions.assertFalse(f1.hasSameValueOf(Numbers.I));
	}

	@Test
	void isRealTest() {
		Assertions.assertTrue(zero.isReal());
		Assertions.assertTrue(one.isReal());
		Assertions.assertTrue(c1.isReal());
		Assertions.assertTrue(c2.isReal());
		Assertions.assertTrue(d1.isReal());

		Assertions.assertFalse(Numbers.I.isReal());
		Assertions.assertFalse(a1.isReal());
		Assertions.assertFalse(a2.isReal());
		Assertions.assertFalse(b1.isReal());
		Assertions.assertFalse(b2.isReal());
		Assertions.assertFalse(e1.isReal());
		Assertions.assertFalse(f1.isReal());
	}

	@Test
	void isImaginaryTest() {
		Assertions.assertTrue(zero.isImaginary());
		Assertions.assertTrue(i.isImaginary());
		Assertions.assertTrue(i.isImaginary());
		Assertions.assertTrue(e1.isImaginary());
		Assertions.assertTrue(f1.isImaginary());

		Assertions.assertFalse(one.isImaginary());
		Assertions.assertFalse(a1.isImaginary());
		Assertions.assertFalse(a2.isImaginary());
		Assertions.assertFalse(b1.isImaginary());
		Assertions.assertFalse(b2.isImaginary());
		Assertions.assertFalse(c1.isImaginary());
		Assertions.assertFalse(c2.isImaginary());
		Assertions.assertFalse(d1.isImaginary());
	}

	@Test
	void isZeroTest() {
		Assertions.assertTrue(zero.isZero());

		Assertions.assertFalse(one.isZero());
		Assertions.assertFalse(i.isZero());
		Assertions.assertFalse(a1.isZero());
		Assertions.assertFalse(a2.isZero());
		Assertions.assertFalse(b1.isZero());
		Assertions.assertFalse(b2.isZero());
		Assertions.assertFalse(c1.isZero());
		Assertions.assertFalse(c2.isZero());
		Assertions.assertFalse(d1.isZero());
		Assertions.assertFalse(e1.isZero());
		Assertions.assertFalse(f1.isZero());
	}

	@Test
	void realImaginaryTest() {
		Assertions.assertEquals(0, zero.real().compareTo(Numbers.ZERO));
		Assertions.assertEquals(0, zero.imaginary().compareTo(Numbers.ZERO));
		Assertions.assertEquals(0, one.real().compareTo(Numbers.ONE));
		Assertions.assertEquals(0, one.imaginary().compareTo(Numbers.ZERO));
		Assertions.assertEquals(0, i.real().compareTo(Numbers.ZERO));
		Assertions.assertEquals(0, i.imaginary().compareTo(Numbers.ONE));

		Assertions.assertEquals(0, c1.real().compareTo(realValue(5)));
		Assertions.assertEquals(0, c1.imaginary().compareTo(realValue(0)));

		Assertions.assertEquals(0, d1.real().compareTo(realValue(-1)));
		Assertions.assertEquals(0, d1.imaginary().compareTo(realValue(0)));

		Assertions.assertEquals(0, e1.real().compareTo(realValue(0)));
		Assertions.assertEquals(0, e1.imaginary().compareTo(realValue(7)));

		Assertions.assertEquals(0, f1.real().compareTo(realValue(0)));
		Assertions.assertEquals(0, f1.imaginary().compareTo(realValue(-7)));
	}

	@Test
	void equalsAndHashcodeTest() {
		Assertions.assertEquals(a1, a1);
		Assertions.assertEquals(a1.hashCode(), a1.hashCode());

		Assertions.assertNotEquals(a1, b1);
		Assertions.assertNotEquals(b1.hashCode(), a1.hashCode());

		Assertions.assertEquals(Complex.fromPolarRepresentation(null, null), new PolarComplex(null, null));
		Assertions.assertEquals(Complex.fromPolarRepresentation(null, null).hashCode(),
								new PolarComplex(null, null).hashCode());

	}

	@Test
	void modulusTest() {
		Assertions.assertEquals(0, zero.modulus().compareTo(Numbers.ZERO));
		Assertions.assertEquals(0, one.modulus().compareTo(Numbers.ONE));
		Assertions.assertEquals(0, i.modulus().compareTo(Numbers.ONE));

		Assertions.assertEquals(0, a1.modulus().compareTo(realValue(5)));
		Assertions.assertEquals(0, a2.modulus().compareTo(realValue(5)));
		Assertions.assertEquals(0, b1.modulus().compareTo(realValue(5)));
		Assertions.assertEquals(0, b2.modulus().compareTo(realValue(5)));
		Assertions.assertEquals(0, c1.modulus().compareTo(realValue(5)));

		Assertions.assertEquals(0, h1.modulus().compareTo(Real.from(new BigDecimal("658978.556"))));
	}

	@Test
	void argumentTest() {
		Assertions.assertEquals(0.0, Complex.from(Numbers.ZERO).argument().doubleValue(), PRECISION);
		Assertions.assertTrue(Complex.from(Numbers.ZERO).argumentPi().hasSameValueOf(Numbers.ZERO));
		Assertions.assertEquals(0.0, one.argument().doubleValue(), PRECISION);
		Assertions.assertTrue(one.argumentPi().hasSameValueOf(Numbers.ZERO));
		Assertions.assertEquals(Math.PI, one.negate().argument().doubleValue(), PRECISION);
		Assertions.assertTrue(one.negate().argumentPi().hasSameValueOf(Numbers.ONE));
		Assertions.assertEquals(Math.PI / 2, Complex.toCartesianRepresentation(Numbers.I).argument().doubleValue(),
								0.000001);
		Assertions.assertTrue(
				Complex.toCartesianRepresentation(Numbers.I).argumentPi().hasSameValueOf(Numbers.ONE_HALF));
		Assertions.assertEquals(-Math.PI / 2,
								Complex.toCartesianRepresentation(Numbers.I.negate()).argument().doubleValue(),
								0.000001);
		Assertions.assertTrue(Complex.toCartesianRepresentation(Numbers.I.negate()).argumentPi()
									 .hasSameValueOf(Numbers.ONE_HALF.negate()));

		Assertions.assertEquals(0.25 * Math.PI, a1.argument().doubleValue(), PRECISION);
		Assertions.assertEquals(0.25 * Math.PI, a2.argument().doubleValue(), PRECISION);

		Assertions.assertEquals(-0.75 * Math.PI, b1.argument().doubleValue(), PRECISION);
		Assertions.assertEquals(-0.75 * Math.PI, b2.argument().doubleValue(), PRECISION);

		Assertions.assertEquals(0.0, c1.argument().doubleValue(), PRECISION);
		Assertions.assertTrue(c1.argumentPi().hasSameValueOf(Numbers.ZERO));
		Assertions.assertEquals(Math.PI, c1.negate().argument().doubleValue(), PRECISION);
		Assertions.assertTrue(c1.negate().argumentPi().hasSameValueOf(Numbers.ONE));

		Assertions.assertEquals(-Math.PI / 2, f1.argument().doubleValue(), PRECISION);
		Assertions.assertTrue(f1.argumentPi().hasSameValueOf(Numbers.ONE_HALF.negate()));
		Assertions.assertEquals(Math.PI / 2, f1.negate().argument().doubleValue(), PRECISION);
		Assertions.assertTrue(f1.negate().argumentPi().hasSameValueOf(Numbers.ONE_HALF));

		Assertions.assertEquals(3 * Math.PI / 4, h1.argument().doubleValue(), PRECISION);
		Assertions.assertEquals(3.0 / 4.0, h1.argumentPi().doubleValue(), PRECISION);
	}

	@Test
	void conjugateTest() {
		Assertions.assertTrue(zero.conjugate().hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(one.conjugate().hasSameValueOf(Numbers.ONE));
		Assertions.assertTrue(Numbers.I.conjugate().hasSameValueOf(Numbers.I.negate()));

		Assertions.assertTrue(a1.conjugate().hasSameValueOf(new PolarComplex(realValue(5), realValue("-0.25"))));
		Assertions.assertTrue(a2.conjugate().hasSameValueOf(new PolarComplex(realValue(5), realValue("-0.25"))));

		Assertions.assertTrue(b1.conjugate().hasSameValueOf(new PolarComplex(realValue(5), realValue("0.75"))));

		Assertions.assertTrue(c1.conjugate().hasSameValueOf(c1));

		Assertions.assertTrue(
				h1.conjugate().hasSameValueOf(new PolarComplex(realValue("658978.556"), realValue("-0.75"))));
	}

	@Test
	void negateTest() {
		Assertions.assertTrue(zero.negate().hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(one.negate().hasSameValueOf(new CartesianComplex(realValue(-1))));
		Assertions.assertTrue(i.negate().hasSameValueOf(new CartesianComplex(Numbers.ZERO, realValue(-1))));

		Assertions.assertTrue(a1.negate().hasSameValueOf(new PolarComplex(realValue(5), realValue("-0.75"))));
		Assertions.assertTrue(a2.negate().hasSameValueOf(new PolarComplex(realValue(5), realValue("-0.75"))));

		Assertions.assertTrue(b1.negate().hasSameValueOf(new PolarComplex(realValue(5), realValue("0.25"))));

		Assertions.assertTrue(
				h1.negate().hasSameValueOf(new PolarComplex(realValue("658978.556"), realValue("-0.25"))));
	}

	@Test
	void addTest() {
		Complex added = a1.add(Numbers.ZERO);
		Assertions.assertEquals(a1.modulus().doubleValue(), added.modulus().doubleValue(), PRECISION);
		Assertions.assertEquals(a1.argumentPi().doubleValue(), added.argumentPi().doubleValue(), PRECISION);

		added = b1.add(Numbers.ZERO);
		Assertions.assertEquals(b1.modulus().doubleValue(), added.modulus().doubleValue(), PRECISION);
		Assertions.assertEquals(b1.argumentPi().doubleValue(), added.argumentPi().doubleValue(), PRECISION);

		added = c1.add(Numbers.ZERO);
		Assertions.assertEquals(c1.modulus().doubleValue(), added.modulus().doubleValue(), PRECISION);
		Assertions.assertEquals(c1.argumentPi().doubleValue(), added.argumentPi().doubleValue(), PRECISION);

		added = d1.add(Numbers.ZERO);
		Assertions.assertEquals(d1.modulus().doubleValue(), added.modulus().doubleValue(), PRECISION);
		Assertions.assertEquals(d1.argumentPi().doubleValue(), added.argumentPi().doubleValue(), PRECISION);

		added = e1.add(Numbers.ZERO);
		Assertions.assertEquals(e1.modulus().doubleValue(), added.modulus().doubleValue(), PRECISION);
		Assertions.assertEquals(e1.argumentPi().doubleValue(), added.argumentPi().doubleValue(), PRECISION);

		added = f1.add(Numbers.ZERO);
		Assertions.assertEquals(f1.modulus().doubleValue(), added.modulus().doubleValue(), PRECISION);
		Assertions.assertEquals(f1.argumentPi().doubleValue(), added.argumentPi().doubleValue(), PRECISION);

		added = h1.add(Numbers.ZERO);
		Assertions.assertEquals(h1.modulus().doubleValue(), added.modulus().doubleValue(), PRECISION);
		Assertions.assertEquals(h1.argumentPi().doubleValue(), added.argumentPi().doubleValue(), PRECISION);
	}

	@Test
	void subtractTest() {
		Complex subtracted = a1.subtract(Numbers.ZERO);
		Assertions.assertEquals(a1.modulus().doubleValue(), subtracted.modulus().doubleValue(), PRECISION);
		Assertions.assertEquals(a1.argumentPi().doubleValue(), subtracted.argumentPi().doubleValue(), PRECISION);

		subtracted = b1.subtract(Numbers.ZERO);
		Assertions.assertEquals(b1.modulus().doubleValue(), subtracted.modulus().doubleValue(), PRECISION);
		Assertions.assertEquals(b1.argumentPi().doubleValue(), subtracted.argumentPi().doubleValue(), PRECISION);

		subtracted = c1.subtract(Numbers.ZERO);
		Assertions.assertEquals(c1.modulus().doubleValue(), subtracted.modulus().doubleValue(), PRECISION);
		Assertions.assertEquals(c1.argumentPi().doubleValue(), subtracted.argumentPi().doubleValue(), PRECISION);

		subtracted = d1.subtract(Numbers.ZERO);
		Assertions.assertEquals(d1.modulus().doubleValue(), subtracted.modulus().doubleValue(), PRECISION);
		Assertions.assertEquals(d1.argumentPi().doubleValue(), subtracted.argumentPi().doubleValue(), PRECISION);

		subtracted = e1.subtract(Numbers.ZERO);
		Assertions.assertEquals(e1.modulus().doubleValue(), subtracted.modulus().doubleValue(), PRECISION);
		Assertions.assertEquals(e1.argumentPi().doubleValue(), subtracted.argumentPi().doubleValue(), PRECISION);

		subtracted = f1.subtract(Numbers.ZERO);
		Assertions.assertEquals(f1.modulus().doubleValue(), subtracted.modulus().doubleValue(), PRECISION);
		Assertions.assertEquals(f1.argumentPi().doubleValue(), subtracted.argumentPi().doubleValue(), PRECISION);

		subtracted = h1.subtract(Numbers.ZERO);
		Assertions.assertEquals(h1.modulus().doubleValue(), subtracted.modulus().doubleValue(), PRECISION);
		Assertions.assertEquals(h1.argumentPi().doubleValue(), subtracted.argumentPi().doubleValue(), PRECISION);
	}

	@Test
	void multiplyComplexTest() {
		Assertions.assertTrue(zero.multiply(zero).hasSameValueOf(zero));
		Assertions.assertTrue(i.multiply(zero).hasSameValueOf(zero));

		Assertions.assertTrue(one.multiply(i).hasSameValueOf(Numbers.I));
		Assertions.assertTrue(i.multiply(one).hasSameValueOf(Numbers.I));

		Assertions.assertTrue(i.multiply(i).hasSameValueOf(one.negate()));

		Assertions.assertTrue(a1.multiply(Numbers.ZERO).hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(b1.multiply(Numbers.ZERO).hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(c1.multiply(Numbers.ZERO).hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(d1.multiply(Numbers.ZERO).hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(e1.multiply(Numbers.ZERO).hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(f1.multiply(Numbers.ZERO).hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(h1.multiply(Numbers.ZERO).hasSameValueOf(Numbers.ZERO));

		Assertions.assertTrue(a1.multiply(Numbers.ONE).hasSameValueOf(a1));
		Assertions.assertTrue(b1.multiply(Numbers.ONE).hasSameValueOf(b1));
		Assertions.assertTrue(c1.multiply(Numbers.ONE).hasSameValueOf(c1));
		Assertions.assertTrue(d1.multiply(Numbers.ONE).hasSameValueOf(d1));
		Assertions.assertTrue(e1.multiply(Numbers.ONE).hasSameValueOf(e1));
		Assertions.assertTrue(f1.multiply(Numbers.ONE).hasSameValueOf(f1));
		Assertions.assertTrue(h1.multiply(Numbers.ONE).hasSameValueOf(h1));

		Assertions.assertTrue(a1.multiply(b1).hasSameValueOf(new PolarComplex(realValue(25), realValue("-0.5"))));
		Assertions.assertTrue(a2.multiply(b2).hasSameValueOf(new PolarComplex(realValue(25), realValue("-0.5"))));

		Assertions.assertTrue(c1.multiply(d1).hasSameValueOf(new PolarComplex(realValue(5), realValue(1))));
		Assertions.assertTrue(c1.multiply(f1).hasSameValueOf(new PolarComplex(realValue(35), realValue("-0.5"))));
		Assertions.assertTrue(e1.multiply(f1).hasSameValueOf(new PolarComplex(realValue(49), realValue(0))));
	}

	@Test
	void divideComplexTest() {
		Assertions.assertTrue(Numbers.ZERO.divide(Numbers.ONE).hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(Numbers.ONE.divide(Numbers.ONE).hasSameValueOf(Numbers.ONE));
		Assertions.assertTrue(Numbers.I.divide(Numbers.ONE).hasSameValueOf(Numbers.I));
		Assertions.assertTrue(Numbers.ZERO.divide(Numbers.I).hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(Numbers.ONE.divide(Numbers.I).hasSameValueOf(Numbers.I.negate()));
		Assertions.assertTrue(Numbers.I.divide(Numbers.I).hasSameValueOf(Numbers.ONE));

		Assertions.assertTrue(a1.divide(Numbers.ONE).hasSameValueOf(a1));
		Assertions.assertTrue(b1.divide(Numbers.ONE).hasSameValueOf(b1));
		Assertions.assertTrue(c1.divide(Numbers.ONE).hasSameValueOf(c1));
		Assertions.assertTrue(d1.divide(Numbers.ONE).hasSameValueOf(d1));
		Assertions.assertTrue(e1.divide(Numbers.ONE).hasSameValueOf(e1));
		Assertions.assertTrue(f1.divide(Numbers.ONE).hasSameValueOf(f1));
		Assertions.assertTrue(h1.divide(Numbers.ONE).hasSameValueOf(h1));

		Assertions.assertTrue(a1.divide(b1).hasSameValueOf(Numbers.ONE.negate()));
		Assertions.assertTrue(a2.divide(b2).hasSameValueOf(Numbers.ONE.negate()));

		Assertions.assertTrue(d1.divide(c1).hasSameValueOf(new PolarComplex(realValue("0.2"), realValue("1"))));

		Complex division = c1.divide(f1);
		Assertions.assertTrue(division.real().hasSameValueOf(Numbers.ZERO));
		Assertions.assertEquals(5.0 / 7, division.imaginary().doubleValue(), PRECISION);

		division = e1.divide(f1);
		Assertions.assertTrue(division.modulus().hasSameValueOf(Numbers.ONE));
		Assertions.assertTrue(division.argumentPi().hasSameValueOf(Numbers.ONE));

		Assertions.assertThrows(ArithmeticException.class, () -> one.divide(zero));
	}

	@Test
	void inverseTest() {
		Assertions.assertTrue(Numbers.ONE.inverse().hasSameValueOf(Numbers.ONE));
		Assertions.assertTrue(Numbers.I.inverse().hasSameValueOf(Numbers.I.negate()));

		Assertions.assertTrue(a1.inverse().hasSameValueOf(new PolarComplex(realValue("0.2"), realValue("-0.25"))));
		Assertions.assertTrue(a2.inverse().hasSameValueOf(new PolarComplex(realValue("0.2"), realValue("-0.25"))));
		Assertions.assertTrue(b1.inverse().hasSameValueOf(new PolarComplex(realValue("0.2"), realValue("0.75"))));
		Assertions.assertTrue(b2.inverse().hasSameValueOf(new PolarComplex(realValue("0.2"), realValue("0.75"))));
		Assertions.assertTrue(c1.inverse().hasSameValueOf(Numbers.ONE.divide(c1)));
		Assertions.assertTrue(d1.inverse().hasSameValueOf(Numbers.ONE.divide(d1)));
		Assertions.assertTrue(e1.inverse().hasSameValueOf(Numbers.ONE.divide(e1)));
		Assertions.assertTrue(f1.inverse().hasSameValueOf(Numbers.ONE.divide(f1)));

		Complex inverse = h1.inverse();
		Assertions.assertEquals(1 / 658978.556, inverse.modulus().doubleValue(), PRECISION);
		Assertions.assertTrue(inverse.argumentPi().hasSameValueOf(realValue("-0.75")));

		Assertions.assertThrows(ArithmeticException.class, () -> zero.inverse());

	}

	@Test
	void equalsTest() {
		Assertions.assertEquals(zero, zero);
		Assertions.assertEquals(h1, h1);

		Assertions.assertFalse(one.equals(null));
		Assertions.assertFalse(i.equals(null));
		Assertions.assertFalse(one.equals(Numbers.ONE));

		Assertions.assertEquals(new PolarComplex(realValue("658978.556"), realValue("0.75")), h1);

		Assertions.assertEquals(new PolarComplex(realValue(36540), Numbers.ZERO),
								new PolarComplex(realValue(36540), Numbers.ZERO));
		Assertions.assertEquals(new PolarComplex(realValue(36540), Numbers.ONE),
								new PolarComplex(realValue(36540), Numbers.ONE));

		Assertions.assertNotEquals(zero, a1);
		Assertions.assertNotEquals(new PolarComplex(realValue("658978.556"), realValue("0.33")), h1);
		Assertions.assertNotEquals(new PolarComplex(realValue("658978.555"), realValue("-0.33")), h1);

		Assertions.assertNotEquals(new PolarComplex(realValue(36540), realValue("0.25")),
								   new PolarComplex(realValue(36541), realValue("0.25")));
	}
}
