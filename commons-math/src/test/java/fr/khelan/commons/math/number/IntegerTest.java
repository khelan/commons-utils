package fr.khelan.commons.math.number;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class IntegerTest {
	private static final float FLOAT_PRECISION = 0.001F;
	private static final double DOUBLE_PRECISION = 0.001;

	private final Integer zero;

	private final Integer one;
	private final Integer two;
	private final Integer three;
	private final Integer four;
	private final Integer five;
	private final Integer ten;
	private final Integer eightyFive;
	private final Integer longValue;
	private final Integer bigInteger;

	private final Integer oneNegative;
	private final Integer twoNegative;
	private final Integer threeNegative;
	private final Integer tenNegative;
	private final Integer thirtyThreeNegative;
	private final Integer longNegative;
	private final Integer bigNegativeInteger;

	IntegerTest() {
		zero = Integer.from(0);
		one = Integer.from(1);
		two = Integer.from(2);
		three = Integer.from(3);
		four = Integer.from(4);
		five = Integer.from(5);
		ten = Integer.from(10L);
		eightyFive = Integer.from(85);
		longValue = Integer.from(7654825048765065875L);
		bigInteger = Integer.from(new BigInteger("44348653431046868743201020678603668736200"));
		oneNegative = Integer.from(-1);
		twoNegative = Integer.from(-2);
		threeNegative = Integer.from(-3);
		tenNegative = Integer.from(-10);
		thirtyThreeNegative = Integer.from(-33);
		longNegative = Integer.from(-5489650234065703972L);
		bigNegativeInteger = Integer.from("-541068907326578960357406570305674893025");
	}

	@Test
	void hasSameValueOfTest() {
		Assertions.assertTrue(zero.hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(zero.hasSameValueOf(Complex.from(Numbers.ZERO)));

		Assertions.assertTrue(one.hasSameValueOf(Numbers.ONE));
		Assertions.assertTrue(one.hasSameValueOf(Integer.from(BigInteger.ONE)));
		Assertions.assertTrue(ten.hasSameValueOf(Integer.from(BigInteger.TEN)));
		Assertions.assertTrue(longValue.hasSameValueOf(Integer.from("7654825048765065875")));
		Assertions.assertTrue(bigInteger.hasSameValueOf(Integer.from("44348653431046868743201020678603668736200")));

		Assertions.assertTrue(tenNegative.hasSameValueOf(Integer.from("-10")));
		Assertions.assertTrue(longNegative.hasSameValueOf(Integer.from("-5489650234065703972")));
		Assertions.assertTrue(
				bigNegativeInteger.hasSameValueOf(Integer.from("-541068907326578960357406570305674893025")));

		Assertions.assertFalse(one.hasSameValueOf(two));
		Assertions.assertFalse(one.hasSameValueOf(oneNegative));
		Assertions.assertFalse(bigInteger.hasSameValueOf(Integer.from("44348653431046820678603668736201")));
	}

	@Test
	void primitiveValuesTest() {
		Assertions.assertEquals(0, zero.intValue());
		Assertions.assertEquals(1, one.intValue());
		Assertions.assertEquals(85, eightyFive.intValue());
		Assertions.assertEquals(-2, twoNegative.intValue());
		Assertions.assertEquals(-33, thirtyThreeNegative.intValue());

		Assertions.assertEquals(0L, zero.longValue());
		Assertions.assertEquals(5L, five.longValue());
		Assertions.assertEquals(7654825048765065875L, longValue.longValue());
		Assertions.assertEquals(-5489650234065703972L, longNegative.longValue());

		Assertions.assertEquals(0.0F, zero.floatValue(), FLOAT_PRECISION);
		Assertions.assertEquals(3.0F, three.floatValue(), FLOAT_PRECISION);
		Assertions.assertEquals(-3.0F, threeNegative.floatValue(), FLOAT_PRECISION);
		Assertions.assertEquals(-33.0F, thirtyThreeNegative.floatValue(), FLOAT_PRECISION);

		Assertions.assertEquals(0.0, zero.doubleValue(), DOUBLE_PRECISION);
		Assertions.assertEquals(4.0, four.doubleValue(), DOUBLE_PRECISION);
		Assertions.assertEquals(-3.0, threeNegative.doubleValue(), DOUBLE_PRECISION);
		Assertions.assertEquals(-541068907326578960357406570305674893025.0, bigNegativeInteger.doubleValue(),
								DOUBLE_PRECISION);
	}

	@Test
	void parentValuesTest() {
		Assertions.assertEquals(new BigDecimal("0"), zero.decimalValue());
		Assertions.assertEquals(new BigDecimal("85"), eightyFive.decimalValue());
		Assertions.assertEquals(new BigDecimal("-3"), threeNegative.decimalValue());
		Assertions.assertEquals(new BigDecimal("-541068907326578960357406570305674893025"),
								bigNegativeInteger.decimalValue());

		Assertions.assertEquals(zero, zero.numerator());
		Assertions.assertEquals(two, two.numerator());
		Assertions.assertEquals(bigInteger, bigInteger.numerator());
		Assertions.assertEquals(threeNegative, threeNegative.numerator());
		Assertions.assertEquals(longNegative, longNegative.numerator());

		Assertions.assertEquals(one, zero.denominator());
		Assertions.assertEquals(one, ten.denominator());
		Assertions.assertEquals(one, bigInteger.denominator());
		Assertions.assertEquals(one, oneNegative.denominator());
		Assertions.assertEquals(one, tenNegative.denominator());
		Assertions.assertEquals(one, longNegative.denominator());
	}

	@Test
	void absSignumTest() {
		Assertions.assertEquals(zero, zero.abs());
		Assertions.assertEquals(one, one.abs());
		Assertions.assertEquals(four, four.abs());
		Assertions.assertEquals(longValue, longValue.abs());

		Assertions.assertEquals(0, zero.signum());
		Assertions.assertEquals(1, three.signum());
		Assertions.assertEquals(1, bigInteger.signum());

		Assertions.assertEquals(Integer.from(1L), oneNegative.abs());
		Assertions.assertEquals(Integer.from(33L), thirtyThreeNegative.abs());
		Assertions.assertEquals(Integer.from(5489650234065703972L), longNegative.abs());

		Assertions.assertEquals(-1, twoNegative.signum());
		Assertions.assertEquals(-1, tenNegative.signum());
		Assertions.assertEquals(-1, bigNegativeInteger.signum());
	}

	@Test
	void addTest() {
		Assertions.assertEquals(zero, zero.add(zero));
		Assertions.assertEquals(ten, ten.add(zero));
		Assertions.assertEquals(ten, zero.add(ten));

		Assertions.assertEquals(five, two.add(three));
		Assertions.assertEquals(five, four.add(one));

		Assertions.assertEquals(threeNegative, oneNegative.add(twoNegative));
		Assertions.assertEquals(threeNegative, twoNegative.add(oneNegative));

		Assertions.assertEquals(three, five.add(twoNegative));
		Assertions.assertEquals(oneNegative, threeNegative.add(two));
	}

	@Test
	void subtractTest() {
		Assertions.assertEquals(zero, zero.subtract(zero));
		Assertions.assertEquals(ten, ten.subtract(zero));
		Assertions.assertEquals(threeNegative, zero.subtract(three));
		Assertions.assertEquals(two, zero.subtract(twoNegative));

		Assertions.assertEquals(three, five.subtract(two));
		Assertions.assertEquals(oneNegative, threeNegative.subtract(twoNegative));

		Assertions.assertEquals(five, two.subtract(threeNegative));
		Assertions.assertEquals(threeNegative, oneNegative.subtract(two));
	}

	@Test
	void negateTest() {
		Assertions.assertEquals(zero, zero.negate());
		Assertions.assertEquals(oneNegative, one.negate());
		Assertions.assertEquals(one, oneNegative.negate());
		Assertions.assertEquals(three, threeNegative.negate());
		Assertions.assertEquals(twoNegative, two.negate());
	}

	@Test
	void multiplyTest() {
		Assertions.assertEquals(zero, zero.multiply(eightyFive));
		Assertions.assertEquals(zero, thirtyThreeNegative.multiply(zero));

		Assertions.assertEquals(eightyFive, one.multiply(eightyFive));
		Assertions.assertEquals(thirtyThreeNegative, thirtyThreeNegative.multiply(one));

		Assertions.assertEquals(Integer.from(15L), three.multiply(five));

		Assertions.assertEquals(Integer.from(-12L), threeNegative.multiply(four));
		Assertions.assertEquals(Integer.from(-6L), three.multiply(twoNegative));

		Assertions.assertEquals(Integer.from(6L), twoNegative.multiply(threeNegative));
	}

	@Test
	void divideTest() {
		Assertions.assertTrue(Numbers.ZERO.hasSameValueOf(zero.divide(two)));
		Assertions.assertTrue(Numbers.ONE.hasSameValueOf(one.divide(one)));
		Assertions.assertTrue(Numbers.ONE.hasSameValueOf(five.divide(five)));
		Assertions.assertTrue(Numbers.ONE.hasSameValueOf(oneNegative.divide(oneNegative)));
		Assertions.assertTrue(Numbers.ONE.negate().hasSameValueOf(oneNegative.divide(one)));
		Assertions.assertTrue(Numbers.ONE.negate().hasSameValueOf(one.divide(oneNegative)));

		Assertions.assertTrue(Numbers.ONE_HALF.hasSameValueOf(one.divide(two)));
		Assertions.assertTrue(four.hasSameValueOf(four.divide(one)));
		Assertions.assertTrue(two.hasSameValueOf(four.divide(two)));

		Assertions.assertTrue(twoNegative.hasSameValueOf(four.divide(twoNegative)));
		Assertions.assertTrue(Integer.from("-221743267155234343716005103393018343681")
									 .hasSameValueOf(bigInteger.divide(Integer.from(-200L))));

		Assertions.assertTrue(two.hasSameValueOf(Integer.from("-4").divide(twoNegative)));

		Assertions.assertTrue(
				Rational.from("2424520948523583258523582346724953455224", "32353253580950232").hasSameValueOf(
						Integer.from("2424520948523583258523582346724953455224")
							   .divide(Integer.from("32353253580950232"))));
	}

	@Test
	void divideToIntegralValueTest() {
		Assertions.assertTrue(Numbers.ZERO.hasSameValueOf(zero.divideToIntegralValue(two)));
		Assertions.assertTrue(Numbers.ONE.hasSameValueOf(one.divideToIntegralValue(one)));
		Assertions.assertTrue(Numbers.ONE.hasSameValueOf(five.divideToIntegralValue(five)));
		Assertions.assertTrue(Numbers.ONE.hasSameValueOf(oneNegative.divideToIntegralValue(oneNegative)));
		Assertions.assertTrue(Numbers.ONE.negate().hasSameValueOf(oneNegative.divideToIntegralValue(one)));
		Assertions.assertTrue(Numbers.ONE.negate().hasSameValueOf(one.divideToIntegralValue(oneNegative)));

		Assertions.assertTrue(Numbers.ZERO.hasSameValueOf(one.divideToIntegralValue(two)));
		Assertions.assertTrue(four.hasSameValueOf(four.divideToIntegralValue(one)));
		Assertions.assertTrue(two.hasSameValueOf(four.divideToIntegralValue(two)));

		Assertions.assertTrue(twoNegative.hasSameValueOf(four.divideToIntegralValue(twoNegative)));
		Assertions.assertTrue(Integer.from("-221743267155234343716005103393018343681")
									 .hasSameValueOf(bigInteger.divideToIntegralValue(Integer.from(-200L))));

		Assertions.assertTrue(two.hasSameValueOf(Integer.from("-4").divideToIntegralValue(twoNegative)));

		Assertions.assertTrue(Integer.from("-183336657")
									 .hasSameValueOf(Integer.from("432534259038752")
															.divideToIntegralValue(Integer.from("-2359235"))));
	}

	@Test
	void remainderTest() {
		Assertions.assertTrue(Numbers.ZERO.hasSameValueOf(zero.remainder(two)));
		Assertions.assertTrue(Numbers.ONE.hasSameValueOf(one.remainder(two)));

		Assertions.assertEquals(Integer.from("32"), Integer.from("-432534259038752").remainder(Integer.from("64")));
		Assertions.assertEquals(Integer.from("2"), Integer.from("432534259038752").remainder(Integer.from("-33")));
	}

	@Test
	void inverseTest() {
		Assertions.assertEquals(three.inverse(), one.divide(three));
		Assertions.assertEquals(thirtyThreeNegative.inverse(), one.divide(thirtyThreeNegative));

		Assertions.assertEquals(bigInteger.inverse(), one.divide(bigInteger));
	}

	@Test
	void powTest() {
		Assertions.assertEquals(one, zero.pow(zero));
		Assertions.assertEquals(one, eightyFive.pow(zero));
		Assertions.assertEquals(one, thirtyThreeNegative.pow(zero));

		Assertions.assertEquals(Integer.from("1024"), two.pow(ten));
		Assertions.assertEquals(Integer.from("1024"), twoNegative.pow(ten));

		Assertions.assertEquals(Integer.from("-243"), threeNegative.pow(five));
	}

	@Test
	void gcdTest() {
		Assertions.assertTrue(Numbers.ONE.hasSameValueOf(five.gcd(one)));
		Assertions.assertTrue(eightyFive.hasSameValueOf(eightyFive.gcd(eightyFive)));
		Assertions.assertTrue(two.hasSameValueOf(four.gcd(two)));
		Assertions.assertTrue(two.hasSameValueOf(four.gcd(twoNegative)));

		Assertions.assertTrue(Integer.from(12L).hasSameValueOf(Integer.from(60L).gcd(Integer.from(84L))));
		Assertions.assertTrue(Integer.from(12L).hasSameValueOf(Integer.from(84L).gcd(Integer.from(60L))));
		Assertions.assertTrue(Integer.from(12L).hasSameValueOf(Integer.from(-84L).gcd(Integer.from(60L))));
		Assertions.assertTrue(Integer.from(12L).hasSameValueOf(Integer.from(-84L).gcd(Integer.from(-60L))));
		Assertions.assertTrue(Integer.from(12L).hasSameValueOf(Integer.from(84L).gcd(Integer.from(-60L))));

		Assertions.assertTrue(Integer.from(1L).hasSameValueOf(Integer.from(21L).gcd(Integer.from(10L))));
		Assertions.assertTrue(Integer.from(6L).hasSameValueOf(Integer.from(285648L).gcd(Integer.from(1654122L))));
		Assertions.assertTrue(Integer.from(24L).hasSameValueOf(Integer.from(1272L).gcd(Integer.from(1128L))));
	}

	@Test
	void lcmTest() {
		Assertions.assertTrue(Integer.from(5).hasSameValueOf(five.lcm(one)));
		Assertions.assertTrue(Integer.from(5).hasSameValueOf(one.lcm(five)));

		Assertions.assertTrue(Integer.from(0L).hasSameValueOf(Integer.from(0L).lcm(Integer.from(63L))));
		Assertions.assertTrue(Integer.from(0L).hasSameValueOf(Integer.from(63L).lcm(Integer.from(0L))));

		Assertions.assertTrue(Integer.from(75L).hasSameValueOf(Integer.from(1L).lcm(Integer.from(75L))));

		Assertions.assertTrue(Integer.from(24L).hasSameValueOf(Integer.from(6L).lcm(Integer.from(8L))));
		Assertions.assertTrue(Integer.from(24L).hasSameValueOf(Integer.from(8L).lcm(Integer.from(6L))));
		Assertions.assertTrue(Integer.from(24L).hasSameValueOf(Integer.from(-8L).lcm(Integer.from(6L))));
		Assertions.assertTrue(Integer.from(24L).hasSameValueOf(Integer.from(-8L).lcm(Integer.from(-6L))));
		Assertions.assertTrue(Integer.from(24L).hasSameValueOf(Integer.from(8L).lcm(Integer.from(-6L))));
	}

	@Test
	void equalsAndHashcodeTest() {
		Assertions.assertEquals(zero, zero);
		Assertions.assertEquals(zero.hashCode(), zero.hashCode());
		Assertions.assertEquals(one, Integer.from("1"));
		Assertions.assertEquals(Integer.from("1").hashCode(), one.hashCode());

		Assertions.assertEquals(eightyFive, Integer.from("85"));
		Assertions.assertEquals(Integer.from("85").hashCode(), eightyFive.hashCode());
		Assertions.assertEquals(thirtyThreeNegative, Integer.from("-33"));
		Assertions.assertEquals(Integer.from("-33").hashCode(), thirtyThreeNegative.hashCode());

		Assertions.assertEquals(Integer.from((BigInteger) null), Integer.from((BigInteger) null));

		Assertions.assertNotEquals(one, zero);
		Assertions.assertNotEquals(zero, one);
		Assertions.assertNotEquals(oneNegative, one);

		Assertions.assertNotEquals(null, eightyFive);
		Assertions.assertNotEquals(Integer.from((BigInteger) null), eightyFive);
	}
}
