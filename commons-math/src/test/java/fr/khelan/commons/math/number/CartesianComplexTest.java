package fr.khelan.commons.math.number;

import java.math.BigDecimal;
import java.math.MathContext;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CartesianComplexTest {
	private final CartesianComplex zero;
	private final CartesianComplex one;
	private final CartesianComplex i;
	private final CartesianComplex a1;
	private final CartesianComplex a2;
	private final CartesianComplex a3;
	private final CartesianComplex b1;
	private final CartesianComplex b2;
	private final CartesianComplex c1;
	private final CartesianComplex c2;
	private final CartesianComplex d1;
	private final CartesianComplex e1;
	private final CartesianComplex f1;
	private final CartesianComplex g1;
	private final CartesianComplex h1;

	CartesianComplexTest() {
		zero = new CartesianComplex(Numbers.ZERO);
		one = new CartesianComplex(Numbers.ONE);
		i = new CartesianComplex(Numbers.ZERO, Numbers.ONE);
		a1 = new CartesianComplex(realValue(3), realValue(4));
		a2 = new CartesianComplex(realValue("3"), realValue("4"));
		a3 = new CartesianComplex(realValue("3.00"), realValue(4));
		b1 = new CartesianComplex(realValue(4), realValue(-3));
		b2 = new CartesianComplex(realValue("4.0"), realValue("-3.0"));
		c1 = new CartesianComplex(realValue(5), realValue(0));
		c2 = new CartesianComplex(realValue("5"));
		d1 = new CartesianComplex(realValue("1"));
		e1 = new CartesianComplex(Numbers.ZERO, realValue(1));
		f1 = new CartesianComplex(Numbers.ZERO, realValue(7));
		g1 = new CartesianComplex(realValue(5), realValue(-2));
		h1 = new CartesianComplex(realValue("-658978.556"), realValue("-263680.587"));
	}

	private static final Real realValue(final int integer) {
		return Real.from(new BigDecimal(integer));
	}

	private static final Real realValue(final String decimal) {
		return Real.from(new BigDecimal(decimal));
	}

	@Test
	void hasSameValueOfTest() {
		Assertions.assertTrue(zero.hasSameValueOf(Numbers.ZERO));

		Assertions.assertTrue(a1.hasSameValueOf(a2));
		Assertions.assertTrue(a1.hasSameValueOf(a3));
		Assertions.assertTrue(a2.hasSameValueOf(a3));

		Assertions.assertTrue(b1.hasSameValueOf(b2));

		Assertions.assertTrue(c1.hasSameValueOf(c2));

		Assertions.assertTrue(d1.hasSameValueOf(Numbers.ONE));

		Assertions.assertTrue(e1.hasSameValueOf(Numbers.I));

		Assertions.assertFalse(zero.hasSameValueOf(Numbers.ONE));
		Assertions.assertFalse(zero.hasSameValueOf(Numbers.I));
		Assertions.assertFalse(a1.hasSameValueOf(b1));
		Assertions.assertFalse(a2.hasSameValueOf(b1));
		Assertions.assertFalse(a3.hasSameValueOf(b1));
		Assertions.assertFalse(a3.hasSameValueOf(b2));
		Assertions.assertFalse(a1.hasSameValueOf(c1));
		Assertions.assertFalse(a1.hasSameValueOf(d1));
		Assertions.assertFalse(a1.hasSameValueOf(e1));
		Assertions.assertFalse(a1.hasSameValueOf(f1));
		Assertions.assertFalse(a1.hasSameValueOf(f1));
		Assertions.assertFalse(b1.hasSameValueOf(c1));
		Assertions.assertFalse(b1.hasSameValueOf(d1));
		Assertions.assertFalse(b1.hasSameValueOf(e1));
		Assertions.assertFalse(b1.hasSameValueOf(f1));
		Assertions.assertFalse(c1.hasSameValueOf(d1));
		Assertions.assertFalse(c1.hasSameValueOf(e1));
		Assertions.assertFalse(d1.hasSameValueOf(e1));
		Assertions.assertFalse(d1.hasSameValueOf(f1));
		Assertions.assertFalse(d1.hasSameValueOf(zero));
		Assertions.assertFalse(d1.hasSameValueOf(Numbers.I));
		Assertions.assertFalse(f1.hasSameValueOf(Numbers.I));
	}

	@Test
	void isRealTest() {
		Assertions.assertTrue(zero.isReal());
		Assertions.assertTrue(one.isReal());
		Assertions.assertTrue(c1.isReal());
		Assertions.assertTrue(c2.isReal());
		Assertions.assertTrue(d1.isReal());

		Assertions.assertFalse(i.isReal());
		Assertions.assertFalse(a1.isReal());
		Assertions.assertFalse(a2.isReal());
		Assertions.assertFalse(a3.isReal());
		Assertions.assertFalse(b1.isReal());
		Assertions.assertFalse(b2.isReal());
		Assertions.assertFalse(e1.isReal());
		Assertions.assertFalse(f1.isReal());
	}

	@Test
	void isImaginaryTest() {
		Assertions.assertTrue(zero.isImaginary());
		Assertions.assertTrue(i.isImaginary());
		Assertions.assertTrue(e1.isImaginary());
		Assertions.assertTrue(f1.isImaginary());

		Assertions.assertFalse(one.isImaginary());
		Assertions.assertFalse(a1.isImaginary());
		Assertions.assertFalse(a2.isImaginary());
		Assertions.assertFalse(a3.isImaginary());
		Assertions.assertFalse(b1.isImaginary());
		Assertions.assertFalse(b2.isImaginary());
		Assertions.assertFalse(c1.isImaginary());
		Assertions.assertFalse(c2.isImaginary());
		Assertions.assertFalse(d1.isImaginary());
	}

	@Test
	void isZeroTest() {
		Assertions.assertTrue(zero.isZero());

		Assertions.assertFalse(one.isZero());
		Assertions.assertFalse(i.isZero());
		Assertions.assertFalse(a1.isZero());
		Assertions.assertFalse(a2.isZero());
		Assertions.assertFalse(a3.isZero());
		Assertions.assertFalse(b1.isZero());
		Assertions.assertFalse(b2.isZero());
		Assertions.assertFalse(c1.isZero());
		Assertions.assertFalse(c2.isZero());
		Assertions.assertFalse(d1.isZero());
		Assertions.assertFalse(e1.isZero());
		Assertions.assertFalse(f1.isZero());
	}

	@Test
	void realImaginaryTest() {
		Assertions.assertEquals(0, zero.real().compareTo(Numbers.ZERO));
		Assertions.assertEquals(0, zero.imaginary().compareTo(Numbers.ZERO));
		Assertions.assertEquals(0, one.real().compareTo(Numbers.ONE));
		Assertions.assertEquals(0, one.imaginary().compareTo(Numbers.ZERO));
		Assertions.assertEquals(0, i.real().compareTo(Numbers.ZERO));
		Assertions.assertEquals(0, i.imaginary().compareTo(Numbers.ONE));

		Assertions.assertEquals(0, a1.real().compareTo(realValue(3)));
		Assertions.assertEquals(0, a1.imaginary().compareTo(realValue(4)));
		Assertions.assertEquals(0, a2.real().compareTo(realValue(3)));
		Assertions.assertEquals(0, a2.imaginary().compareTo(realValue(4)));

		Assertions.assertEquals(0, b1.real().compareTo(realValue(4)));
		Assertions.assertEquals(0, b1.imaginary().compareTo(realValue(-3)));
		Assertions.assertEquals(0, b2.real().compareTo(realValue(4)));
		Assertions.assertEquals(0, b2.imaginary().compareTo(realValue(-3)));

		Assertions.assertEquals(0, c1.real().compareTo(realValue(5)));
		Assertions.assertEquals(0, c1.imaginary().compareTo(realValue(0)));

		Assertions.assertEquals(0, f1.real().compareTo(realValue(0)));
		Assertions.assertEquals(0, f1.imaginary().compareTo(realValue(7)));
	}

	@Test
	void equalsAndHashcodeTest() {
		Assertions.assertEquals(a1, a2);
		Assertions.assertEquals(a1.hashCode(), a2.hashCode());

		Assertions.assertNotEquals(a1, a3);
		Assertions.assertNotEquals(a3.hashCode(), a1.hashCode());

		Assertions.assertNotEquals(a2, a3);
		Assertions.assertNotEquals(a3.hashCode(), a2.hashCode());

		Assertions.assertNotEquals(a1, b1);
		Assertions.assertNotEquals(b1.hashCode(), a1.hashCode());
	}

	@Test
	void modulusTest() {
		Assertions.assertEquals(0, zero.modulus().compareTo(Numbers.ZERO));
		Assertions.assertEquals(0, one.modulus().compareTo(Numbers.ONE));
		Assertions.assertEquals(0, i.modulus().compareTo(Numbers.ONE));

		Assertions.assertEquals(0, a1.modulus().compareTo(realValue(5)));
		Assertions.assertEquals(0, a2.modulus().compareTo(realValue(5)));
		Assertions.assertEquals(0, a3.modulus().compareTo(realValue(5)));
		Assertions.assertEquals(0, b1.modulus().compareTo(realValue(5)));
		Assertions.assertEquals(0, b2.modulus().compareTo(realValue(5)));
		Assertions.assertEquals(0, c1.modulus().compareTo(realValue(5)));

		Assertions.assertEquals(0, g1.modulus().realValue().round(MathContext.DECIMAL128)
									 .compareTo(new BigDecimal("5.38516480713450403125071049154033")));

		Assertions.assertEquals(0, h1.modulus().realValue().round(MathContext.DECIMAL128)
									 .compareTo(new BigDecimal("709774.7454147052878219973325850544")));
	}

	@Test
	void argumentTest() {
		Assertions.assertEquals(0.0, zero.argument().doubleValue(), 0.000001);
		Assertions.assertTrue(zero.argumentPi().hasSameValueOf(Numbers.ZERO));
		Assertions.assertEquals(0.0, one.argument().doubleValue(), 0.000001);
		Assertions.assertTrue(one.argumentPi().hasSameValueOf(Numbers.ZERO));
		Assertions.assertEquals(Math.PI, one.negate().argument().doubleValue(), 0.000001);
		Assertions.assertTrue(one.negate().argumentPi().hasSameValueOf(Numbers.ONE));
		Assertions.assertEquals(Math.PI / 2, i.argument().doubleValue(),
								0.000001);
		Assertions.assertTrue(i.argumentPi().hasSameValueOf(Numbers.ONE_HALF));
		Assertions.assertEquals(-Math.PI / 2, i.negate().argument().doubleValue(), 0.000001);
		Assertions.assertTrue(i.negate().argumentPi().hasSameValueOf(Numbers.ONE_HALF.negate()));

		Assertions.assertEquals(0.9272952180016, a1.argument().doubleValue(), 0.000001);
		Assertions.assertEquals(0.9272952180016, a2.argument().doubleValue(), 0.000001);
		Assertions.assertEquals(0.9272952180016, a3.argument().doubleValue(), 0.000001);

		Assertions.assertEquals(-0.64350110879328, b1.argument().doubleValue(), 0.000001);
		Assertions.assertEquals(-0.64350110879328, b2.argument().doubleValue(), 0.000001);

		Assertions.assertEquals(0.0, c1.argument().doubleValue(), 0.000001);
		Assertions.assertTrue(c1.argumentPi().hasSameValueOf(Numbers.ZERO));
		Assertions.assertEquals(Math.PI, c1.negate().argument().doubleValue(), 0.000001);
		Assertions.assertTrue(c1.negate().argumentPi().hasSameValueOf(Numbers.ONE));

		Assertions.assertEquals(Math.PI / 2, f1.argument().doubleValue(), 0.000001);
		Assertions.assertTrue(f1.argumentPi().hasSameValueOf(Numbers.ONE_HALF));
		Assertions.assertEquals(-Math.PI / 2, f1.negate().argument().doubleValue(), 0.000001);
		Assertions.assertTrue(f1.negate().argumentPi().hasSameValueOf(Numbers.ONE_HALF.negate()));

		Assertions.assertEquals(-2.7609696377079, h1.argument().doubleValue(), 0.0000000000001);
		Assertions.assertEquals(-0.8788439311357, h1.argumentPi().doubleValue(), 0.0000000000001);
	}

	@Test
	void conjugateTest() {
		Assertions.assertTrue(zero.conjugate().hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(one.conjugate().hasSameValueOf(Numbers.ONE));
		Assertions.assertTrue(i.conjugate().hasSameValueOf(Numbers.I.negate()));

		Assertions.assertTrue(a1.conjugate().hasSameValueOf(new CartesianComplex(realValue(3), realValue(-4))));
		Assertions.assertTrue(a2.conjugate().hasSameValueOf(new CartesianComplex(realValue(3), realValue(-4))));
		Assertions.assertTrue(a3.conjugate().hasSameValueOf(new CartesianComplex(realValue(3), realValue(-4))));

		Assertions.assertTrue(b1.conjugate().hasSameValueOf(new CartesianComplex(realValue(4), realValue(3))));

		Assertions.assertTrue(c1.conjugate().hasSameValueOf(c1));

		Assertions.assertTrue(
				h1.conjugate().hasSameValueOf(new CartesianComplex(realValue("-658978.556"), realValue("263680.587"))));
	}

	@Test
	void negateTest() {
		Assertions.assertTrue(zero.negate().hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(one.negate().hasSameValueOf(new CartesianComplex(realValue(-1))));
		Assertions.assertTrue(i.negate().hasSameValueOf(new CartesianComplex(Numbers.ZERO, realValue(-1))));

		Assertions.assertTrue(a1.negate().hasSameValueOf(new CartesianComplex(realValue(-3), realValue(-4))));
		Assertions.assertTrue(a2.negate().hasSameValueOf(new CartesianComplex(realValue(-3), realValue(-4))));
		Assertions.assertTrue(a3.negate().hasSameValueOf(new CartesianComplex(realValue(-3), realValue(-4))));

		Assertions.assertTrue(b1.negate().hasSameValueOf(new CartesianComplex(realValue(-4), realValue(3))));

		Assertions.assertTrue(
				h1.negate().hasSameValueOf(new CartesianComplex(realValue("658978.556"), realValue("263680.587"))));
	}

	@Test
	void addTest() {
		Assertions.assertTrue(zero.add(one).hasSameValueOf(Numbers.ONE));
		Assertions.assertTrue(zero.add(i).hasSameValueOf(Numbers.I));
		Assertions.assertTrue(one.add(i).hasSameValueOf(new CartesianComplex(realValue(1), realValue(1))));

		Assertions.assertTrue(a1.add(Numbers.ZERO).hasSameValueOf(a1));
		Assertions.assertTrue(b1.add(Numbers.ZERO).hasSameValueOf(b1));
		Assertions.assertTrue(c1.add(Numbers.ZERO).hasSameValueOf(c1));
		Assertions.assertTrue(d1.add(Numbers.ZERO).hasSameValueOf(d1));
		Assertions.assertTrue(e1.add(Numbers.ZERO).hasSameValueOf(e1));
		Assertions.assertTrue(f1.add(Numbers.ZERO).hasSameValueOf(f1));
		Assertions.assertTrue(g1.add(Numbers.ZERO).hasSameValueOf(g1));
		Assertions.assertTrue(h1.add(Numbers.ZERO).hasSameValueOf(h1));

		Assertions.assertTrue(a1.add(b1).hasSameValueOf(new CartesianComplex(realValue(7), realValue(1))));
		Assertions.assertTrue(a2.add(b1).hasSameValueOf(new CartesianComplex(realValue(7), realValue(1))));
		Assertions.assertTrue(a1.add(b2).hasSameValueOf(new CartesianComplex(realValue(7), realValue(1))));

		Assertions.assertTrue(c1.add(d1).hasSameValueOf(new CartesianComplex(realValue(6))));
		Assertions.assertTrue(c1.add(f1).hasSameValueOf(new CartesianComplex(realValue(5), realValue(7))));
		Assertions.assertTrue(e1.add(f1).hasSameValueOf(new CartesianComplex(realValue(0), realValue(8))));
		Assertions.assertTrue(
				g1.add(h1).hasSameValueOf(new CartesianComplex(realValue("-658973.556"), realValue("-263682.587"))));
	}

	@Test
	void subtractTest() {
		Assertions.assertTrue(one.subtract(Numbers.ZERO).hasSameValueOf(Numbers.ONE));
		Assertions.assertTrue(i.subtract(Numbers.ZERO).hasSameValueOf(Numbers.I));
		Assertions.assertTrue(
				one.subtract(Numbers.I).hasSameValueOf(new CartesianComplex(realValue(1), realValue(-1))));

		Assertions.assertTrue(a1.subtract(Numbers.ZERO).hasSameValueOf(a1));
		Assertions.assertTrue(b1.subtract(Numbers.ZERO).hasSameValueOf(b1));
		Assertions.assertTrue(c1.subtract(Numbers.ZERO).hasSameValueOf(c1));
		Assertions.assertTrue(d1.subtract(Numbers.ZERO).hasSameValueOf(d1));
		Assertions.assertTrue(e1.subtract(Numbers.ZERO).hasSameValueOf(e1));
		Assertions.assertTrue(f1.subtract(Numbers.ZERO).hasSameValueOf(f1));
		Assertions.assertTrue(g1.subtract(Numbers.ZERO).hasSameValueOf(g1));
		Assertions.assertTrue(h1.subtract(Numbers.ZERO).hasSameValueOf(h1));

		Assertions.assertTrue(a1.subtract(b1).hasSameValueOf(new CartesianComplex(realValue(-1), realValue(7))));
		Assertions.assertTrue(a2.subtract(b1).hasSameValueOf(new CartesianComplex(realValue(-1), realValue(7))));
		Assertions.assertTrue(a1.subtract(b2).hasSameValueOf(new CartesianComplex(realValue(-1), realValue(7))));

		Assertions.assertTrue(c1.subtract(d1).hasSameValueOf(new CartesianComplex(realValue(4))));
		Assertions.assertTrue(c1.subtract(f1).hasSameValueOf(new CartesianComplex(realValue(5), realValue(-7))));
		Assertions.assertTrue(e1.subtract(f1).hasSameValueOf(new CartesianComplex(realValue(0), realValue(-6))));
		Assertions.assertTrue(
				g1.subtract(h1).hasSameValueOf(new CartesianComplex(realValue("658983.556"), realValue("263678.587"))));
	}

	@Test
	void multiplyComplexTest() {
		Assertions.assertTrue(zero.multiply(Numbers.ZERO).hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(one.multiply(Numbers.ZERO).hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(i.multiply(Numbers.ZERO).hasSameValueOf(Numbers.ZERO));

		Assertions.assertTrue(one.multiply(Numbers.I).hasSameValueOf(Numbers.I));
		Assertions.assertTrue(i.multiply(Numbers.ONE).hasSameValueOf(Numbers.I));

		Assertions.assertTrue(i.multiply(Numbers.I).hasSameValueOf(Numbers.ONE.negate()));

		Assertions.assertTrue(a1.multiply(Numbers.ZERO).hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(b1.multiply(Numbers.ZERO).hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(c1.multiply(Numbers.ZERO).hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(d1.multiply(Numbers.ZERO).hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(e1.multiply(Numbers.ZERO).hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(f1.multiply(Numbers.ZERO).hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(g1.multiply(Numbers.ZERO).hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(h1.multiply(Numbers.ZERO).hasSameValueOf(Numbers.ZERO));

		Assertions.assertTrue(a1.multiply(Numbers.ONE).hasSameValueOf(a1));
		Assertions.assertTrue(b1.multiply(Numbers.ONE).hasSameValueOf(b1));
		Assertions.assertTrue(c1.multiply(Numbers.ONE).hasSameValueOf(c1));
		Assertions.assertTrue(d1.multiply(Numbers.ONE).hasSameValueOf(d1));
		Assertions.assertTrue(e1.multiply(Numbers.ONE).hasSameValueOf(e1));
		Assertions.assertTrue(f1.multiply(Numbers.ONE).hasSameValueOf(f1));
		Assertions.assertTrue(g1.multiply(Numbers.ONE).hasSameValueOf(g1));
		Assertions.assertTrue(h1.multiply(Numbers.ONE).hasSameValueOf(h1));

		Assertions.assertTrue(a1.multiply(b1).hasSameValueOf(new CartesianComplex(realValue(24), realValue(7))));
		Assertions.assertTrue(a2.multiply(b2).hasSameValueOf(new CartesianComplex(realValue(24), realValue(7))));
		Assertions.assertTrue(a3.multiply(b1).hasSameValueOf(new CartesianComplex(realValue(24), realValue(7))));

		Assertions.assertTrue(c1.multiply(d1).hasSameValueOf(new CartesianComplex(realValue(5))));
		Assertions.assertTrue(c1.multiply(f1).hasSameValueOf(new CartesianComplex(Numbers.ZERO, realValue(35))));
		Assertions.assertTrue(e1.multiply(f1).hasSameValueOf(new CartesianComplex(realValue(-7))));
		Assertions.assertTrue(
				g1.multiply(h1).hasSameValueOf(new CartesianComplex(realValue("-3822253.954"), realValue("-445.823"))));
	}

	@Test
	void divideComplexTest() {
		Assertions.assertTrue(zero.divide(Numbers.ONE).hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(one.divide(Numbers.ONE).hasSameValueOf(Numbers.ONE));
		Assertions.assertTrue(i.divide(Numbers.ONE).hasSameValueOf(Numbers.I));
		Assertions.assertTrue(zero.divide(Numbers.I).hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(one.divide(Numbers.I).hasSameValueOf(Numbers.I.negate()));
		Assertions.assertTrue(i.divide(Numbers.I).hasSameValueOf(Numbers.ONE));

		Assertions.assertTrue(a1.divide(Numbers.ONE).hasSameValueOf(a1));
		Assertions.assertTrue(b1.divide(Numbers.ONE).hasSameValueOf(b1));
		Assertions.assertTrue(c1.divide(Numbers.ONE).hasSameValueOf(c1));
		Assertions.assertTrue(d1.divide(Numbers.ONE).hasSameValueOf(d1));
		Assertions.assertTrue(e1.divide(Numbers.ONE).hasSameValueOf(e1));
		Assertions.assertTrue(f1.divide(Numbers.ONE).hasSameValueOf(f1));
		Assertions.assertTrue(g1.divide(Numbers.ONE).hasSameValueOf(g1));
		Assertions.assertTrue(h1.divide(Numbers.ONE).hasSameValueOf(h1));

		Assertions.assertTrue(a1.divide(b1).hasSameValueOf(Numbers.I));
		Assertions.assertTrue(a2.divide(b2).hasSameValueOf(Numbers.I));
		Assertions.assertTrue(a3.divide(b1).hasSameValueOf(Numbers.I));

		Assertions.assertTrue(d1.divide(c1).hasSameValueOf(new CartesianComplex(realValue("0.2"))));

		Complex division = c1.divide(f1);
		Assertions.assertTrue(division.real().hasSameValueOf(Numbers.ZERO));
		Assertions.assertEquals(0, division.imaginary().realValue().round(MathContext.DECIMAL128)
										   .compareTo(new BigDecimal("-0.7142857142857142857142857142857143")));

		division = e1.divide(f1);
		Assertions.assertEquals(0, division.real().realValue().round(MathContext.DECIMAL128)
										   .compareTo(new BigDecimal("0.1428571428571428571428571428571429")));
		Assertions.assertTrue(division.imaginary().hasSameValueOf(Numbers.ZERO));

		division = h1.divide(g1);
		Assertions.assertEquals(0, division.real().realValue().round(MathContext.DECIMAL128)
										   .compareTo(new BigDecimal("-95432.12434482758620689655172413793")));
		Assertions.assertEquals(0, division.imaginary().realValue().round(MathContext.DECIMAL128)
										   .compareTo(new BigDecimal("-90908.96713793103448275862068965517")));
	}

	@Test
	void inverseTest() {
		Assertions.assertTrue(one.inverse().hasSameValueOf(Numbers.ONE));
		Assertions.assertTrue(i.inverse().hasSameValueOf(Numbers.I.negate()));

		Assertions.assertTrue(a1.inverse().hasSameValueOf(Numbers.ONE.divide(a1)));
		Assertions.assertTrue(a2.inverse().hasSameValueOf(Numbers.ONE.divide(a1)));
		Assertions.assertTrue(a3.inverse().hasSameValueOf(Numbers.ONE.divide(a1)));
		Assertions.assertTrue(b1.inverse().hasSameValueOf(Numbers.ONE.divide(b1)));
		Assertions.assertTrue(b2.inverse().hasSameValueOf(Numbers.ONE.divide(b1)));
		Assertions.assertTrue(c1.inverse().hasSameValueOf(Numbers.ONE.divide(c1)));
		Assertions.assertTrue(d1.inverse().hasSameValueOf(Numbers.ONE.divide(d1)));
		Assertions.assertTrue(e1.inverse().hasSameValueOf(Numbers.ONE.divide(e1)));
		Assertions.assertTrue(f1.inverse().hasSameValueOf(Numbers.ONE.divide(f1)));
		Assertions.assertTrue(g1.inverse().hasSameValueOf(Numbers.ONE.divide(g1)));
		Assertions.assertTrue(h1.inverse().hasSameValueOf(Numbers.ONE.divide(h1)));
	}

	@Test
	void equalsTest() {
		Assertions.assertEquals(zero, zero);
		Assertions.assertEquals(one, one);
		Assertions.assertEquals(i, i);
		Assertions.assertEquals(h1, h1);

		Assertions.assertFalse(one.equals(null));
		Assertions.assertFalse(i.equals(null));
		Assertions.assertFalse(one.equals(Numbers.ONE));

		Assertions.assertEquals(new CartesianComplex(realValue(5), realValue(-2)), g1);
		Assertions.assertEquals(new CartesianComplex(realValue("-658978.556"), realValue("-263680.587")), h1);

		Assertions.assertEquals(new CartesianComplex(realValue("-36540"), null),
								new CartesianComplex(realValue(-36540), null));
		Assertions.assertEquals(new CartesianComplex(null, realValue("-36540")),
								new CartesianComplex(null, realValue(-36540)));

		Assertions.assertNotEquals(null, g1);

		Assertions.assertNotEquals(zero, a1);
		Assertions.assertNotEquals(new CartesianComplex(realValue(5), realValue(-1)), g1);
		Assertions.assertNotEquals(new CartesianComplex(realValue(4), realValue(-2)), g1);
		Assertions.assertNotEquals(new CartesianComplex(realValue("-658978.556"), realValue("-263680.585")), h1);
		Assertions.assertNotEquals(new CartesianComplex(realValue("-658978.555"), realValue("-263680.587")), h1);

		Assertions.assertNotEquals(new CartesianComplex(realValue("-36540"), null),
								   new CartesianComplex(realValue(-36540), realValue(0)));
		Assertions.assertNotEquals(new CartesianComplex(null, realValue("-36540")),
								   new CartesianComplex(realValue(0), realValue(-36540)));
	}
}
