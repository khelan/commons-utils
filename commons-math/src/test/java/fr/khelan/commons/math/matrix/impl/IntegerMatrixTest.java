package fr.khelan.commons.math.matrix.impl;

import fr.khelan.commons.math.matrix.Matrix;
import fr.khelan.commons.math.matrix.exception.MultiplicationNotAvailableException;
import fr.khelan.commons.math.matrix.exception.NotSquareMatrixException;
import fr.khelan.commons.math.matrix.exception.OutOfMatrixBoundsException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class IntegerMatrixTest {
	private Matrix<Integer> matrix1, matrix2, squareMatrix, squareMatrix2, squareMatrix3;

	@BeforeEach
	void init() {
		matrix1 = new IntegerMatrix(4, 3);
		matrix1.set(1, 0, 0);
		matrix1.set(2, 0, 1);
		matrix1.set(3, 0, 2);
		matrix1.set(4, 1, 0);
		matrix1.set(5, 1, 1);
		matrix1.set(6, 1, 2);
		matrix1.set(7, 2, 0);
		matrix1.set(8, 2, 1);
		matrix1.set(9, 2, 2);
		matrix1.set(10, 3, 0);
		matrix1.set(11, 3, 1);
		matrix1.set(12, 3, 2);

		matrix2 = new IntegerMatrix(4, 3);
		matrix2.set(21, 0, 0);
		matrix2.set(23, 0, 1);
		matrix2.set(25, 0, 2);
		matrix2.set(27, 1, 0);
		matrix2.set(29, 1, 1);
		matrix2.set(31, 1, 2);
		matrix2.set(33, 2, 0);
		matrix2.set(35, 2, 1);
		matrix2.set(37, 2, 2);
		matrix2.set(39, 3, 0);
		matrix2.set(41, 3, 1);
		matrix2.set(43, 3, 2);

		squareMatrix = new IntegerMatrix(3, 3);
		squareMatrix.set(99, 0, 0);
		squareMatrix.set(88, 0, 1);
		squareMatrix.set(77, 0, 2);
		squareMatrix.set(66, 1, 0);
		squareMatrix.set(55, 1, 1);
		squareMatrix.set(44, 1, 2);
		squareMatrix.set(33, 2, 0);
		squareMatrix.set(22, 2, 1);
		squareMatrix.set(11, 2, 2);

		squareMatrix2 = new IntegerMatrix(3, 3);
		squareMatrix2.set(4, 0, 0);
		squareMatrix2.set(4, 0, 1);
		squareMatrix2.set(2, 0, 2);
		squareMatrix2.set(2, 1, 0);
		squareMatrix2.set(5, 1, 1);
		squareMatrix2.set(1, 1, 2);
		squareMatrix2.set(1, 2, 0);
		squareMatrix2.set(3, 2, 1);
		squareMatrix2.set(2, 2, 2);

		squareMatrix3 = new IntegerMatrix(4, 4);
		squareMatrix3.set(4, 0, 0);
		squareMatrix3.set(4, 0, 1);
		squareMatrix3.set(2, 0, 2);
		squareMatrix3.set(1, 0, 3);
		squareMatrix3.set(7, 1, 0);
		squareMatrix3.set(5, 1, 1);
		squareMatrix3.set(3, 1, 2);
		squareMatrix3.set(1, 1, 3);
		squareMatrix3.set(2, 2, 0);
		squareMatrix3.set(1, 2, 1);
		squareMatrix3.set(2, 2, 2);
		squareMatrix3.set(3, 2, 3);
		squareMatrix3.set(1, 3, 0);
		squareMatrix3.set(3, 3, 1);
		squareMatrix3.set(2, 3, 2);
		squareMatrix3.set(1, 3, 3);
	}

	@Test
	void getSetMatrixTest() {
		Assertions.assertEquals(Integer.valueOf(4), matrix1.getRowNumber());
		Assertions.assertEquals(Integer.valueOf(3), matrix1.getColumnNumber());
		Assertions.assertEquals(Integer.valueOf(7), matrix1.get(2, 0));
	}

	@Test
	void mapTest() {
		final Matrix<Integer> result = matrix1.map(i -> i + 11);

		Assertions.assertEquals(Integer.valueOf(3), matrix1.get(0, 2));

		Assertions.assertEquals(Integer.valueOf(14), result.get(0, 2));
		Assertions.assertEquals(Integer.valueOf(16), result.get(1, 1));
		Assertions.assertEquals(Integer.valueOf(17), result.get(1, 2));
		Assertions.assertEquals(Integer.valueOf(22), result.get(3, 1));
		Assertions.assertEquals(Integer.valueOf(23), result.get(3, 2));
	}

	@Test
	void addScalarTest() {
		final Matrix<Integer> result = matrix1.add(11);

		Assertions.assertEquals(Integer.valueOf(3), matrix1.get(0, 2));

		Assertions.assertEquals(Integer.valueOf(14), result.get(0, 2));
		Assertions.assertEquals(Integer.valueOf(16), result.get(1, 1));
		Assertions.assertEquals(Integer.valueOf(17), result.get(1, 2));
		Assertions.assertEquals(Integer.valueOf(22), result.get(3, 1));
		Assertions.assertEquals(Integer.valueOf(23), result.get(3, 2));
	}

	@Test
	void addTest() {
		final Matrix<Integer> result = matrix1.add(matrix2);

		Assertions.assertEquals(Integer.valueOf(3), matrix1.get(0, 2));
		Assertions.assertEquals(Integer.valueOf(25), matrix2.get(0, 2));

		Assertions.assertEquals(Integer.valueOf(28), result.get(0, 2));
		Assertions.assertEquals(Integer.valueOf(34), result.get(1, 1));
		Assertions.assertEquals(Integer.valueOf(37), result.get(1, 2));
		Assertions.assertEquals(Integer.valueOf(52), result.get(3, 1));
		Assertions.assertEquals(Integer.valueOf(55), result.get(3, 2));
	}

	@Test
	void subtractScalarTest() {
		final Matrix<Integer> result = matrix1.subtract(8);

		Assertions.assertEquals(Integer.valueOf(3), matrix1.get(0, 2));

		Assertions.assertEquals(Integer.valueOf(-5), result.get(0, 2));
		Assertions.assertEquals(Integer.valueOf(-3), result.get(1, 1));
		Assertions.assertEquals(Integer.valueOf(-2), result.get(1, 2));
		Assertions.assertEquals(Integer.valueOf(3), result.get(3, 1));
		Assertions.assertEquals(Integer.valueOf(4), result.get(3, 2));
	}

	@Test
	void subtractTest() {
		final Matrix<Integer> result = matrix1.subtract(matrix2);

		Assertions.assertEquals(Integer.valueOf(3), matrix1.get(0, 2));
		Assertions.assertEquals(Integer.valueOf(25), matrix2.get(0, 2));

		Assertions.assertEquals(Integer.valueOf(-22), result.get(0, 2));
		Assertions.assertEquals(Integer.valueOf(-24), result.get(1, 1));
		Assertions.assertEquals(Integer.valueOf(-25), result.get(1, 2));
		Assertions.assertEquals(Integer.valueOf(-30), result.get(3, 1));
		Assertions.assertEquals(Integer.valueOf(-31), result.get(3, 2));
	}

	@Test
	void scalarProductTest() {
		final Matrix<Integer> m1 = new IntegerMatrix(3, 1);
		m1.set(3, 0, 0);
		m1.set(5, 1, 0);
		m1.set(7, 2, 0);

		final Matrix<Integer> m2 = new IntegerMatrix(1, 3);
		m2.set(2, 0, 0);
		m2.set(4, 0, 1);
		m2.set(6, 0, 2);

		Assertions.assertEquals(Integer.valueOf(68), m2.scalarProduct(m1));
	}

	@Test
	void multiplyScalarTest() {
		final Matrix<Integer> result = matrix1.multiply(5);

		Assertions.assertEquals(Integer.valueOf(5), result.get(0, 0));
		Assertions.assertEquals(Integer.valueOf(10), result.get(0, 1));
		Assertions.assertEquals(Integer.valueOf(15), result.get(0, 2));
		Assertions.assertEquals(Integer.valueOf(20), result.get(1, 0));
		Assertions.assertEquals(Integer.valueOf(25), result.get(1, 1));
		Assertions.assertEquals(Integer.valueOf(30), result.get(1, 2));
		Assertions.assertEquals(Integer.valueOf(35), result.get(2, 0));
		Assertions.assertEquals(Integer.valueOf(40), result.get(2, 1));
		Assertions.assertEquals(Integer.valueOf(45), result.get(2, 2));
	}

	@Test
	void multiplyTest() {
		final Matrix<Integer> result = matrix1.multiply(squareMatrix);

		Assertions.assertEquals(Integer.valueOf(330), result.get(0, 0));
		Assertions.assertEquals(Integer.valueOf(264), result.get(0, 1));
		Assertions.assertEquals(Integer.valueOf(198), result.get(0, 2));
		Assertions.assertEquals(Integer.valueOf(924), result.get(1, 0));
		Assertions.assertEquals(Integer.valueOf(759), result.get(1, 1));
		Assertions.assertEquals(Integer.valueOf(594), result.get(1, 2));
		Assertions.assertEquals(Integer.valueOf(1518), result.get(2, 0));
		Assertions.assertEquals(Integer.valueOf(1254), result.get(2, 1));
		Assertions.assertEquals(Integer.valueOf(990), result.get(2, 2));
		Assertions.assertEquals(Integer.valueOf(2112), result.get(3, 0));
		Assertions.assertEquals(Integer.valueOf(1749), result.get(3, 1));
		Assertions.assertEquals(Integer.valueOf(1386), result.get(3, 2));
	}

	@Test
	void transposeTest() {
		final Matrix<Integer> result = matrix1.transpose();

		Assertions.assertEquals(Integer.valueOf(1), result.get(0, 0));
		Assertions.assertEquals(Integer.valueOf(4), result.get(0, 1));
		Assertions.assertEquals(Integer.valueOf(7), result.get(0, 2));
		Assertions.assertEquals(Integer.valueOf(10), result.get(0, 3));
		Assertions.assertEquals(Integer.valueOf(2), result.get(1, 0));
		Assertions.assertEquals(Integer.valueOf(5), result.get(1, 1));
		Assertions.assertEquals(Integer.valueOf(8), result.get(1, 2));
		Assertions.assertEquals(Integer.valueOf(11), result.get(1, 3));
		Assertions.assertEquals(Integer.valueOf(3), result.get(2, 0));
		Assertions.assertEquals(Integer.valueOf(6), result.get(2, 1));
		Assertions.assertEquals(Integer.valueOf(9), result.get(2, 2));
		Assertions.assertEquals(Integer.valueOf(12), result.get(2, 3));
	}

	@Test
	void determinantTest() {
		Assertions.assertEquals(Integer.valueOf(1), IntegerMatrix.identityMatrix(2).determinant());
		Assertions.assertEquals(Integer.valueOf(1), IntegerMatrix.identityMatrix(3).determinant());
		Assertions.assertEquals(Integer.valueOf(1), IntegerMatrix.identityMatrix(4).determinant());
		Assertions.assertEquals(Integer.valueOf(1), IntegerMatrix.identityMatrix(5).determinant());

		Assertions.assertEquals(Integer.valueOf(0), squareMatrix.determinant());

		Assertions.assertEquals(Integer.valueOf(18), squareMatrix2.determinant());

		Assertions.assertEquals(Integer.valueOf(23), squareMatrix3.determinant());
	}

	@Test
	void isSquareMatrixTest() {
		Assertions.assertTrue(squareMatrix.isSquareMatrix());
		Assertions.assertTrue(squareMatrix2.isSquareMatrix());

		Assertions.assertFalse(matrix1.isSquareMatrix());
		Assertions.assertFalse(matrix2.isSquareMatrix());
	}

	@Test
	void hasSameSizeOfTest() {
		Assertions.assertTrue(matrix1.hasSameSizeOf(matrix1));
		Assertions.assertTrue(matrix1.hasSameSizeOf(matrix2));
		Assertions.assertTrue(squareMatrix.hasSameSizeOf(squareMatrix2));

		Assertions.assertFalse(squareMatrix.hasSameSizeOf(matrix1));
		Assertions.assertFalse(squareMatrix.hasSameSizeOf(matrix2));
		Assertions.assertFalse(squareMatrix.hasSameSizeOf(squareMatrix3));
	}

	@Test
	void canMultiplyWithTest() {
		Assertions.assertTrue(matrix1.canMultiplyWith(squareMatrix));
		Assertions.assertTrue(matrix1.canMultiplyWith(squareMatrix2));
		Assertions.assertTrue(squareMatrix.canMultiplyWith(squareMatrix));
		Assertions.assertTrue(squareMatrix.canMultiplyWith(squareMatrix2));
		Assertions.assertTrue(squareMatrix2.canMultiplyWith(squareMatrix2));

		Assertions.assertFalse(matrix1.canMultiplyWith(matrix1));
		Assertions.assertFalse(matrix1.canMultiplyWith(matrix2));
		Assertions.assertFalse(squareMatrix.canMultiplyWith(matrix1));
		Assertions.assertFalse(squareMatrix.canMultiplyWith(matrix2));
		Assertions.assertFalse(squareMatrix.canMultiplyWith(squareMatrix3));
	}

	@Test
	void rowVectorTest() {
		final Matrix<Integer> rowVector = matrix1.rowVector(1);
		Assertions.assertEquals(Integer.valueOf(1), rowVector.getRowNumber());
		Assertions.assertEquals(matrix1.getColumnNumber(), rowVector.getColumnNumber());

		Assertions.assertEquals(Integer.valueOf(4), rowVector.get(0, 0));
		Assertions.assertEquals(Integer.valueOf(5), rowVector.get(0, 1));
		Assertions.assertEquals(Integer.valueOf(6), rowVector.get(0, 2));
	}

	@Test
	void columnVectorTest() {
		final Matrix<Integer> columnVector = matrix1.columnVector(2);
		Assertions.assertEquals(matrix1.getRowNumber(), columnVector.getRowNumber());
		Assertions.assertEquals(Integer.valueOf(1), columnVector.getColumnNumber());

		Assertions.assertEquals(Integer.valueOf(3), columnVector.get(0, 0));
		Assertions.assertEquals(Integer.valueOf(6), columnVector.get(1, 0));
		Assertions.assertEquals(Integer.valueOf(9), columnVector.get(2, 0));
		Assertions.assertEquals(Integer.valueOf(12), columnVector.get(3, 0));
	}

	@Test
	void subMatrixTest() {
		final Matrix<Integer> submatrix = matrix1.subMatrix(1, 2);

		Assertions.assertEquals(Integer.valueOf(3), submatrix.getRowNumber());
		Assertions.assertEquals(Integer.valueOf(2), submatrix.getColumnNumber());

		Assertions.assertEquals(Integer.valueOf(1), submatrix.get(0, 0));
		Assertions.assertEquals(Integer.valueOf(2), submatrix.get(0, 1));
		Assertions.assertEquals(Integer.valueOf(7), submatrix.get(1, 0));
		Assertions.assertEquals(Integer.valueOf(8), submatrix.get(1, 1));
		Assertions.assertEquals(Integer.valueOf(10), submatrix.get(2, 0));
		Assertions.assertEquals(Integer.valueOf(11), submatrix.get(2, 1));
	}

	@Test
	void toStringTest() {
		Assertions.assertEquals("|  1  2  3 |\n" +
								"|  4  5  6 |\n" +
								"|  7  8  9 |\n" +
								"| 10 11 12 |\n", matrix1.toString());
		Assertions.assertEquals("| 99 88 77 |\n" +
								"| 66 55 44 |\n" +
								"| 33 22 11 |\n", squareMatrix.toString());
	}

	@Test
	void constructTestErrorRow() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			new IntegerMatrix(-1, 5);
		});
	}

	@Test
	void constructTestErrorColumn() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			new IntegerMatrix(2, 0);
		});
	}

	@Test
	void getTestErrorNegativeRow() {
		Assertions.assertThrows(OutOfMatrixBoundsException.class, () -> {
			matrix1.get(-1, 1);
		});
	}

	@Test
	void getTestErrorNegativeColumn() {
		Assertions.assertThrows(OutOfMatrixBoundsException.class, () -> {
			matrix1.get(2, -2);
		});
	}

	@Test
	void getTestErrorBigRow() {
		Assertions.assertThrows(OutOfMatrixBoundsException.class, () -> {
			matrix1.get(4, 2);
		});
	}

	@Test
	void getTestErrorBigColumn() {
		Assertions.assertThrows(OutOfMatrixBoundsException.class, () -> {
			matrix1.get(2, 3);
		});
	}

	@Test
	void determinantTestNonSquareError() {
		Assertions.assertThrows(NotSquareMatrixException.class, () -> {
			matrix1.determinant();
		});
	}

	@Test
	void multiplyTestNotAvailable() {
		Assertions.assertThrows(MultiplicationNotAvailableException.class, () -> {
			matrix1.multiply(matrix2);
		});
	}
}
