package fr.khelan.commons.math.number;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class RationalTest {
	private final Rational zero;
	private final Rational one;
	private final Rational oneHalf;
	private final Rational oneThird;
	private final Rational minusThreeQuarters;

	RationalTest() {
		zero = Rational.from(BigInteger.ZERO, BigInteger.ONE);
		one = Rational.from(BigInteger.ONE, BigInteger.ONE);
		oneHalf = Rational.from("1", "2");
		oneThird = Rational.from(1, 3);
		minusThreeQuarters = Rational.from(3, -4);
	}

	private static final Rational rationalValue(final int integer) {
		return rationalValue(integer, 1);
	}

	private static Rational rationalValue(final int numerator, final int denominator) {
		return Rational.from(numerator, denominator);
	}

	private static final Rational rationalValue(final String decimal) {
		BigDecimal bigDecimal = new BigDecimal(decimal);
		return Rational.irreductibleFraction(bigDecimal.unscaledValue(),
											 BigInteger.TEN.pow(bigDecimal.scale()));
	}

	@Test
	void numeratorDenominatorRealTest() {
		Assertions.assertEquals(0, zero.numerator().intValue());
		Assertions.assertEquals(1, zero.denominator().intValue());
		Assertions.assertEquals(BigDecimal.ZERO, zero.realValue());

		Assertions.assertEquals(1, one.numerator().intValue());
		Assertions.assertEquals(1, one.denominator().intValue());
		Assertions.assertEquals(BigDecimal.ONE, one.realValue());

		Assertions.assertEquals(1, oneHalf.numerator().intValue());
		Assertions.assertEquals(2, oneHalf.denominator().intValue());
		Assertions.assertEquals(BigDecimal.valueOf(0.5), oneHalf.realValue());

		Assertions.assertEquals(1, oneThird.numerator().intValue());
		Assertions.assertEquals(3, oneThird.denominator().intValue());

		Assertions.assertEquals(-3, minusThreeQuarters.numerator().intValue());
		Assertions.assertEquals(4, minusThreeQuarters.denominator().intValue());
		Assertions.assertEquals(BigDecimal.valueOf(-0.75), minusThreeQuarters.realValue());
	}

	@Test
	void fromTest() {
		Assertions.assertThrows(ArithmeticException.class, () -> Rational.from(0, 0));
		Assertions.assertThrows(ArithmeticException.class, () -> Rational.from("0", "0"));
		Assertions.assertThrows(ArithmeticException.class, () -> Rational.from(BigInteger.ZERO, BigInteger.ZERO));
		Assertions.assertThrows(ArithmeticException.class, () -> Rational.from(Numbers.ZERO, Numbers.ZERO));
	}

	@Test
	void hasSameValueOfTest() {
		Assertions.assertTrue(zero.hasSameValueOf(Numbers.ZERO));

		Assertions.assertTrue(one.hasSameValueOf(Numbers.ONE));
		Assertions.assertTrue(oneHalf.hasSameValueOf(rationalValue("0.5")));
		Assertions.assertTrue(oneThird.hasSameValueOf(rationalValue(1, 3)));
		Assertions.assertTrue(minusThreeQuarters.hasSameValueOf(rationalValue("-0.75")));

		Assertions.assertFalse(one.hasSameValueOf(zero));
		Assertions.assertFalse(one.hasSameValueOf(oneHalf));
		Assertions.assertFalse(one.hasSameValueOf(oneThird));
		Assertions.assertFalse(one.hasSameValueOf(minusThreeQuarters));
		Assertions.assertFalse(oneHalf.hasSameValueOf(minusThreeQuarters));
	}

	@Test
	void absSignumTest() {
		Assertions.assertTrue(zero.abs().hasSameValueOf(zero));
		Assertions.assertEquals(0, zero.signum());

		Assertions.assertTrue(one.abs().hasSameValueOf(one));
		Assertions.assertEquals(1, one.signum());

		Assertions.assertTrue(oneHalf.abs().hasSameValueOf(oneHalf));
		Assertions.assertEquals(1, oneHalf.signum());

		Assertions.assertTrue(oneThird.abs().hasSameValueOf(oneThird));
		Assertions.assertEquals(1, oneThird.signum());

		Assertions.assertTrue(minusThreeQuarters.abs().hasSameValueOf(rationalValue(3, 4)));
		Assertions.assertEquals(-1, minusThreeQuarters.signum());
	}

	@Test
	void addTest() {
		Assertions.assertTrue(zero.add(one).hasSameValueOf(one));
		Assertions.assertTrue(zero.add(oneHalf).hasSameValueOf(oneHalf));
		Assertions.assertTrue(zero.add(oneThird).hasSameValueOf(oneThird));
		Assertions.assertTrue(zero.add(minusThreeQuarters).hasSameValueOf(minusThreeQuarters));

		Assertions.assertTrue(one.add(one).hasSameValueOf(Numbers.TWO));
		Assertions.assertTrue(oneHalf.add(oneHalf).hasSameValueOf(Numbers.ONE));
		Assertions.assertTrue(oneThird.add(oneThird).add(oneThird).hasSameValueOf(Numbers.ONE));
		Assertions.assertTrue(minusThreeQuarters.add(oneHalf).hasSameValueOf(rationalValue(-1, 4)));
	}

	@Test
	void subtractNegateTest() {
		Assertions.assertTrue(zero.subtract(one).hasSameValueOf(Numbers.ONE.negate()));
		Assertions.assertTrue(zero.subtract(oneHalf).hasSameValueOf(oneHalf.negate()));
		Assertions.assertTrue(zero.subtract(oneThird).hasSameValueOf(oneThird.negate()));
		Assertions.assertTrue(zero.subtract(minusThreeQuarters).hasSameValueOf(minusThreeQuarters.negate()));

		Assertions.assertTrue(one.subtract(one).hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(oneHalf.subtract(oneHalf).hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(oneThird.subtract(oneThird).hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(minusThreeQuarters.subtract(oneHalf).hasSameValueOf(rationalValue(-5, 4)));
	}

	@Test
	void multiplyTest() {
		Assertions.assertTrue(zero.multiply(one).hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(zero.multiply(oneHalf).hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(zero.multiply(oneThird).hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(zero.multiply(minusThreeQuarters).hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(one.multiply(zero).hasSameValueOf(Numbers.ZERO));

		Assertions.assertTrue(one.multiply(one).hasSameValueOf(one));
		Assertions.assertTrue(one.multiply(oneHalf).hasSameValueOf(oneHalf));
		Assertions.assertTrue(one.multiply(oneThird).hasSameValueOf(oneThird));
		Assertions.assertTrue(one.multiply(minusThreeQuarters).hasSameValueOf(minusThreeQuarters));
		Assertions.assertTrue(minusThreeQuarters.multiply(one).hasSameValueOf(minusThreeQuarters));

		Assertions.assertTrue(minusThreeQuarters.multiply(oneThird).hasSameValueOf(rationalValue(-1, 4)));
		Assertions.assertTrue(minusThreeQuarters.multiply(minusThreeQuarters).hasSameValueOf(rationalValue(9, 16)));
	}

	@Test
	void divideInverseTest() {
		Assertions.assertTrue(one.divide(one).hasSameValueOf(one));
		Assertions.assertTrue(oneHalf.divide(one).hasSameValueOf(oneHalf));
		Assertions.assertTrue(oneThird.divide(one).hasSameValueOf(oneThird));
		Assertions.assertTrue(minusThreeQuarters.divide(one).hasSameValueOf(minusThreeQuarters));

		Assertions.assertTrue(one.divide(one).hasSameValueOf(one));
		Assertions.assertTrue(one.divide(oneHalf).hasSameValueOf(oneHalf.inverse()));
		Assertions.assertTrue(one.divide(oneThird).hasSameValueOf(oneThird.inverse()));
		Assertions.assertTrue(one.divide(minusThreeQuarters).hasSameValueOf(minusThreeQuarters.inverse()));

		Assertions.assertThrows(ArithmeticException.class, () -> zero.inverse());
		Assertions.assertThrows(ArithmeticException.class, () -> one.divide(zero));
		Assertions.assertThrows(ArithmeticException.class, () -> zero.divide(zero));
		Assertions.assertThrows(ArithmeticException.class, () -> zero.divide(zero));
		Assertions.assertThrows(ArithmeticException.class, () -> zero.divide(zero));
	}

	@Test
	void powTest() {
		Assertions.assertTrue(zero.pow(Numbers.TEN).hasSameValueOf(Numbers.ZERO));
		Assertions.assertTrue(one.pow(Numbers.TEN).hasSameValueOf(Numbers.ONE));
		Assertions.assertTrue(oneHalf.pow(Numbers.TWO).hasSameValueOf(rationalValue(1, 4)));
		Assertions.assertTrue(oneThird.pow(Numbers.THREE).hasSameValueOf(rationalValue(1, 27)));
		Assertions.assertTrue(minusThreeQuarters.pow(Numbers.TWO).hasSameValueOf(rationalValue(9, 16)));
		Assertions.assertTrue(minusThreeQuarters.pow(Numbers.THREE).hasSameValueOf(rationalValue(-27, 64)));
	}

	@Test
	void toStringTest() {
		Assertions.assertEquals("0 / 1", zero.toRationalString());
		Assertions.assertEquals("1 / 1", one.toRationalString());
		Assertions.assertEquals("1 / 3", oneThird.toRationalString());
		Assertions.assertEquals("-3 / 4", minusThreeQuarters.toRationalString());

		Assertions.assertEquals("0", zero.toString());
		Assertions.assertEquals("1 / 1", one.toString());
		Assertions.assertEquals("1 / 3", oneThird.toString());
		Assertions.assertEquals("-3 / 4", minusThreeQuarters.toString());
	}

	@Test
	void equalsAndHashcodeTest() {
		Assertions.assertEquals(zero, zero);
		Assertions.assertEquals(zero.hashCode(), rationalValue(0, 1).hashCode());
		Assertions.assertEquals(one.hashCode(), rationalValue(1, 1).hashCode());
		Assertions.assertEquals(oneHalf, oneHalf);
		Assertions.assertEquals(oneHalf.hashCode(), rationalValue(1, 2).hashCode());
		Assertions.assertEquals(oneThird, oneThird);
		Assertions.assertEquals(oneThird.hashCode(), rationalValue(1, 3).hashCode());
		Assertions.assertEquals(minusThreeQuarters, minusThreeQuarters);
		Assertions.assertEquals(minusThreeQuarters,
								Rational.from(minusThreeQuarters.numerator(), minusThreeQuarters.denominator()));
		Assertions.assertEquals(minusThreeQuarters.hashCode(), rationalValue(-3, 4).hashCode());

		Assertions.assertFalse(zero.equals(null));
		Assertions.assertFalse(one.equals(null));
		Assertions.assertFalse(minusThreeQuarters.equals(null));
		Assertions.assertFalse(one.equals(Numbers.ONE));
	}
}
