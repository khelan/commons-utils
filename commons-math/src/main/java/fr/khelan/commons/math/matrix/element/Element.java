package fr.khelan.commons.math.matrix.element;

import lombok.Getter;

@Getter
public class Element<T> {
	private Position position;
	private T value;

	public Element(final Position position, final T value) {
		this.position = position;
		this.value = value;
	}
}
