package fr.khelan.commons.math.number.impl;

import fr.khelan.commons.math.number.Integer;
import fr.khelan.commons.math.number.Number;

/**
 * Interface representing a number.
 *
 * @param <T>
 * 		The type of the number.
 * @param <I>
 * 		The type of the inverse or result of a division.
 */
public interface NumberImpl<T extends Number<?, ?>, I extends Number<?, ?>> {
	/**
	 * Returns a Number whose value is the absolute value of this Number.
	 *
	 * @return {@code abs(this)}
	 */
	T abs();

	/**
	 * Returns the sign of this Number.
	 *
	 * @return -1, 0 or 1 as the value of this Number is negative, zero or positive.
	 */
	int signum();

	/**
	 * Returns a {@link Number} whose value is {@code (this + otherNumber)}.
	 *
	 * @param otherNumber
	 * 		the other number to be added to this {@code Number}.
	 *
	 * @return {@code this + otherNumber}
	 */
	T add(T otherNumber);

	/**
	 * Returns a {@link Number} whose value is {@code (this - otherNumber)}.
	 *
	 * @param otherNumber
	 * 		the other number to be subtracted to this {@code Number}.
	 *
	 * @return {@code this - otherNumber}
	 */
	T subtract(T otherNumber);

	/**
	 * Returns a {link Number} whose value is {@code (-this)}.
	 *
	 * @return {@code -this}
	 */
	T negate();

	/**
	 * Returns a {@link Number} whose value is {@code (this * factor)}.
	 *
	 * @param factor
	 * 		the other number to be multiplied by this {@code Number}.
	 *
	 * @return {@code this * factor}
	 */
	T multiply(T factor);

	/**
	 * Returns a {@link Number} whose value is {@code (this / divisor)}.
	 *
	 * @param divisor
	 * 		the other number by which {@code Number} is to be divided.
	 *
	 * @return {@code this / divisor}
	 *
	 * @throws ArithmeticException
	 * 		if the divisor is {@code null} or {@code 0}.
	 */
	I divide(T divisor) throws ArithmeticException;

	/**
	 * Returns the multiplicative inverse whose value is {@code (1 / this)}.
	 *
	 * @return 1 &divide; this
	 *
	 * @throws ArithmeticException
	 * 		if this number is equals to {@code 0}.
	 */
	I inverse() throws ArithmeticException;

	T pow(Integer integer);

	Number<?, ?> sqrt();

	/**
	 * Checks if this {@code Number} has the same value of the other number, i.e. {@code (this = otherNumber)}.
	 *
	 * @param otherNumber
	 * 		the other number to be compared with this {@code Number}.
	 *
	 * @return true if the numbers have the same value, false otherwise.
	 */
	boolean hasSameValueOf(T otherNumber);
}
