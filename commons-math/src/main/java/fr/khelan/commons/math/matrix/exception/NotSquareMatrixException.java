package fr.khelan.commons.math.matrix.exception;

public class NotSquareMatrixException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public NotSquareMatrixException() {
		super("Not a square matrix.");
	}

}
