package fr.khelan.commons.math.number;

import java.math.BigDecimal;
import java.util.Objects;

import fr.khelan.commons.math.number.impl.DecimalImpl;

/**
 * A Real number from &#8477;.
 */
public abstract class Real extends Complex implements Comparable<Real> {
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new Real.
	 *
	 * @param value The value of this Real.
	 *
	 * @return The Real number with the specified value.
	 */
	protected static Real from(final BigDecimal value) {
		return new DecimalImpl(value);
	}

	/**
	 * Creates a new Real.
	 *
	 * @param value The {@link String} representation of the value of this Real.
	 *
	 * @return The Real number with the specified value.
	 */
	protected static Real from(final double value) {
		return Real.from(BigDecimal.valueOf(value));
	}

	/**
	 * Returns the value backed by this {@link Real}.
	 *
	 * @return The {@link BigDecimal} backed by this Real.
	 */
	protected abstract BigDecimal realValue();

	/**
	 * Returns a {@link Real} whose value is {@code (this + addend)}.
	 *
	 * @param addend the other number to be added to this.
	 *
	 * @return {@code this + addend}
	 */
	public Real add(final Real addend) {
		return from(realValue().add(addend.realValue()));
	}

	/**
	 * Returns a {@link Real} whose value is {@code (this - subtrahend)}.
	 *
	 * @param subtrahend the other number to be subtracted to this.
	 *
	 * @return {@code this - subtrahend}
	 */
	public Real subtract(final Real subtrahend) {
		return from(realValue().subtract(subtrahend.realValue()));
	}

	/**
	 * Returns a {@link Real} which is the negative number of this number.
	 *
	 * @return {@code -this}
	 */
	@Override
	public Real negate() {
		return from(realValue().negate());
	}

	/**
	 * Returns a {@link Real} whose value is {@code (this} &times; {@code factor)}.
	 *
	 * @param factor the other number to be multiplied by this.
	 *
	 * @return this &times; factor
	 */
	public Real multiply(final Real factor) {
		return from(realValue().multiply(factor.realValue()));
	}

	/**
	 * Returns a {@link Real} whose value is {@code (this / divisor)}.
	 *
	 * @param divisor the other number by which this is to be divided.
	 *
	 * @return this &divide; divisor
	 *
	 * @throws ArithmeticException if the divisor is {@code null} or {@code 0}.
	 */
	@Override
	public Real divide(final Real divisor) throws ArithmeticException {
		if (divisor == null || divisor.hasSameValueOf(Numbers.ZERO)) {
			throw new ArithmeticException("Divisor must not be null or equals to 0.");
		}
		return from(realValue().divide(divisor.realValue(), MATH_CONTEXT));
	}

	/**
	 * Returns a {@link Real} which is the multiplicative inverse whose value is
	 * {@code (1 / this)}.
	 *
	 * @return 1 &divide; this
	 *
	 * @throws ArithmeticException if this number is equals to {@code 0}.
	 */
	@Override
	public Real inverse() {
		return Numbers.ONE.divide(this);
	}

	@Override
	public Real pow(final Integer n) {
		return from(realValue().pow(n.intValue()));
	}

	/**
	 * Returns a {@link Real} whose value is the Square Root of this number.
	 *
	 * @return &radic;(this)
	 */
	@Override
	public Real sqrt() {
		return from(realValue().sqrt(MATH_CONTEXT));
	}

	/**
	 * Compare the number z with this, only by value, without considering the scale
	 * of both numbers.
	 *
	 * @param val the other number to compare with.
	 * @return {@code true} if this has the same value of z, {@code false}
	 *         otherwise.
	 */
	public boolean hasSameValueOf(final Real val) {
		return realValue().compareTo(val.realValue()) == 0;
	}

	@Override
	public Real real() {
		return this;
	}

	@Override
	public Real imaginary() {
		return Numbers.ZERO;
	}

	@Override
	public Real absoluteSquare() {
		return pow(Numbers.TWO);
	}

	@Override
	public Real modulus() {
		return abs();
	}

	@Override
	public Real argument() {
		if (signum() >= 0) {
			return Numbers.ZERO;
		} else {
			return Numbers.PI;
		}
	}

	@Override
	public Real argumentPi() {
		if (signum() >= 0) {
			return Numbers.ZERO;
		} else {
			return Numbers.ONE;
		}
	}

	@Override
	public int intValue() {
		return realValue().intValue();
	}

	@Override
	public long longValue() {
		return realValue().longValue();
	}

	@Override
	public float floatValue() {
		return realValue().floatValue();
	}

	@Override
	public double doubleValue() {
		return realValue().doubleValue();
	}

	/**
	 * Compare the number z with this, only by value, without considering the scale
	 * of both numbers.
	 *
	 * @param val the other number to compare with.
	 * @return -1, 0, or 1 as this number is numerically less than, equal to, or
	 *         greater than val.
	 */
	@Override
	public int compareTo(final Real val) {
		return realValue().compareTo(val.realValue());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (realValue() == null ? 0 : realValue().hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		return Objects.equals(realValue(), ((Real) obj).realValue());
	}

	@Override
	public String toString() {
		return realValue().toString();
	}
}
