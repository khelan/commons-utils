package fr.khelan.commons.math.number;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Objects;

import fr.khelan.commons.math.number.impl.IntegerImpl;

/**
 * An Integer number from &#8484;
 */
public abstract class Integer extends Decimal {
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new Integer.
	 *
	 * @param value The value of this new Integer.
	 *
	 * @return The new Integer.
	 */
	public static Integer from(final String value) {
		return new IntegerImpl(new BigInteger(value));
	}

	/**
	 * Creates a new Integer.
	 *
	 * @param value The value of this new Integer.
	 *
	 * @return The new Integer.
	 */
	public static Integer from(final BigInteger value) {
		return new IntegerImpl(value);
	}

	/**
	 * Creates a new Integer.
	 *
	 * @param value The value of this new Integer.
	 *
	 * @return The new Integer.
	 */
	public static Integer from(final long value) {
		return new IntegerImpl(BigInteger.valueOf(value));
	}

	/**
	 * Returns the value backed by this {@link Integer}.
	 *
	 * @return The {@link BigInteger} backed by this Integer.
	 */
	protected abstract BigInteger integerValue();

	@Override
	protected BigDecimal decimalValue() {
		return new BigDecimal(integerValue());
	}

	@Override
	public Integer numerator() {
		return this;
	}

	@Override
	public Integer denominator() {
		return Numbers.ONE;
	}

	@Override
	public Integer abs() {
		return from(integerValue().abs());
	}

	@Override
	public int signum() {
		return integerValue().signum();
	}

	/**
	 * Returns an {@link Integer} whose value is {@code (this + addend)}.
	 *
	 * @param addend the other number to be added to this.
	 *
	 * @return {@code this + addend}
	 */
	public Integer add(final Integer addend) {
		return from(integerValue().add(addend.integerValue()));
	}

	/**
	 * Returns an {@link Integer} whose value is {@code (this - subtrahend)}.
	 *
	 * @param subtrahend the other number to be subtracted to this.
	 *
	 * @return {@code this - subtrahend}
	 */
	public Integer subtract(final Integer subtrahend) {
		return from(integerValue().subtract(subtrahend.integerValue()));
	}

	/**
	 * Returns an {@link Integer} which is the negative number of this number.
	 *
	 * @return {@code -this}
	 */
	@Override
	public Integer negate() {
		return from(integerValue().negate());
	}

	/**
	 * Returns an {@link Integer} whose value is {@code (this} &times;
	 * {@code factor)}.
	 *
	 * @param factor the other number to be multiplied by this.
	 *
	 * @return this &times; factor
	 */
	public Integer multiply(final Integer factor) {
		return from(integerValue().multiply(factor.integerValue()));
	}

	/**
	 * Returns a {@link Rational} whose value is {@code (this / divisor)}.
	 *
	 * @param divisor the other number by which this is to be divided.
	 *
	 * @return this &divide; divisor
	 *
	 * @throws ArithmeticException if the divisor is {@code null} or {@code 0}.
	 */
	public Rational divide(final Integer divisor) throws ArithmeticException {
		return super.divide(divisor);
	}

	/**
	 * Returns an {@link Integer} whose value is {@code (this / divisor)}.
	 *
	 * @param divisor the other number by which this is to be divided.
	 * @return this / divisor
	 * @throws ArithmeticException if the divisor is {@code null} or {@code 0}.
	 */
	public Integer divideToIntegralValue(final Integer divisor) throws ArithmeticException {
		return Integer.from(integerValue().divide(divisor.integerValue()));
	}

	/**
	 * Returns an {@link Integer} whose value is {@code (this % divisor)}.
	 *
	 * @param divisor the other number by which this is to be divided.
	 * @return this % divisor
	 * @throws ArithmeticException if the divisor is {@code null} or {@code 0}.
	 */
	public Integer remainder(final Integer divisor) throws ArithmeticException {
		Integer remainder = Integer.from(integerValue().remainder(divisor.integerValue()));
		if (remainder.signum() < 0) {
			remainder = divisor.abs().add(remainder);
		}
		return remainder;
	}

	/**
	 * Returns a {@link Rational} which is the multiplicative inverse whose value is
	 * {@code (1 / this)}.
	 *
	 * @return 1 &divide; this
	 *
	 * @throws ArithmeticException if this number is equals to {@code 0}.
	 */
	@Override
	public Rational inverse() {
		return super.inverse();
	}

	@Override
	public Integer pow(final Integer otherNumber) {
		return Integer.from(integerValue().pow(otherNumber.intValue()));
	}

	/**
	 * Returns a Number whose value is the greatest common divisor of
	 * {@code abs(this)} and {@code abs(val)}. Returns 0 if
	 * {@code this == 0 && val == 0}.
	 *
	 * @param val the other integer with which the GCD is to be computed.
	 * @return {@code GCD(abs(this), abs(val))}
	 */
	public Integer gcd(final Integer val) {
		return abs().doGcd(val.abs());
	}

	private Integer doGcd(final Integer val) {
		Integer min, max;
		if (compareTo(val) >= 0) {
			min = val;
			max = this;
		} else {
			min = this;
			max = val;
		}
		if (min.hasSameValueOf(Numbers.ZERO)) {
			return max;
		}

		return min.doGcd(max.subtract(min));
	}

	/**
	 * Returns a Number whose value is the least common multiple of
	 * {@code abs(this)} and {@code abs(val)}. Returns 0 if {@code abs(this) == 0}
	 * or {@code abs(val) == 0}
	 *
	 * @param val the other integer with which the LCM is to be computed.
	 * @return {@code LCM(abs(this), abs(val))}
	 */
	public Integer lcm(final Integer val) {
		if (Numbers.ZERO.hasSameValueOf(this) || Numbers.ZERO.hasSameValueOf(val)) {
			return Numbers.ZERO;
		}
		return abs().multiply(val.abs()).divideToIntegralValue(gcd(val));
	}

	/**
	 * Compare the number {@code n} with {@code this}, only by value, without
	 * considering the scale of both numbers.
	 *
	 * @param n the other number to compare with.
	 * @return {@code true} if {@code this} has the same value of {@code n},
	 *         {@code false} otherwise.
	 */
	public boolean hasSameValueOf(final Integer n) {
		return integerValue().compareTo(n.integerValue()) == 0;
	}

	@Override
	public int intValue() {
		return integerValue().intValue();
	}

	@Override
	public long longValue() {
		return integerValue().longValue();
	}

	@Override
	public float floatValue() {
		return integerValue().floatValue();
	}

	@Override
	public double doubleValue() {
		return integerValue().doubleValue();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (integerValue() == null ? 0 : integerValue().hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		return Objects.equals(integerValue(), ((Integer) obj).integerValue());
	}

	@Override
	public String toString() {
		return integerValue().toString();
	}
}
