package fr.khelan.commons.math.number.impl;

import fr.khelan.commons.math.number.Integer;
import fr.khelan.commons.math.number.Numbers;
import fr.khelan.commons.math.number.Rational;

public class RationalImpl extends Rational implements NumberImpl<Rational, Rational> {
	private static final long serialVersionUID = 1L;

	private final Integer numerator;
	private final Integer denominator;

	public RationalImpl(final Integer numerator, final Integer denominator) {
		if (denominator == null || denominator.hasSameValueOf(Numbers.ZERO)) {
			throw new ArithmeticException("Denominator must not be null or equals to 0.");
		}
		this.numerator = numerator;
		this.denominator = denominator;
	}

	@Override
	public Integer numerator() {
		return numerator;
	}

	@Override
	public Integer denominator() {
		return denominator;
	}

}
