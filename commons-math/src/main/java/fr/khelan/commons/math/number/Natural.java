package fr.khelan.commons.math.number;

import java.math.BigInteger;

import fr.khelan.commons.math.number.impl.NaturalImpl;

/**
 * A Natural number from &#8469;.
 */
public abstract class Natural extends Integer {
	private static final long serialVersionUID = 1L;

	public static final Natural ZERO = from(0L);

	/**
	 * Creates a new Natural.
	 *
	 * @param value The value of this new Natural.
	 *
	 * @return The new Natural.
	 */
	public static Natural from(final String value) {
		return from(new BigInteger(value));
	}

	/**
	 * Creates a new Natural.
	 *
	 * @param value The value of this new Natural.
	 *
	 * @return The new Natural.
	 */
	public static Natural from(final BigInteger value) {
		return new NaturalImpl(value);
	}

	/**
	 * Creates a new Natural.
	 *
	 * @param value The value of this new Natural.
	 *
	 * @return The new Natural.
	 */
	public static Natural from(final long value) {
		return from(BigInteger.valueOf(value));
	}

	@Override
	public Natural abs() {
		return this;
	}

	/**
	 * Returns a {@link Natural} whose value is {@code (this + addend)}.
	 *
	 * @param addend the other number to be added to this.
	 *
	 * @return {@code this + addend}
	 */
	public Natural add(final Natural addend) {
		return from(integerValue().add(addend.integerValue()));
	}

	/**
	 * Returns a {@link Natural} whose value is {@code (this - subtrahend)}.
	 *
	 * @param subtrahend the other number to be subtracted to this.
	 *
	 * @return {@code this - subtrahend}
	 */
	public Natural subtract(final Natural subtrahend) {
		if (compareTo(subtrahend) < 0) {
			throw new ArithmeticException("This must be bigger or equals to subtrahend.");
		}
		return from(integerValue().subtract(subtrahend.integerValue()));
	}

	/**
	 * Returns a {@link Natural} which is the negative number of this number.
	 *
	 * @return {@code -this}
	 */
	@Override
	public Natural negate() {
		if (hasSameValueOf(Numbers.ZERO)) {
			return this;
		}
		throw new ArithmeticException("A natural cannot be negate.");
	}

	/**
	 * Returns a {@link Natural} whose value is {@code (this} &times;
	 * {@code factor)}.
	 *
	 * @param factor the other number to be multiplied by this.
	 *
	 * @return this &times; factor
	 */
	public Natural multiply(final Natural factor) {
		return from(integerValue().multiply(factor.integerValue()));
	}

	/**
	 * Returns a {@link Rational} whose value is {@code (this / divisor)}.
	 *
	 * @param divisor the other number by which this is to be divided.
	 *
	 * @return this &divide; divisor
	 *
	 * @throws ArithmeticException if the divisor is {@code null} or {@code 0}.
	 */
	public Rational divide(final Natural divisor) throws ArithmeticException {
		return super.divide(divisor);
	}

	/**
	 * Returns a {@link Natural} whose value is {@code (this / divisor)}.
	 *
	 * @param divisor the other number by which this is to be divided.
	 * @return this / divisor
	 * @throws ArithmeticException if the divisor is {@code null} or {@code 0}.
	 */
	public Natural divideToIntegralValue(final Natural divisor) throws ArithmeticException {
		return Natural.from(integerValue().divide(divisor.integerValue()));
	}

	/**
	 * Returns a {@link Natural} whose value is {@code (this % divisor)}.
	 *
	 * @param divisor the other number by which this is to be divided.
	 * @return this % divisor
	 * @throws ArithmeticException if the divisor is {@code null} or {@code 0}.
	 */
	public Natural remainder(final Natural divisor) throws ArithmeticException {
		return from(integerValue().remainder(divisor.integerValue()));
	}

	@Override
	public Natural pow(final Integer otherNumber) {
		return from(integerValue().pow(otherNumber.intValue()));
	}

	/**
	 * Returns a Number whose value is the greatest common divisor of
	 * {@code abs(this)} and {@code abs(val)}. Returns 0 if
	 * {@code this == 0 && val == 0}.
	 *
	 * @param val value with which the GCD is to be computed.
	 * @return {@code GCD(abs(this), abs(val))}
	 */
	public Natural gcd(final Natural val) {
		Natural min, max;
		if (compareTo(val) >= 0) {
			min = val;
			max = this;
		} else {
			min = this;
			max = val;
		}
		if (min.hasSameValueOf(ZERO)) {
			return max;
		}

		return min.gcd(max.subtract(min));
	}

	/**
	 * Returns a Number whose value is the least common multiple of
	 * {@code abs(this)} and {@code abs(val)}. Returns 0 if {@code abs(this) == 0}
	 * or {@code abs(val) == 0}
	 *
	 * @param val the other integer with which the LCM is to be computed.
	 * @return {@code LCM(abs(this), abs(val))}
	 */
	public Natural lcm(final Natural val) {
		if (ZERO.hasSameValueOf(this) || ZERO.hasSameValueOf(val)) {
			return ZERO;
		}
		return this.multiply(val).divideToIntegralValue(gcd(val));
	}

	/**
	 * Compare the number {@code n} with {@code this}, only by value, without
	 * considering the scale of both numbers.
	 *
	 * @param n the other number to compare with.
	 * @return {@code true} if {@code this} has the same value of {@code n},
	 *         {@code false} otherwise.
	 */
	public boolean hasSameValueOf(final Natural n) {
		return super.hasSameValueOf(n);
	}

	@Override
	public int intValue() {
		return integerValue().intValue();
	}

	@Override
	public long longValue() {
		return integerValue().longValue();
	}

	@Override
	public float floatValue() {
		return integerValue().floatValue();
	}

	@Override
	public double doubleValue() {
		return integerValue().doubleValue();
	}

	@Override
	public String toString() {
		return integerValue().toString();
	}
}
