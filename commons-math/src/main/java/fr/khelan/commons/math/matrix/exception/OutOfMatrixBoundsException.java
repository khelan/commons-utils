package fr.khelan.commons.math.matrix.exception;

public class OutOfMatrixBoundsException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public OutOfMatrixBoundsException() {
		super();
	}

	public OutOfMatrixBoundsException(final int index, final int maxIndex, final boolean isRow) {
		super((isRow ? "The row" : "The column") + " index " + index + " must be between 0 and " + maxIndex);
	}
}
