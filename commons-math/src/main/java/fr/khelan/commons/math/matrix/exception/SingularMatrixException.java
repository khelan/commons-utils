package fr.khelan.commons.math.matrix.exception;

public class SingularMatrixException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public SingularMatrixException() {
		super("This matrix is not invertible.");
	}
}
