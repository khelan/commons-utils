package fr.khelan.commons.math.number;

public interface Number<T extends Number<?, ?>, I extends Number<?, ?>> {
	/**
	 * Checks if this number has the same value of 0, i.e. its real and imaginary parts are nulls.
	 *
	 * @return true if this number is null, false otherwise.
	 */
	boolean isZero();

	/**
	 * Returns a Number whose value is the absolute value of this Number.
	 *
	 * @return {@code abs(this)}
	 */
	T abs();

	/**
	 * Returns the sign of this Number.
	 *
	 * @return -1, 0 or 1 as the value of this Number is negative, zero or positive.
	 */
	int signum();

	/**
	 * Returns a {@link Number} whose value is {@code (this + number)}.
	 *
	 * @param number
	 * 		the other number to be added to this {@code Number}.
	 *
	 * @return {@code this + number}
	 */
	T add(final T number);

	/**
	 * Returns a {@link Number} whose value is {@code (this - number)}.
	 *
	 * @param number
	 * 		the other number to be subtracted to this {@code Number}.
	 *
	 * @return {@code this - number}
	 */
	T subtract(final T number);

	/**
	 * Returns a {@link Number} which is the negative number of this number number.
	 *
	 * @return {@code -this}
	 */
	T negate();

	/**
	 * Returns a {@link Number} whose value is {@code (this} &times; {@code factor)}.
	 *
	 * @param factor
	 * 		the other number to be multiplied by this {@code Number}.
	 *
	 * @return this &times; factor
	 */
	T multiply(final T factor);

	/**
	 * Returns a {@link Number} whose value is {@code (this / divisor)}.
	 *
	 * @param divisor
	 * 		the other number by which {@code Number} is to be divided.
	 *
	 * @return this &divide; divisor
	 *
	 * @throws ArithmeticException
	 * 		if the divisor is {@code null} or {@code 0}.
	 */
	I divide(final T divisor) throws ArithmeticException;

	/**
	 * Returns a {@link Number} which is the multiplicative inverse whose value is {@code (1 / this)}.
	 *
	 * @return 1 &divide; this
	 *
	 * @throws ArithmeticException
	 * 		if this number is equals to {@code 0}.
	 */
	I inverse();

	/**
	 * <p>
	 * Returns a number whose value is ( this<sup>n</sup> ).
	 * </p>
	 * <p>
	 * The parameter n must be in the range 0 through 999999999, inclusive.
	 * </p>
	 *
	 * @param n
	 * 		the power to raise this number
	 *
	 * @return this<sup>n</sup>
	 */
	I pow(final Integer n);

	/**
	 * Returns a {@link Number} whose value is the Square Root of this number.
	 *
	 * @return &radic;(this)
	 */
	Number<?, ?> sqrt();

	/**
	 * Compare the number z with this, only by value, without considering the scale of both numbers.
	 *
	 * @param val
	 * 		the other number to compare with.
	 *
	 * @return {@code true} if this has the same value of z, {@code false} otherwise.
	 */
	boolean hasSameValueOf(final T val);

	/**
	 * Returns the hash code for this Number. Note that two numbers who are numerically equal but differ in scale (like
	 * 2.0 and 2.00) will generally not have the same hash code.
	 *
	 * @return the hash for this Number object.
	 */
	@Override
	int hashCode();

	/**
	 * Compares this Number with the specified Object for equality. Unlike {@link #hasSameValueOf(Number)}, this method
	 * considers two Number objects equal only if they are equal in value and scale (thus 2.0 is not equal to 2.00 when
	 * compared by this method).<br/>
	 *
	 * @param x
	 * 		The object to which this Number is to be compared.
	 *
	 * @return true if and only if the specified object has the same class and whose representation is equal in scale
	 * and value with this Number.
	 */
	@Override
	boolean equals(final Object x);
}
