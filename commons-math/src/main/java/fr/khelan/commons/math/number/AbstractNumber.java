package fr.khelan.commons.math.number;

import java.io.Serializable;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 * A number.
 */
public abstract class AbstractNumber<T extends Number<?, ?>, I extends Number<?, ?>> extends java.lang.Number
		implements Number<T, I>, Serializable {
	private static final long serialVersionUID = 1L;

	protected static final MathContext MATH_CONTEXT = new MathContext(1024, RoundingMode.HALF_UP);
}
