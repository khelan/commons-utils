package fr.khelan.commons.math.number;

import fr.khelan.commons.math.number.impl.NumberImpl;

import java.util.Objects;

public class CartesianComplex extends Complex implements NumberImpl<Complex, Complex> {
	private static final long serialVersionUID = 1L;

	/** The real part of this complex number. */
	private final Real real;

	/** The imaginary part of this complex number. */
	private final Real imaginary;

	/**
	 * Creates a new Complex given only the real part.
	 *
	 * @param real The real part value.
	 */
	protected CartesianComplex(final Real real) {
		this(real, Numbers.ZERO);
	}

	/**
	 * Creates a new Complex given the real and imaginary parts.
	 *
	 * @param real      The real part value.
	 * @param imaginary The imaginary part value.
	 */
	protected CartesianComplex(final Real real, final Real imaginary) {
		this.real = real;
		this.imaginary = imaginary;
	}

	/**
	 * Returns the real part.
	 *
	 * @return {@code a} in the Cartesian representation of this number
	 *         {@code a + b.i}
	 */
	@Override
	public Real real() {
		return real;
	}

	/**
	 * Returns the imaginary part.
	 *
	 * @return {@code b} in the Cartesian representation of this number
	 *         {@code a + b.i}
	 */
	@Override
	public Real imaginary() {
		return imaginary;
	}

	@Override
	public Real modulus() {
		return absoluteSquare().sqrt();
	}

	@Override
	public Real argument() {
		Real argument;
		if (isReal()) {
			if (real.compareTo(Numbers.ZERO) >= 0) {
				argument = Numbers.ZERO;
			} else {
				argument = Numbers.PI;
			}
		} else if (isImaginary()) {
			if (imaginary.compareTo(Numbers.ZERO) >= 0) {
				argument = Numbers.PI.divide(Numbers.TWO);
			} else {
				argument = Numbers.PI.divide(Numbers.TWO).negate();
			}
		} else {
			argument = Real.from(Math.atan2(imaginary.doubleValue(), real.doubleValue()));
		}
		return argument;
	}

	@Override
	public Real argumentPi() {
		Real argumentPi;
		if (isReal()) {
			if (real.compareTo(Numbers.ZERO) >= 0) {
				argumentPi = Numbers.ZERO;
			} else {
				argumentPi = Numbers.ONE;
			}
		} else if (isImaginary()) {
			if (imaginary.compareTo(Numbers.ZERO) >= 0) {
				argumentPi = Numbers.ONE_HALF;
			} else {
				argumentPi = Numbers.ONE_HALF.negate();
			}
		} else {
			argumentPi = Real.from(Math.atan2(imaginary.doubleValue(), real.doubleValue()) / Math.PI);
		}
		return argumentPi;
	}

	@Override
	public boolean hasSameValueOf(final Complex val) {
		return super.hasSameValueOf(val);
	}

	@Override
	public Complex add(final Complex complex) {
		return super.add(complex);
	}

	@Override
	public Complex subtract(final Complex complex) {
		return super.subtract(complex);
	}

	@Override
	public Complex multiply(final Complex factor) {
		return super.multiply(factor);
	}

	@Override
	public Complex divide(final Complex divisor) throws ArithmeticException {
		return super.divide(divisor);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (imaginary == null ? 0 : imaginary.hashCode());
		result = prime * result + (real == null ? 0 : real.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		return Objects.equals(real, ((CartesianComplex) obj).real())
			   && Objects.equals(imaginary, ((CartesianComplex) obj).imaginary);
	}
}
