package fr.khelan.commons.math.matrix.impl;

import fr.khelan.commons.math.matrix.AbstractMatrix;
import fr.khelan.commons.math.matrix.Matrix;
import fr.khelan.commons.math.matrix.element.Element;
import fr.khelan.commons.math.matrix.element.Position;
import fr.khelan.commons.math.matrix.exception.MultiplicationNotAvailableException;
import fr.khelan.commons.math.matrix.exception.SingularMatrixException;
import lombok.Getter;
import lombok.Setter;

public class DoubleMatrix extends AbstractMatrix<Double> {
	private final double[][] matrix;

	@Getter
	@Setter
	private int formatLength = 5;

	public DoubleMatrix(final Integer rowNumber, final Integer columnNumber) {
		super(rowNumber, columnNumber);
		matrix = new double[rowNumber][columnNumber];
	}

	public DoubleMatrix(final double[]... matrix) {
		super(matrix.length, matrix[0].length);
		this.matrix = new double[getRowNumber()][getColumnNumber()];
		for (int row = 0; row < getRowNumber(); row++) {
			for (int col = 0; col < getColumnNumber(); col++) {
				set(matrix[row][col], row, col);
			}
		}
	}

	public static final Matrix<Double> identityMatrix(final int order) {
		final Matrix<Double> identityMatrix = new DoubleMatrix(order, order);
		for (int i = 0; i < order; i++) {
			identityMatrix.set(1.0, i, i);
		}
		return identityMatrix;
	}

	@Override
	protected AbstractMatrix<Double> createNewMatrix(final Integer rowNumber, final Integer columnNumber) {
		return new DoubleMatrix(rowNumber, columnNumber);
	}

	@Override
	protected void multiplyLineByScalar(final int row, final Double scalar) {
		for (int c = 0; c < getColumnNumber(); c++) {
			set(get(row, c) * scalar, row, c);
		}
	}

	@Override
	protected void addLineMultipliedByScalarToOtherLine(final int sourceRow, final Double scalar, final int targetRow) {
		for (int c = 0; c < getColumnNumber(); c++) {
			set(get(sourceRow, c) * scalar + get(targetRow, c), targetRow, c);
		}
	}

	@Override
	public Double get(final Integer row, final Integer column) {
		checkIfInMatrix(row, column);
		return matrix[row][column];
	}

	@Override
	public Double getModulusValue(final Integer row, final Integer column) {
		return Math.abs(get(row, column));
	}

	@Override
	public void set(final Double value, final Integer row, final Integer column) {
		checkIfInMatrix(row, column);
		matrix[row][column] = value;
	}

	@Override
	public Matrix<Double> add(final Double scalar) {
		final Matrix<Double> sum = createNewMatrix();

		for (int r = 0; r < getRowNumber(); r++) {
			for (int c = 0; c < getColumnNumber(); c++) {
				sum.set(get(r, c) + scalar, r, c);
			}
		}
		return sum;
	}

	@Override
	public Matrix<Double> add(final Matrix<Double> addend) {
		final Matrix<Double> sum = createNewMatrix();

		for (int r = 0; r < getRowNumber(); r++) {
			for (int c = 0; c < getColumnNumber(); c++) {
				sum.set(get(r, c) + addend.get(r, c), r, c);
			}
		}
		return sum;
	}

	@Override
	public Matrix<Double> subtract(final Double scalar) {
		final Matrix<Double> diff = createNewMatrix();

		for (int r = 0; r < getRowNumber(); r++) {
			for (int c = 0; c < getColumnNumber(); c++) {
				diff.set(get(r, c) - scalar, r, c);
			}
		}
		return diff;
	}

	@Override
	public Matrix<Double> subtract(final Matrix<Double> subtrahend) {
		final Matrix<Double> diff = createNewMatrix();

		for (int r = 0; r < getRowNumber(); r++) {
			for (int c = 0; c < getColumnNumber(); c++) {
				diff.set(get(r, c) - subtrahend.get(r, c), r, c);
			}
		}
		return diff;
	}

	@Override
	public Double scalarProduct(final Matrix<Double> factor) {
		if (!canMultiplyWith(factor) || getRowNumber() != 1 || factor.getColumnNumber() != 1) {
			throw new MultiplicationNotAvailableException();
		}
		double scalar = 0.0;
		for (int i = 0; i < getColumnNumber(); i++) {
			scalar += get(0, i) * factor.get(i, 0);
		}
		return scalar;
	}

	@Override
	public Matrix<Double> multiply(final Double scalar) {
		final Matrix<Double> product = createNewMatrix();

		for (int r = 0; r < getRowNumber(); r++) {
			for (int c = 0; c < getColumnNumber(); c++) {
				product.set(get(r, c) * scalar, r, c);
			}
		}
		return product;
	}

	@Override
	public Matrix<Double> inverse() {
		checkIfIsSquareMatrix();
		final Matrix<Double> inverseMatrix = augment(identityMatrix(getRowNumber())).gaussianElimination();
		if (!identityMatrix(getRowNumber()).isTheSameOf(
				inverseMatrix.subMatrix(0, getRowNumber(), 0, getColumnNumber()))) {
			throw new SingularMatrixException();
		}
		return inverseMatrix.subMatrix(0, getRowNumber(), getRowNumber(), inverseMatrix.getColumnNumber());
	}

	@Override
	public Element<Double> max() {
		int row = 0;
		int column = 0;
		Double max = get(0, 0);

		for (int r = 0; r < getRowNumber(); r++) {
			for (int c = 0; c < getColumnNumber(); c++) {
				if (get(r, c) > max) {
					row = r;
					column = c;
					max = get(r, c);
				}
			}
		}
		return new Element<>(new Position(row, column), max);
	}

	@Override
	public Element<Double> maxModulus() {
		int row = 0;
		int column = 0;
		Double max = getModulusValue(0, 0);

		for (int r = 0; r < getRowNumber(); r++) {
			for (int c = 0; c < getColumnNumber(); c++) {
				if (getModulusValue(r, c) > max) {
					row = r;
					column = c;
					max = getModulusValue(r, c);
				}
			}
		}
		return new Element<>(new Position(row, column), get(row, column));
	}

	@Override
	public Double determinant() {
		checkIfIsSquareMatrix();
		if (getColumnNumber() == 1) {
			return get(0, 0);
		}
		double determinant = 0.0;
		for (int c = 0; c < getColumnNumber(); c++) {
			determinant += (c % 2 == 0 ? 1 : -1) * get(0, c) * subMatrix(0, c).determinant();
		}
		return determinant;
	}

	@Override
	public Matrix<Double> gaussianElimination() {
		final DoubleMatrix result = (DoubleMatrix) createCopy();
		int row = -1;
		for (int col = 0; col < getColumnNumber(); col++) {
			final Element<Double> max = result.subMatrix(row + 1, getRowNumber(), col, col + 1).maxModulus();
			final int maxRow = max.getPosition().getRow() + row + 1;
			final double maxValue = max.getValue();
			if (maxValue != 0.0) {
				row++;
				result.multiplyLineByScalar(maxRow, 1 / maxValue);
				if (maxRow != row) {
					result.switchLines(maxRow, row);
				}
				for (int i = 0; i < getRowNumber(); i++) {
					if (i != row) {
						result.addLineMultipliedByScalarToOtherLine(row, -result.get(i, col), i);
					}
				}
			}
			if (row >= getRowNumber() - 1) {
				break;
			}
		}

		return result;
	}

	@Override
	public String toString() {
		int nbChar = maxCharactersNumber() + 1;

		final StringBuilder st = new StringBuilder(getRowNumber() * (formatLength * getColumnNumber() + 4));
		for (int r = 0; r < getRowNumber(); r++) {
			st.append("|");
			for (int c = 0; c < getColumnNumber(); c++) {
				String number = get(r, c).toString();
				for (int i = 0; i < nbChar - number.length(); i++) {
					st.append(" ");
				}
				st.append(number);
			}
			st.append(" |\n");
		}

		return st.toString();
	}
}
