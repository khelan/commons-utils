package fr.khelan.commons.math.matrix;

import java.util.function.Function;

import fr.khelan.commons.math.matrix.element.Element;

public interface Matrix<T> {
	Integer getRowNumber();

	Integer getColumnNumber();

	T get(Integer row, Integer column);

	T getModulusValue(Integer row, Integer column);

	void set(T value, Integer row, Integer column);

	Matrix<T> map(final Function<? super T, ? extends T> mapper);

	Matrix<T> add(T scalar);

	Matrix<T> add(Matrix<T> addend);

	Matrix<T> subtract(T scalar);

	Matrix<T> subtract(Matrix<T> subtrahend);

	T scalarProduct(Matrix<T> factor);

	Matrix<T> multiply(T scalar);

	Matrix<T> multiply(Matrix<T> factor);

	Matrix<T> inverse();

	Matrix<T> transpose();

	Element<T> max();

	Element<T> maxModulus();

	T determinant();

	Matrix<T> gaussianElimination();

	boolean isSquareMatrix();

	boolean hasSameSizeOf(final Matrix<T> matrix);

	boolean canMultiplyWith(Matrix<T> matrix);

	Matrix<T> rowVector(final Integer row);

	Matrix<T> columnVector(final Integer column);

	Matrix<T> subMatrix(final Integer row, final Integer column);

	Matrix<T> subMatrix(final int rowStart, final int rowEnd, final int columnStart, final int columnEnd);

	boolean isTheSameOf(Matrix<T> matrix);
}
