package fr.khelan.commons.math.number.impl;

import java.math.BigInteger;

import fr.khelan.commons.math.number.Natural;
import fr.khelan.commons.math.number.Rational;

public class NaturalImpl extends Natural implements NumberImpl<Natural, Rational> {
	private static final long serialVersionUID = 1L;

	private final BigInteger value;

	public NaturalImpl(final BigInteger value) {
		if (value != null && value.signum() < 0) {
			throw new ArithmeticException("The value of the Natural must be positive or null.");
		}
		this.value = value;
	}

	@Override
	protected BigInteger integerValue() {
		return value;
	}
}
