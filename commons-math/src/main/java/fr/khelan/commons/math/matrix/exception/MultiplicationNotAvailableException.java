package fr.khelan.commons.math.matrix.exception;

public class MultiplicationNotAvailableException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public MultiplicationNotAvailableException() {
		super("These two matrix can be multiplied.");
	}
}
