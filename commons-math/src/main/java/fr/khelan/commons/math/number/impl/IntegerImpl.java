package fr.khelan.commons.math.number.impl;

import java.math.BigInteger;

import fr.khelan.commons.math.number.Integer;
import fr.khelan.commons.math.number.Rational;

public class IntegerImpl extends Integer implements NumberImpl<Integer, Rational> {
	private static final long serialVersionUID = 1L;

	private final BigInteger value;

	public IntegerImpl(final BigInteger value) {
		this.value = value;
	}

	@Override
	protected BigInteger integerValue() {
		return value;
	}

}
