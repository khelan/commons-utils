package fr.khelan.commons.math.matrix.impl;

import fr.khelan.commons.math.matrix.AbstractMatrix;
import fr.khelan.commons.math.matrix.Matrix;
import fr.khelan.commons.math.matrix.element.Element;
import fr.khelan.commons.math.matrix.element.Position;
import fr.khelan.commons.math.matrix.exception.MultiplicationNotAvailableException;

public class IntegerMatrix extends AbstractMatrix<Integer> {
	private final int[][] matrix;

	public static final Matrix<Integer> identityMatrix(final int order) {
		final Matrix<Integer> identityMatrix = new IntegerMatrix(order, order);
		for (int i = 0; i < order; i++) {
			identityMatrix.set(1, i, i);
		}
		return identityMatrix;
	}

	public IntegerMatrix(final Integer rowNumber, final Integer columnNumber) {
		super(rowNumber, columnNumber);
		matrix = new int[rowNumber][columnNumber];
	}

	@Override
	protected AbstractMatrix<Integer> createNewMatrix(final Integer rowNumber, final Integer columnNumber) {
		return new IntegerMatrix(rowNumber, columnNumber);
	}

	@Override
	public Integer get(final Integer row, final Integer column) {
		checkIfInMatrix(row, column);
		return matrix[row][column];
	}

	@Override
	public Integer getModulusValue(final Integer row, final Integer column) {
		return Math.abs(get(row, column));
	}

	@Override
	public void set(final Integer value, final Integer row, final Integer column) {
		checkIfInMatrix(row, column);
		matrix[row][column] = value;
	}

	@Override
	public Matrix<Integer> add(final Integer scalar) {
		final Matrix<Integer> sum = createNewMatrix();

		for (int r = 0; r < getRowNumber(); r++) {
			for (int c = 0; c < getColumnNumber(); c++) {
				sum.set(get(r, c) + scalar, r, c);
			}
		}
		return sum;
	}

	@Override
	public Matrix<Integer> add(final Matrix<Integer> addend) {
		final Matrix<Integer> sum = createNewMatrix();

		for (int r = 0; r < getRowNumber(); r++) {
			for (int c = 0; c < getColumnNumber(); c++) {
				sum.set(get(r, c) + addend.get(r, c), r, c);
			}
		}
		return sum;
	}

	@Override
	public Matrix<Integer> subtract(final Integer scalar) {
		final Matrix<Integer> diff = createNewMatrix();

		for (int r = 0; r < getRowNumber(); r++) {
			for (int c = 0; c < getColumnNumber(); c++) {
				diff.set(get(r, c) - scalar, r, c);
			}
		}
		return diff;
	}

	@Override
	public Matrix<Integer> subtract(final Matrix<Integer> subtrahend) {
		final Matrix<Integer> diff = createNewMatrix();

		for (int r = 0; r < getRowNumber(); r++) {
			for (int c = 0; c < getColumnNumber(); c++) {
				diff.set(get(r, c) - subtrahend.get(r, c), r, c);
			}
		}
		return diff;
	}

	@Override
	public Integer scalarProduct(final Matrix<Integer> factor) {
		if (!canMultiplyWith(factor) || getRowNumber() != 1 || factor.getColumnNumber() != 1) {
			throw new MultiplicationNotAvailableException();
		}
		int scalar = 0;
		for (int i = 0; i < getColumnNumber(); i++) {
			scalar += get(0, i) * factor.get(i, 0);
		}
		return scalar;
	}

	@Override
	public Matrix<Integer> multiply(final Integer scalar) {
		final Matrix<Integer> product = createNewMatrix();

		for (int r = 0; r < getRowNumber(); r++) {
			for (int c = 0; c < getColumnNumber(); c++) {
				product.set(get(r, c) * scalar, r, c);
			}
		}
		return product;
	}

	@Override
	public Matrix<Integer> inverse() {
		checkIfIsSquareMatrix();
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer determinant() {
		checkIfIsSquareMatrix();
		if (getColumnNumber() == 1) {
			return get(0, 0);
		}
		int determinant = 0;
		for (int c = 0; c < getColumnNumber(); c++) {
			determinant += (c % 2 == 0 ? 1 : -1) * get(0, c) * subMatrix(0, c).determinant();
		}
		return determinant;
	}

	@Override
	public Matrix<Integer> gaussianElimination() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void multiplyLineByScalar(final int row, final Integer scalar) {
		for (int c = 0; c < getColumnNumber(); c++) {
			set(get(row, c) * scalar, row, c);
		}
	}

	@Override
	protected void addLineMultipliedByScalarToOtherLine(final int sourceRow, final Integer scalar,
			final int targetRow) {
		for (int c = 0; c < getColumnNumber(); c++) {
			set(get(sourceRow, c) * scalar + get(targetRow, c), targetRow, c);
		}
	}

	@Override
	public Element<Integer> max() {
		int row = 0, column = 0;
		Integer max = get(0, 0);

		for (int r = 0; r < getRowNumber(); r++) {
			for (int c = 0; c < getColumnNumber(); c++) {
				if (get(r, c) > max) {
					row = r;
					column = c;
					max = get(r, c);
				}
			}
		}
		return new Element<>(new Position(row, column), max);
	}

	@Override
	public Element<Integer> maxModulus() {
		int row = 0, column = 0;
		Integer max = getModulusValue(0, 0);

		for (int r = 0; r < getRowNumber(); r++) {
			for (int c = 0; c < getColumnNumber(); c++) {
				if (getModulusValue(r, c) > max) {
					row = r;
					column = c;
					max = getModulusValue(r, c);
				}
			}
		}
		return new Element<>(new Position(row, column), get(row, column));
	}

	@Override
	public String toString() {
		int nbChar = maxCharactersNumber() + 1;
		String format = "% " + nbChar + "d";

		StringBuilder st = new StringBuilder(getRowNumber() * (nbChar * getColumnNumber() + 4));
		for (int r = 0; r < getRowNumber(); r++) {
			st.append("|");
			for (int c = 0; c < getColumnNumber(); c++) {
				st.append(String.format(format, get(r, c)));
			}
			st.append(" |\n");
		}

		return st.toString();
	}
}
