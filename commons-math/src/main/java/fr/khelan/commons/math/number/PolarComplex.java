package fr.khelan.commons.math.number;

import java.math.BigDecimal;
import java.util.Objects;

import fr.khelan.commons.math.number.impl.NumberImpl;

public class PolarComplex extends Complex implements NumberImpl<Complex, Complex> {
	protected static final Complex ZERO = new PolarComplex(Numbers.ZERO, Numbers.ZERO);
	protected static final Complex ONE = new PolarComplex(Numbers.ONE, Numbers.ZERO);
	protected static final Complex I = new PolarComplex(Numbers.ONE, Numbers.ONE_HALF);
	private static final long serialVersionUID = 1L;
	private final Real modulus;
	private final Real argumentPi;

	/**
	 * Creates a new Complex in the polar representation r.e^(θ.π.i).
	 *
	 * @param modulus
	 * 		The modulus part r.
	 * @param argumentPi
	 * 		The argument part θ, without Pi multiplicator.
	 */
	protected PolarComplex(final Real modulus, final Real argumentPi) {
		this.modulus = modulus;
		if (modulus != null && !modulus.isZero()) {
			this.argumentPi = modulo2Pi(argumentPi);
		} else {
			this.argumentPi = Numbers.ZERO;
		}
	}

	private static Real modulo2Pi(final Real argumentPi) {
		if (Numbers.ONE.compareTo(argumentPi) < 0) {
			return modulo2Pi(argumentPi.subtract(Numbers.TWO));
		} else if (Numbers.ONE.negate().compareTo(argumentPi) >= 0) {
			return modulo2Pi(argumentPi.add(Numbers.TWO));
		} else {
			return argumentPi;
		}
	}

	/**
	 * Checks if this is a real number, i.e. its imaginary part is null.
	 *
	 * @return true if this number is real, false otherwise.
	 */
	@Override
	public boolean isReal() {
		final Real arg = modulo2Pi(argumentPi);
		return arg.hasSameValueOf(Numbers.ZERO) || arg.hasSameValueOf(Numbers.ONE);
	}

	/**
	 * Checks if this is a purely imaginary number, i.e. its real part is null.
	 *
	 * @return true if this number is purely imaginary, false otherwise.
	 */
	@Override
	public boolean isImaginary() {
		final Real arg = modulo2Pi(argumentPi);
		return modulus.hasSameValueOf(Numbers.ZERO) || arg.hasSameValueOf(Numbers.ONE_HALF)
			   || arg.hasSameValueOf(Numbers.ONE_HALF.negate());
	}

	/**
	 * Checks if this number has the same value of 0, i.e. its real and imaginary parts are nulls.
	 *
	 * @return true if this number is null, false otherwise.
	 */
	@Override
	public boolean isZero() {
		return modulus.hasSameValueOf(Numbers.ZERO);
	}

	@Override
	public Real real() {
		final Real arg = modulo2Pi(argumentPi);
		if (arg.hasSameValueOf(Numbers.ZERO)) {
			return modulus;
		} else if (arg.hasSameValueOf(Numbers.ONE)) {
			return modulus.negate();
		} else if (arg.hasSameValueOf(Numbers.ONE_HALF) || arg.hasSameValueOf(Numbers.ONE_HALF.negate())) {
			return Numbers.ZERO;
		} else {
			return modulus.multiply(
					Real.from(BigDecimal.valueOf(Math.cos(argumentPi.multiply(Numbers.PI).doubleValue()))));
		}
	}

	@Override
	public Real imaginary() {
		final Real arg = modulo2Pi(argumentPi);
		if (arg.hasSameValueOf(Numbers.ZERO) || arg.hasSameValueOf(Numbers.ONE)) {
			return Numbers.ZERO;
		} else if (arg.hasSameValueOf(Numbers.ONE_HALF)) {
			return modulus;
		} else if (arg.hasSameValueOf(Numbers.ONE_HALF.negate())) {
			return modulus.negate();
		} else {
			return modulus.multiply(
					Real.from(BigDecimal.valueOf(Math.sin(argumentPi.multiply(Numbers.PI).doubleValue()))));
		}
	}

	@Override
	public Real absoluteSquare() {
		return modulus.multiply(modulus);
	}

	@Override
	public Real modulus() {
		return modulus;
	}

	@Override
	public Real argument() {
		return argumentPi.multiply(Numbers.PI);
	}

	@Override
	public Real argumentPi() {
		return argumentPi;
	}

	@Override
	public Complex add(final Complex complex) {
		final Complex sum = new CartesianComplex(real(), imaginary()).add(complex);
		return new PolarComplex(sum.modulus(), sum.argument().divide(Numbers.PI));
	}

	@Override
	public Complex subtract(final Complex complex) {
		final Complex result = new CartesianComplex(real(), imaginary()).subtract(complex);
		return new PolarComplex(result.modulus(), result.argument().divide(Numbers.PI));
	}

	@Override
	public Complex conjugate() {
		return new PolarComplex(modulus, argumentPi.negate());
	}

	@Override
	public Complex negate() {
		if (modulus.hasSameValueOf(Numbers.ZERO)) {
			return this;
		} else {
			return new PolarComplex(modulus, modulo2Pi(argumentPi.add(Numbers.ONE)));
		}
	}

	@Override
	public Complex multiply(final Complex factor) {
		return new PolarComplex(modulus.multiply(factor.modulus()), modulo2Pi(argumentPi.add(factor.argumentPi())));
	}

	@Override
	public Complex divide(final Complex divisor) throws ArithmeticException {
		final Real divisorModulus = divisor.modulus();
		if (divisorModulus.hasSameValueOf(Numbers.ZERO)) {
			throw new ArithmeticException("Divisor must not be null.");
		}
		return new PolarComplex(modulus.divide(divisorModulus), modulo2Pi(argumentPi.subtract(divisor.argumentPi())));
	}

	@Override
	public Complex inverse() {
		if (modulus.equals(Numbers.ZERO)) {
			throw new ArithmeticException("This number must not be null.");
		}
		return new PolarComplex(Numbers.ONE.divide(modulus), argumentPi.negate());
	}

	@Override
	public boolean hasSameValueOf(final Complex otherNumber) {
		return modulus.hasSameValueOf(otherNumber.modulus()) && argumentPi.hasSameValueOf(otherNumber.argumentPi());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (argumentPi == null ? 0 : argumentPi.hashCode());
		result = prime * result + (modulus == null ? 0 : modulus.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		return Objects.equals(modulus, ((PolarComplex) obj).modulus)
			   && Objects.equals(argumentPi, ((PolarComplex) obj).argumentPi);
	}
}
