package fr.khelan.commons.math.number;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Objects;

import fr.khelan.commons.math.number.impl.DecimalImpl;

/**
 * A Decimal number from &#8517;.
 */
public abstract class Decimal extends Rational {
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new Decimal.
	 *
	 * @param value The value of this new Decimal.
	 *
	 * @return The new Decimal.
	 */
	public static Decimal from(final String value) {
		return new DecimalImpl(new BigDecimal(value));
	}

	/**
	 * Creates a new Decimal.
	 *
	 * @param value The value of this new Decimal.
	 *
	 * @return The new Decimal.
	 */
	public static Decimal from(final BigDecimal value) {
		return new DecimalImpl(value);
	}

	/**
	 * Returns the value backed by this {@link Decimal}.
	 *
	 * @return The {@link BigDecimal} backed by this Decimal.
	 */
	protected abstract BigDecimal decimalValue();

	@Override
	public Integer numerator() {
		return rationalValue().numerator();
	}

	@Override
	public Integer denominator() {
		return rationalValue().denominator();
	}

	/**
	 * Returns the irreductible fraction from this Decimal.
	 *
	 * @return The {@link Rational} representing the irreductible fraction of this.
	 */
	private Rational rationalValue() {
		return Rational.irreductibleFraction(decimalValue().unscaledValue(),
				BigInteger.TEN.pow(decimalValue().scale()));
	}

	@Override
	protected BigDecimal realValue() {
		return decimalValue();
	}

	@Override
	public Decimal abs() {
		return from(decimalValue().abs());
	}

	@Override
	public int signum() {
		return decimalValue().signum();
	}

	/**
	 * Returns a {@link Decimal} whose value is {@code (this + addend)}.
	 *
	 * @param addend the other number to be added to this.
	 *
	 * @return {@code this + addend}
	 */
	public Decimal add(final Decimal addend) {
		return from(decimalValue().add(addend.decimalValue()));
	}

	/**
	 * Returns a {@link Decimal} whose value is {@code (this - subtrahend)}.
	 *
	 * @param subtrahend the other number to be subtracted to this.
	 *
	 * @return {@code this - subtrahend}
	 */
	public Decimal subtract(final Decimal subtrahend) {
		return from(decimalValue().subtract(subtrahend.decimalValue()));
	}

	/**
	 * Returns a {@link Decimal} which is the negative number of this number.
	 *
	 * @return {@code -this}
	 */
	@Override
	public Decimal negate() {
		return from(decimalValue().negate());
	}

	/**
	 * Returns a {@link Decimal} whose value is {@code (this} &times;
	 * {@code factor)}.
	 *
	 * @param factor the other number to be multiplied by this.
	 *
	 * @return this &times; factor
	 */
	public Decimal multiply(final Decimal factor) {
		return from(decimalValue().multiply(factor.decimalValue()));
	}

	/**
	 * Returns a {@link Rational} whose value is {@code (this / divisor)}.
	 *
	 * @param divisor the other number by which this is to be divided.
	 *
	 * @return this &divide; divisor
	 *
	 * @throws ArithmeticException if the divisor is {@code null} or {@code 0}.
	 */
	public Rational divide(final Decimal divisor) throws ArithmeticException {
		return super.divide(divisor);
	}

	/**
	 * Returns a {@link Rational} which is the multiplicative inverse whose value is
	 * {@code (1 / this)}.
	 *
	 * @return 1 / this
	 *
	 * @throws ArithmeticException if this number is equals to {@code 0}.
	 */
	@Override
	public Rational inverse() {
		return super.inverse();
	}

	@Override
	public Decimal pow(final Integer n) {
		return from(decimalValue().pow(n.intValue()));
	}

	/**
	 * Compare the number {@code d} with {@code this}, only by value, without
	 * considering the scale of both numbers.
	 *
	 * @param d the other number to compare with.
	 * @return {@code true} if {@code this} has the same value of {@code d},
	 *         {@code false} otherwise.
	 */
	public boolean hasSameValueOf(final Decimal d) {
		return decimalValue().compareTo(d.decimalValue()) == 0;
	}

	@Override
	public int intValue() {
		return decimalValue().intValue();
	}

	@Override
	public long longValue() {
		return decimalValue().longValue();
	}

	@Override
	public float floatValue() {
		return decimalValue().floatValue();
	}

	@Override
	public double doubleValue() {
		return decimalValue().doubleValue();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (decimalValue() == null ? 0 : decimalValue().hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		return Objects.equals(decimalValue(), ((Decimal) obj).decimalValue());
	}

	public String toDecimalString() {
		return decimalValue().toString();
	}

	@Override
	public String toString() {
		return toDecimalString();
	}
}
