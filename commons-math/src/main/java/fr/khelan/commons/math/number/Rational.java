package fr.khelan.commons.math.number;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Objects;

import fr.khelan.commons.math.number.impl.RationalImpl;

/**
 * A Rational number from &#8474;
 */
public abstract class Rational extends Real {
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new Rational.
	 *
	 * @param numerator
	 * 		The integer value of the numerator.
	 * @param denominator
	 * 		The integer value of the denominator.
	 *
	 * @return {@code numerator / denominator}
	 *
	 * @throws ArithmeticException
	 * 		if the denominator is {@code null} or {@code 0}.
	 */
	public static Rational from(final Integer numerator, final Integer denominator) {
		return irreductibleFraction(numerator.integerValue(), denominator.integerValue());
	}

	/**
	 * Creates a new Rational as an irreductible fraction.
	 *
	 * @param numerator
	 * 		The integer value of the numerator.
	 * @param denominator
	 * 		The integer value of the denominator.
	 *
	 * @return {@code numerator / denominator} as an irreductible fraction.
	 *
	 * @throws ArithmeticException
	 * 		if the denominator is {@code 0}.
	 */
	protected static Rational irreductibleFraction(final BigInteger numerator, final BigInteger denominator) {
		if (denominator == null || BigInteger.ZERO.compareTo(denominator) == 0) {
			throw new ArithmeticException("Denominator must not be null or equals to 0.");
		}
		if (numerator == null || BigInteger.ZERO.compareTo(numerator) == 0) {
			return Numbers.ZERO;
		} else {
			BigInteger num = numerator;
			final BigInteger den = denominator.abs();
			if (denominator.signum() < 0) {
				num = num.negate();
			}
			final BigInteger gcd = num.gcd(den);
			return new RationalImpl(Integer.from(num.divide(gcd)), Integer.from(den.divide(gcd)));
		}
	}

	/**
	 * Creates a new Rational.
	 *
	 * @param numerator
	 * 		The integer value of the numerator.
	 * @param denominator
	 * 		The integer value of the denominator.
	 *
	 * @return {@code numerator / denominator}
	 *
	 * @throws ArithmeticException
	 * 		if the denominator is {@code null} or {@code 0}.
	 */
	public static Rational from(final BigInteger numerator, final BigInteger denominator) {
		return irreductibleFraction(numerator, denominator);
	}

	/**
	 * Creates a new Rational.
	 *
	 * @param numerator
	 * 		The integer value of the numerator.
	 * @param denominator
	 * 		The integer value of the denominator.
	 *
	 * @return {@code numerator / denominator}
	 *
	 * @throws ArithmeticException
	 * 		if the denominator is {@code null} or {@code 0}.
	 */
	public static Rational from(final String numerator, final String denominator) {
		return irreductibleFraction(new BigInteger(numerator), new BigInteger(denominator));
	}

	/**
	 * Creates a new Rational.
	 *
	 * @param numerator
	 * 		The integer value of the numerator.
	 * @param denominator
	 * 		The integer value of the denominator.
	 *
	 * @return {@code numerator / denominator}
	 *
	 * @throws ArithmeticException
	 * 		if the denominator is {@code 0}.
	 */
	public static Rational from(final long numerator, final long denominator) {
		return irreductibleFraction(BigInteger.valueOf(numerator), BigInteger.valueOf(denominator));
	}

	/**
	 * Returns the numerator.
	 *
	 * @return The integer value of the numerator.
	 */
	public abstract Integer numerator();

	/**
	 * Returns the denominator.
	 *
	 * @return The integer value of the denominator.
	 */
	public abstract Integer denominator();

	@Override
	protected BigDecimal realValue() {
		return Real.from(numerator().realValue()).divide(denominator()).realValue();
	}

	/**
	 * Returns a {@link Rational} which is the negative number of this number.
	 *
	 * @return {@code -this}
	 */
	@Override
	public Rational negate() {
		return from(numerator().negate(), denominator());
	}

	/**
	 * Returns a {@link Rational} which is the multiplicative inverse whose value is {@code (1 / this)}.
	 *
	 * @return {@code denominator / numerator}
	 *
	 * @throws ArithmeticException
	 * 		if this number is equals to {@code 0}.
	 */
	@Override
	public Rational inverse() {
		return from(denominator(), numerator());
	}

	@Override
	public Rational pow(final Integer n) {
		return from(numerator().pow(n), denominator().pow(n));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (numerator() == null ? 0 : numerator().hashCode());
		result = prime * result + (denominator() == null ? 0 : denominator().hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		return Objects.equals(numerator(), ((Rational) obj).numerator())
			   && Objects.equals(denominator(), ((Rational) obj).denominator());
	}

	@Override
	public String toString() {
		return toRationalString();
	}

	@Override
	public Rational abs() {
		return from(numerator().abs(), denominator().abs());
	}

	@Override
	public int signum() {
		return numerator().signum() * denominator().signum();
	}

	/**
	 * Returns a {@link Rational} whose value is {@code (this + addend)}.
	 *
	 * @param addend
	 * 		the other number to be added to this.
	 *
	 * @return {@code this + addend}
	 */
	public Rational add(final Rational addend) {
		return from(numerator().multiply(addend.denominator()).add(addend.numerator().multiply(denominator())),
					denominator().multiply(addend.denominator()));
	}

	/**
	 * Returns a {@link Rational} whose value is {@code (this - subtrahend)}.
	 *
	 * @param subtrahend
	 * 		the other number to be subtracted to this.
	 *
	 * @return {@code this - subtrahend}
	 */
	public Rational subtract(final Rational subtrahend) {
		return from(
				numerator().multiply(subtrahend.denominator()).subtract(subtrahend.numerator().multiply(denominator())),
				denominator().multiply(subtrahend.denominator()));
	}

	/**
	 * Returns a {@link Rational} whose value is {@code (this} &times; {@code factor)}.
	 *
	 * @param factor
	 * 		the other number to be multiplied by this.
	 *
	 * @return this &times; factor
	 */
	public Rational multiply(final Rational factor) {
		return from(numerator().multiply(factor.numerator()), denominator().multiply(factor.denominator()));
	}

	/**
	 * Returns a {@link Rational} whose value is {@code (this / divisor)}.
	 *
	 * @param divisor
	 * 		the other number by which this is to be divided.
	 *
	 * @return this &divide; divisor
	 *
	 * @throws ArithmeticException
	 * 		if the divisor is {@code null} or {@code 0}.
	 */
	public Rational divide(final Rational divisor) throws ArithmeticException {
		if (divisor == null || divisor.hasSameValueOf(Numbers.ZERO)) {
			throw new ArithmeticException("Divisor must not be null or equals to 0.");
		}
		return from(numerator().multiply(divisor.denominator()), denominator().multiply(divisor.numerator()));
	}

	/**
	 * Compare the number q with this, only by value, without considering the scale of both numbers.
	 *
	 * @param q
	 * 		the other number to compare with.
	 *
	 * @return {@code true} if {@code this} has the same value of {@code q}, {@code false} otherwise.
	 */
	public boolean hasSameValueOf(final Rational q) {
		return numerator().compareTo(q.numerator()) == 0 && denominator().compareTo(q.denominator()) == 0;
	}

	public String toRationalString() {
		return numerator() + " / " + denominator();
	}
}
