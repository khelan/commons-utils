package fr.khelan.commons.math.number.impl;

import java.math.BigDecimal;

import fr.khelan.commons.math.number.Decimal;
import fr.khelan.commons.math.number.Rational;

public class DecimalImpl extends Decimal implements NumberImpl<Decimal, Rational> {
	private static final long serialVersionUID = 1L;

	private final BigDecimal value;

	public DecimalImpl(final BigDecimal value) {
		this.value = value;
	}

	@Override
	protected BigDecimal decimalValue() {
		return value;
	}

}
