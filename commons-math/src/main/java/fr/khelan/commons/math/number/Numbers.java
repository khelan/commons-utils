package fr.khelan.commons.math.number;

import java.math.BigInteger;

public final class Numbers {
	/** Constant for 0 */
	public static final Integer ZERO = Integer.from(BigInteger.ZERO);
	/** Constant for 1 */
	public static final Integer ONE = Integer.from(BigInteger.ONE);
	/** Constant for 2 */
	public static final Integer TWO = Integer.from(BigInteger.TWO);
	/** Constant for 3 */
	public static final Integer THREE = Integer.from(3L);
	/** Constant for 10 */
	public static final Integer TEN = Integer.from(BigInteger.TEN);
	/** Constant for the complex number &#8520; */
	public static final Complex I = Complex.from(ZERO, ONE);
	/** Constant for the complex number &frac12; */
	public static final Decimal ONE_HALF = Decimal.from("0.5");
	/** Constant for the Real &pi; */
	public static final Real PI = Real.from(Math.PI);
	/** Constant for the Real &#8519; */
	public static final Real E = Real.from(Math.E);
	private Numbers() {
		throw new AssertionError("Constants class that must not be instantiate.");
	}
}
