package fr.khelan.commons.math.number;

/**
 * A Complex number from &#8450;.
 */
public abstract class Complex extends AbstractNumber<Complex, Complex> {
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new Complex given only the real part.
	 *
	 * @param real The real part value.
	 */
	public static Complex from(final Real real) {
		return new CartesianComplex(real);
	}

	/**
	 * Creates a new Complex given the real (a) and imaginary (b) parts for the
	 * number a + b.&#8520;
	 *
	 * @param a The real part value.
	 * @param b The imaginary part value.
	 */
	public static Complex from(final Real a, final Real b) {
		return new CartesianComplex(a, b);
	}

	/**
	 * Creates a new Complex in the polar representation
	 * r.&#8519;<sup>&theta;.&#8520;</sup>
	 *
	 * @param r     The modulus part.
	 * @param theta The argument part &theta;.
	 */
	public static Complex fromPolarRepresentation(final Real r, final Real theta) {
		return new PolarComplex(r, theta);
	}

	/**
	 * Creates a new Complex in the Cartesian representation.
	 *
	 * @param complex The complex value.
	 */
	public static Complex toCartesianRepresentation(final Complex complex) {
		if (complex instanceof CartesianComplex) {
			return complex;
		} else {
			return new CartesianComplex(complex.real(), complex.argument());
		}
	}

	/**
	 * Creates a new Complex in the polar representation.
	 *
	 * @param complex The complex value.
	 */
	public static Complex toPolarRepresentation(final Complex complex) {
		if (complex instanceof PolarComplex) {
			return complex;
		} else {
			return new PolarComplex(complex.modulus(), complex.argumentPi());
		}
	}

	/**
	 * Checks if this is a real number, i.e. its imaginary part is null.
	 *
	 * @return true if this number is real, false otherwise.
	 */
	public boolean isReal() {
		return imaginary().hasSameValueOf(Numbers.ZERO);
	}

	/**
	 * Checks if this is a purely imaginary number, i.e. its real part is null.
	 *
	 * @return true if this number is purely imaginary, false otherwise.
	 */
	public boolean isImaginary() {
		return real().hasSameValueOf(Numbers.ZERO);
	}

	/**
	 * Checks if this number has the same value of 0, i.e. its real and imaginary
	 * parts are nulls.
	 *
	 * @return true if this number is null, false otherwise.
	 */
	@Override
	public boolean isZero() {
		return hasSameValueOf(Numbers.ZERO);
	}

	/**
	 * Returns the real part.
	 *
	 * @return {@code a} in the Cartesian representation of this number
	 *         {@code a + b.}&#8520;
	 */
	public abstract Real real();

	/**
	 * Returns the imaginary part.
	 *
	 * @return {@code b} in the Cartesian representation of this number
	 *         {@code a + b.}&#8520;
	 */
	public abstract Real imaginary();

	/**
	 * Returns a Number whose value is the absolute value of this Number.
	 *
	 * @return {@code abs(this)}
	 */
	@Override
	public Real abs() {
		return modulus();
	}

	/**
	 * Returns the sign of this Number.
	 *
	 * @return -1, 0 or 1 as the value of this Number is negative, zero or positive.
	 */
	@Override
	public int signum() {
		throw new UnsupportedOperationException("Complex number have no sign.");
	}

	/**
	 * The square of the absolute value (modulus).
	 *
	 * @return {@code a² + b²} in the Cartesian representation of this number
	 *         {@code a + b.}&#8520;
	 */
	public Real absoluteSquare() {
		return real().pow(Numbers.TWO).add(imaginary().pow(Numbers.TWO));
	}

	/**
	 * Returns the modulus.<br/>
	 *
	 * @return {@code r} in the polar representation of this number
	 *         r.&#8519;<sup>&theta;.&#8520;</sup>
	 */
	public abstract Real modulus();

	/**
	 * Returns the argument.
	 *
	 * @return {@code theta} in the polar representation of this number
	 *         r.&#8519;<sup>&theta;.&#8520;</sup>
	 */
	public abstract Real argument();

	/**
	 * Returns the argument.
	 *
	 * @return {@code theta}, without Pi multiplicator, in the polar representation
	 *         of this number r.&#8519;<sup>&theta;.&pi;.&#8520;</sup>
	 */
	public abstract Real argumentPi();

	/**
	 * Returns a {@link Complex} whose value is {@code (this + complex)}.
	 *
	 * @param complex the other complex number to be added to this {@code Complex}.
	 *
	 * @return {@code this + complex}
	 */
	@Override
	public Complex add(final Complex complex) {
		return Complex.from(real().add(complex.real()), imaginary().add(complex.imaginary()));
	}

	/**
	 * Returns a {@link Complex} whose value is {@code (this - complex)}.
	 *
	 * @param complex the other complex number to be subtracted to this
	 *                {@code Complex}.
	 *
	 * @return {@code this - complex}
	 */
	@Override
	public Complex subtract(final Complex complex) {
		return Complex.from(real().subtract(complex.real()), imaginary().subtract(complex.imaginary()));
	}

	/**
	 * Returns a {@link Complex} which is the conjugate of this complex number, ie.
	 * {@code a - b.}&#8520;
	 *
	 * @return The complex conjugate {@code a - b.}&#8520;
	 */
	public Complex conjugate() {
		return Complex.from(real(), imaginary().negate());
	}

	/**
	 * Returns a {@link Complex} which is the negative number of this complex
	 * number, ie. {@code -a - b.}&#8520;
	 *
	 * @return The complex {@code -a - b.}&#8520;
	 */
	@Override
	public Complex negate() {
		return Complex.from(real().negate(), imaginary().negate());
	}

	/**
	 * Returns a {@link Complex} whose value is {@code (this} &times;
	 * {@code factor)}.
	 *
	 * @param factor the other complex number to be multiplied by this
	 *               {@code Complex}.
	 *
	 * @return this &times; factor
	 */
	@Override
	public Complex multiply(final Complex factor) {
		return Complex.from(real().multiply(factor.real()).subtract(imaginary().multiply(factor.imaginary())),
				real().multiply(factor.imaginary()).add(imaginary().multiply(factor.real())));
	}

	/**
	 * Returns a {@link Complex} whose value is {@code (this / divisor)}.
	 *
	 * @param divisor the other complex number by which {@code Complex} is to be
	 *                divided.
	 *
	 * @return this &divide; divisor
	 *
	 * @throws ArithmeticException if the divisor is {@code null} or {@code 0}.
	 */
	@Override
	public Complex divide(final Complex divisor) throws ArithmeticException {
		if (divisor == null) {
			throw new ArithmeticException("Divisor must not be null.");
		}
		final Real absoluteSquare = divisor.absoluteSquare();
		if (absoluteSquare.equals(Numbers.ZERO)) {
			throw new ArithmeticException("Divisor must not be equals to 0.");
		}
		final Complex numerator = Complex.from(
				real().multiply(divisor.real()).add(imaginary().multiply(divisor.imaginary())),
				imaginary().multiply(divisor.real()).add(real().multiply(divisor.imaginary()).negate()));
		return numerator.divide(absoluteSquare);
	}

	/**
	 * Returns a {@link Complex} whose value is {@code (this / divisor)}.
	 *
	 * @param divisor the {@link Real} number by which {@code Complex} is to be
	 *                divided.
	 *
	 * @return this &divide; divisor
	 *
	 * @throws ArithmeticException if the divisor is {@code null} or {@code 0}.
	 */
	protected Complex divide(final Real divisor) {
		if (divisor == null || divisor.hasSameValueOf(Numbers.ZERO)) {
			throw new ArithmeticException("Divisor must not be null or equals to 0.");
		}
		return Complex.from(real().divide(divisor), imaginary().divide(divisor));
	}

	/**
	 * Returns a {@link Complex} which is the multiplicative inverse whose value is
	 * {@code (1 / this)}.
	 *
	 * @return 1 &divide; this
	 *
	 * @throws ArithmeticException if this number is equals to {@code 0}.
	 */
	@Override
	public Complex inverse() {
		if (absoluteSquare().equals(Numbers.ZERO)) {
			throw new ArithmeticException("This number must not be equals to 0.");
		}
		return Numbers.ONE.divide(this);
	}

	/**
	 * <p>
	 * Returns a number whose value is ( this<sup>n</sup> ).
	 * </p>
	 * <p>
	 * The parameter n must be in the range 0 through 999999999, inclusive.
	 * </p>
	 *
	 * @param n the power to raise this complex number
	 * @return this<sup>n</sup>
	 */
	@Override
	public Complex pow(final Integer n) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Returns a {@link Complex} whose value is the Square Root of this number.
	 *
	 * @return &radic;(this)
	 */
	@Override
	public Complex sqrt() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Compare the number z with this, only by value, without considering the scale
	 * of both numbers.
	 *
	 * @param val the other number to compare with.
	 * @return {@code true} if this has the same value of z, {@code false}
	 *         otherwise.
	 */
	@Override
	public boolean hasSameValueOf(final Complex val) {
		return real().hasSameValueOf(val.real()) && imaginary().hasSameValueOf(val.imaginary());
	}

	@Override
	public int intValue() {
		throw unconvertedException();
	}

	@Override
	public long longValue() {
		throw unconvertedException();
	}

	@Override
	public float floatValue() {
		throw unconvertedException();
	}

	@Override
	public double doubleValue() {
		throw unconvertedException();
	}

	private UnsupportedOperationException unconvertedException() {
		return new UnsupportedOperationException("Complex number cannot be converted.");
	}

	@Override
	public String toString() {
		return real() + " + " + imaginary() + " i";
	}
}
