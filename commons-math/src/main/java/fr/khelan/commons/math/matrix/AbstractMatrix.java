package fr.khelan.commons.math.matrix;

import java.util.function.Function;

import fr.khelan.commons.math.matrix.exception.MultiplicationNotAvailableException;
import fr.khelan.commons.math.matrix.exception.NotSquareMatrixException;
import fr.khelan.commons.math.matrix.exception.OutOfMatrixBoundsException;

public abstract class AbstractMatrix<T> implements Matrix<T> {

	private final Integer rowNumber;

	private final Integer columnNumber;

	protected AbstractMatrix(final Integer rowNumber, final Integer columnNumber) {
		if (rowNumber <= 0) {
			throw new IllegalArgumentException("The row number must be a positive integer.");
		}
		if (columnNumber <= 0) {
			throw new IllegalArgumentException("The column number must be a positive integer.");
		}
		this.rowNumber = rowNumber;
		this.columnNumber = columnNumber;
	}

	protected AbstractMatrix<T> createNewMatrix() {
		return createNewMatrix(getRowNumber(), getColumnNumber());
	}

	protected abstract AbstractMatrix<T> createNewMatrix(Integer rowNumber, Integer columnNumber);

	protected AbstractMatrix<T> createCopy() {
		final AbstractMatrix<T> copy = createNewMatrix(getRowNumber(), getColumnNumber());
		for (int row = 0; row < getRowNumber(); row++) {
			for (int column = 0; column < getColumnNumber(); column++) {
				copy.set(get(row, column), row, column);
			}
		}
		return copy;
	}

	protected void checkIfInMatrix(final Integer row, final Integer column) {
		if (row < 0 || row >= getRowNumber()) {
			throw new OutOfMatrixBoundsException(row, getRowNumber() - 1, true);
		}
		if (column < 0 || column >= getColumnNumber()) {
			throw new OutOfMatrixBoundsException(column, getColumnNumber() - 1, false);
		}
	}

	protected void checkIfIsSquareMatrix() {
		if (!isSquareMatrix()) {
			throw new NotSquareMatrixException();
		}
	}

	public AbstractMatrix<T> augment(final Matrix<T> matrix) {
		final AbstractMatrix<T> result = createNewMatrix(getRowNumber(), getColumnNumber() + matrix.getColumnNumber());
		for (int r = 0; r < getRowNumber(); r++) {
			for (int c = 0; c < getColumnNumber(); c++) {
				result.set(get(r, c), r, c);
			}
		}
		for (int r = 0; r < matrix.getRowNumber(); r++) {
			for (int c = 0; c < matrix.getColumnNumber(); c++) {
				result.set(matrix.get(r, c), r, c + getColumnNumber());
			}
		}

		return result;
	}

	protected void switchLines(final int row1, final int row2) {
		T valueRow1;
		for (int c = 0; c < getColumnNumber(); c++) {
			valueRow1 = get(row1, c);
			set(get(row2, c), row1, c);
			set(valueRow1, row2, c);
		}
	}

	protected abstract void multiplyLineByScalar(int row, T value);

	protected abstract void addLineMultipliedByScalarToOtherLine(final int sourceRow, final T scalar,
			final int targetRow);

	@Override
	public Integer getRowNumber() {
		return rowNumber;
	}

	@Override
	public Integer getColumnNumber() {
		return columnNumber;
	}

	@Override
	public Matrix<T> map(final Function<? super T, ? extends T> mapper) {
		final Matrix<T> result = createNewMatrix();

		for (int r = 0; r < getRowNumber(); r++) {
			for (int c = 0; c < getColumnNumber(); c++) {
				result.set(mapper.apply(get(r, c)), r, c);
			}
		}
		return result;
	}

	@Override
	public Matrix<T> multiply(final Matrix<T> factor) {
		if (!canMultiplyWith(factor)) {
			throw new MultiplicationNotAvailableException();
		}
		final Matrix<T> product = createNewMatrix(getRowNumber(), factor.getColumnNumber());
		for (int row = 0; row < getRowNumber(); row++) {
			for (int col = 0; col < factor.getColumnNumber(); col++) {
				product.set(rowVector(row).scalarProduct(factor.columnVector(col)), row, col);
			}
		}
		return product;
	}

	@Override
	public Matrix<T> transpose() {
		final Matrix<T> transpose = createNewMatrix(getColumnNumber(), getRowNumber());

		for (int r = 0; r < getRowNumber(); r++) {
			for (int c = 0; c < getColumnNumber(); c++) {
				transpose.set(get(r, c), c, r);
			}
		}
		return transpose;
	}

	@Override
	public boolean isSquareMatrix() {
		return getRowNumber().equals(getColumnNumber());
	}

	@Override
	public boolean hasSameSizeOf(final Matrix<T> matrix) {
		return getRowNumber().equals(matrix.getRowNumber()) && getColumnNumber().equals(matrix.getColumnNumber());
	}

	@Override
	public boolean canMultiplyWith(final Matrix<T> matrix) {
		return getColumnNumber().equals(matrix.getRowNumber());
	}

	@Override
	public Matrix<T> rowVector(final Integer row) {
		final Matrix<T> vector = createNewMatrix(1, getColumnNumber());
		for (int col = 0; col < getColumnNumber(); col++) {
			vector.set(get(row, col), 0, col);
		}
		return vector;
	}

	@Override
	public Matrix<T> columnVector(final Integer col) {
		final Matrix<T> vector = createNewMatrix(getRowNumber(), 1);
		for (int row = 0; row < getRowNumber(); row++) {
			vector.set(get(row, col), row, 0);
		}
		return vector;
	}

	@Override
	public Matrix<T> subMatrix(final Integer row, final Integer column) {
		final Matrix<T> subMatrix = createNewMatrix(getRowNumber() - 1, getColumnNumber() - 1);
		for (int r = 0; r < getRowNumber() - 1; r++) {
			for (int c = 0; c < getColumnNumber() - 1; c++) {
				subMatrix.set(get(r < row ? r : r + 1, c < column ? c : c + 1), r, c);
			}
		}
		return subMatrix;
	}

	@Override
	public Matrix<T> subMatrix(final int rowStart, final int rowEnd, final int columnStart, final int columnEnd) {
		final Matrix<T> submatrix = createNewMatrix(rowEnd - rowStart, columnEnd - columnStart);

		for (int r = rowStart; r < rowEnd; r++) {
			for (int c = columnStart; c < columnEnd; c++) {
				submatrix.set(get(r, c), r - rowStart, c - columnStart);
			}
		}
		return submatrix;
	}

	@Override
	public boolean isTheSameOf(final Matrix<T> matrix) {
		if (!hasSameSizeOf(matrix)) {
			return false;
		}
		for (int r = 0; r < getRowNumber(); r++) {
			for (int c = 0; c < getColumnNumber(); c++) {
				if (!get(r, c).equals(matrix.get(r, c))) {
					return false;
				}
			}
		}
		return true;
	}

	public int maxCharactersNumber() {
		int maxChars = 0;

		for (int r = 0; r < getRowNumber(); r++) {
			for (int c = 0; c < getColumnNumber(); c++) {
				maxChars = Integer.max(maxChars, get(r, c).toString().length());
			}
		}
		return maxChars;
	}
}
