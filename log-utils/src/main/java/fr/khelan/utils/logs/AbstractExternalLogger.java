package fr.khelan.utils.logs;

import org.slf4j.Logger;
import org.slf4j.Marker;

public class AbstractExternalLogger implements Logger {
	protected final Logger log;

	protected AbstractExternalLogger(Logger logger) {
		this.log = logger;
	}

	@Override
	public String getName() {
		return log.getName();
	}

	@Override
	public boolean isTraceEnabled() {
		return log.isTraceEnabled();
	}

	@Override
	public void trace(final String s) {
		log.trace(s);
	}

	@Override
	public void trace(final String s, final Object o) {
		log.trace(s, o);
	}

	@Override
	public void trace(final String s, final Object o, final Object o1) {
		log.trace(s, o, o1);
	}

	@Override
	public void trace(final String s, final Object... objects) {
		log.trace(s, objects);
	}

	@Override
	public void trace(final String s, final Throwable throwable) {
		log.trace(s, throwable);
	}

	@Override
	public boolean isTraceEnabled(final Marker marker) {
		return log.isTraceEnabled(marker);
	}

	@Override
	public void trace(final Marker marker, final String s) {
		log.trace(marker, s);
	}

	@Override
	public void trace(final Marker marker, final String s, final Object o) {
		log.trace(marker, s, o);
	}

	@Override
	public void trace(final Marker marker, final String s, final Object o, final Object o1) {
		log.trace(marker, s, o, o1);
	}

	@Override
	public void trace(final Marker marker, final String s, final Object... objects) {
		log.trace(marker, s, objects);
	}

	@Override
	public void trace(final Marker marker, final String s, final Throwable throwable) {
		log.trace(marker, s, throwable);
	}

	@Override
	public boolean isDebugEnabled() {
		return log.isDebugEnabled();
	}

	@Override
	public void debug(final String s) {
		log.debug(s);
	}

	@Override
	public void debug(final String s, final Object o) {
		log.debug(s, o);
	}

	@Override
	public void debug(final String s, final Object o, final Object o1) {
		log.debug(s, o, o1);
	}

	@Override
	public void debug(final String s, final Object... objects) {
		log.debug(s, objects);
	}

	@Override
	public void debug(final String s, final Throwable throwable) {
		log.debug(s, throwable);
	}

	@Override
	public boolean isDebugEnabled(final Marker marker) {
		return log.isDebugEnabled(marker);
	}

	@Override
	public void debug(final Marker marker, final String s) {
		log.debug(marker, s);
	}

	@Override
	public void debug(final Marker marker, final String s, final Object o) {
		log.debug(marker, s, o);
	}

	@Override
	public void debug(final Marker marker, final String s, final Object o, final Object o1) {
		log.debug(marker, s, o, o1);
	}

	@Override
	public void debug(final Marker marker, final String s, final Object... objects) {
		log.debug(marker, s, objects);
	}

	@Override
	public void debug(final Marker marker, final String s, final Throwable throwable) {
		log.debug(marker, s, throwable);
	}

	@Override
	public boolean isInfoEnabled() {
		return log.isInfoEnabled();
	}

	@Override
	public void info(final String s) {
		log.info(s);
	}

	@Override
	public void info(final String s, final Object o) {
		log.info(s, o);
	}

	@Override
	public void info(final String s, final Object o, final Object o1) {
		log.info(s, o, o1);
	}

	@Override
	public void info(final String s, final Object... objects) {
		log.info(s, objects);
	}

	@Override
	public void info(final String s, final Throwable throwable) {
		log.info(s, throwable);
	}

	@Override
	public boolean isInfoEnabled(final Marker marker) {
		return log.isInfoEnabled(marker);
	}

	@Override
	public void info(final Marker marker, final String s) {
		log.info(marker, s);
	}

	@Override
	public void info(final Marker marker, final String s, final Object o) {
		log.info(marker, s, o);
	}

	@Override
	public void info(final Marker marker, final String s, final Object o, final Object o1) {
		log.info(marker, s, o, o1);
	}

	@Override
	public void info(final Marker marker, final String s, final Object... objects) {
		log.info(marker, s, objects);
	}

	@Override
	public void info(final Marker marker, final String s, final Throwable throwable) {
		log.info(marker, s, throwable);
	}

	@Override
	public boolean isWarnEnabled() {
		return log.isWarnEnabled();
	}

	@Override
	public void warn(final String s) {
		log.warn(s);
	}

	@Override
	public void warn(final String s, final Object o) {
		log.warn(s, o);
	}

	@Override
	public void warn(final String s, final Object... objects) {
		log.warn(s, objects);
	}

	@Override
	public void warn(final String s, final Object o, final Object o1) {
		log.warn(s, o, o1);
	}

	@Override
	public void warn(final String s, final Throwable throwable) {
		log.warn(s, throwable);
	}

	@Override
	public boolean isWarnEnabled(final Marker marker) {
		return log.isWarnEnabled(marker);
	}

	@Override
	public void warn(final Marker marker, final String s) {
		log.warn(marker, s);
	}

	@Override
	public void warn(final Marker marker, final String s, final Object o) {
		log.warn(marker, s, o);
	}

	@Override
	public void warn(final Marker marker, final String s, final Object o, final Object o1) {
		log.warn(marker, s, o, o1);
	}

	@Override
	public void warn(final Marker marker, final String s, final Object... objects) {
		log.warn(marker, s, objects);
	}

	@Override
	public void warn(final Marker marker, final String s, final Throwable throwable) {
		log.warn(marker, s, throwable);
	}

	@Override
	public boolean isErrorEnabled() {
		return log.isErrorEnabled();
	}

	@Override
	public void error(final String s) {
		log.error(s);
	}

	@Override
	public void error(final String s, final Object o) {
		log.error(s, o);
	}

	@Override
	public void error(final String s, final Object o, final Object o1) {
		log.error(s, o, o1);
	}

	@Override
	public void error(final String s, final Object... objects) {
		log.error(s, objects);
	}

	@Override
	public void error(final String s, final Throwable throwable) {
		log.error(s, throwable);
	}

	@Override
	public boolean isErrorEnabled(final Marker marker) {
		return log.isErrorEnabled(marker);
	}

	@Override
	public void error(final Marker marker, final String s) {
		log.error(marker, s);
	}

	@Override
	public void error(final Marker marker, final String s, final Object o) {
		log.error(marker, s, o);
	}

	@Override
	public void error(final Marker marker, final String s, final Object o, final Object o1) {
		log.error(marker, s, o, o1);
	}

	@Override
	public void error(final Marker marker, final String s, final Object... objects) {
		log.error(marker, s, objects);
	}

	@Override
	public void error(final Marker marker, final String s, final Throwable throwable) {
		log.error(marker, s, throwable);
	}
}
