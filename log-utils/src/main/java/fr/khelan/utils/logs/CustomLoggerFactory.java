package fr.khelan.utils.logs;

public class CustomLoggerFactory {
	private CustomLoggerFactory() {
	}

	public static CustomLogger getLogger(String name) {
		return new CustomLogger(name);
	}

	public static CustomLogger getLogger(Class<?> c) {
		return new CustomLogger(c);
	}
}
