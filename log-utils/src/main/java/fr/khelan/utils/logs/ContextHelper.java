package fr.khelan.utils.logs;

import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.slf4j.MDC;

public class ContextHelper {
	protected static final String CORRELATION_ID = "correlationId";

	protected static final String CONTEXTE_FONCTIONNEL = "contexteFonctionnel";

	protected Map<String, String> getContext() {
		return MDC.getCopyOfContextMap();
	}

	protected void setContext(Map<String, String> context) {
		setCorrelationId();
		MDC.put(CONTEXTE_FONCTIONNEL, context.entrySet().stream().map(this::contextString)
											 .collect(Collectors.joining()));
	}

	private void setCorrelationId() {
		if (MDC.get(CORRELATION_ID) == null) {
			MDC.put(CORRELATION_ID, correlationId());
		}
	}

	public ContextHelper addToContext(String key, String value) {
		setCorrelationId();
		String contexteFonctionnel = MDC.get(CONTEXTE_FONCTIONNEL);
		if (contexteFonctionnel != null) {
			MDC.put(CONTEXTE_FONCTIONNEL, contexteFonctionnel + contextString(key, value));
		} else {
			MDC.put(CONTEXTE_FONCTIONNEL, contextString(key, value));
		}
		return this;
	}

	private String contextString(Map.Entry<String, String> entry) {
		return contextString(entry.getKey(), entry.getValue());
	}

	private String contextString(String key, String value) {
		return "[" + key + "=" + value + "]";
	}

	protected String getContextValue(String key) {
		return MDC.get(key);
	}

	protected void clearContext() {
		MDC.clear();
	}

	private String correlationId() {
		return UUID.randomUUID().toString();
	}
}
