package fr.khelan.utils.logs;

import java.util.Map;

import org.slf4j.LoggerFactory;

public class CustomLogger extends AbstractExternalLogger {
	private final ContextHelper contextHelper;

	protected CustomLogger(Class<?> c) {
		super(LoggerFactory.getLogger(c));
		this.contextHelper = new ContextHelper();
	}

	protected CustomLogger(String name) {
		super(LoggerFactory.getLogger(name));
		this.contextHelper = new ContextHelper();
	}

	public void setContext(Map<String, String> context) {
		contextHelper.setContext(context);
	}

	public ContextHelper addToContext(String key, String value) {
		contextHelper.addToContext(key, value);
		return contextHelper;
	}

	public void clearContext() {
		contextHelper.clearContext();
	}
}
