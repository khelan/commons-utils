package fr.khelan.utils.logs;

import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ContextHelperTest {
	private final ContextHelper contextHelper = new ContextHelper();

	@Test
	void contextHelperTest() {
		Map<String, String> context = new LinkedHashMap<>();
		context.put("cle1", "valeur1");
		context.put("cle2", "valeur2");
		contextHelper.setContext(context);
		contextHelper.addToContext("cle3", "valeur3");
		Assertions.assertEquals(2, contextHelper.getContext().size());
		Assertions.assertEquals("[cle1=valeur1][cle2=valeur2][cle3=valeur3]",
								contextHelper.getContextValue(ContextHelper.CONTEXTE_FONCTIONNEL));
		Assertions.assertNotNull(contextHelper.getContextValue(ContextHelper.CORRELATION_ID));

		contextHelper.clearContext();
		Assertions.assertEquals(0, contextHelper.getContext().size());

		contextHelper.addToContext("cleA", "valeurA")
					 .addToContext("cleB", "valeurB");
		Assertions.assertEquals(2, contextHelper.getContext().size());
		Assertions.assertEquals("[cleA=valeurA][cleB=valeurB]",
								contextHelper.getContextValue(ContextHelper.CONTEXTE_FONCTIONNEL));
	}
}
