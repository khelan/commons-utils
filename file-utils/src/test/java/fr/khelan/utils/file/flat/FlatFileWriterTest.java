package fr.khelan.utils.file.flat;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;

import fr.khelan.commons.exception.KhelanTechnicalException;
import fr.khelan.utils.file.flat.utils.FlatFileCodeDescription;
import fr.khelan.utils.file.flat.utils.FlatFilePosition;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class FlatFileWriterTest {
	private static final String DATE_FORMAT = "YYYY-MM-dd";
	private FlatFileWriter flatFileWriter;
	private OutputStream outputStream;

	@BeforeEach
	void init() {
		outputStream = new ByteArrayOutputStream();
		flatFileWriter = new FlatFileWriter(outputStream);
		flatFileWriter.setLineSeparator("\n");
	}

	@Test
	void writeString() throws KhelanTechnicalException, IOException {
		flatFileWriter = new FlatFileWriter(outputStream, StandardCharsets.US_ASCII);
		flatFileWriter.setLineSeparator("\n");
		flatFileWriter.initLine(FlatFilePosition.LONGUEUR);
		flatFileWriter.write("TEST1", FlatFilePosition.SECOND);
		flatFileWriter.endLine();
		flatFileWriter.flush();
		flatFileWriter.close();
		Assertions.assertEquals("    TEST1                          \n", outputStream.toString());
	}

	@Test
	void writeInteger() throws KhelanTechnicalException, IOException {
		flatFileWriter.initLine(FlatFilePosition.LONGUEUR);
		flatFileWriter.write(123456, FlatFilePosition.SECOND);
		flatFileWriter.flush();
		flatFileWriter.close();
		Assertions.assertEquals("    0000123456                     \n", outputStream.toString());
	}

	@Test
	void writeLong() throws KhelanTechnicalException, IOException {
		flatFileWriter.initLine(FlatFilePosition.LONGUEUR);
		flatFileWriter.write(123456L, FlatFilePosition.SECOND);
		flatFileWriter.flush();
		flatFileWriter.close();
		Assertions.assertEquals("    0000123456                     \n", outputStream.toString());
	}

	@Test
	void writeBigDecimal() throws KhelanTechnicalException, IOException {
		flatFileWriter.initLine(FlatFilePosition.LONGUEUR);
		flatFileWriter.write(new BigDecimal("654321.987654"), FlatFilePosition.SECOND);
		flatFileWriter.flush();
		flatFileWriter.close();
		Assertions.assertEquals("    0065432199                     \n", outputStream.toString());
	}

	@Test
	void writeBoolean() throws KhelanTechnicalException, IOException {
		flatFileWriter.initLine(FlatFilePosition.LONGUEUR);
		flatFileWriter.write(true, FlatFilePosition.TROISIEME);
		flatFileWriter.write(false, FlatFilePosition.QUATRIEME);
		flatFileWriter.flush();
		flatFileWriter.close();
		Assertions.assertEquals("              TF                   \n", outputStream.toString());
	}

	@Test
	void writeDate() throws KhelanTechnicalException, IOException {
		final Calendar date = Calendar.getInstance();
		date.set(Calendar.YEAR, 2016);
		date.set(Calendar.MONTH, 9);
		date.set(Calendar.DAY_OF_MONTH, 14);
		flatFileWriter.initLine(FlatFilePosition.LONGUEUR);
		flatFileWriter.write(date.getTime(), DATE_FORMAT, FlatFilePosition.SECOND);
		flatFileWriter.flush();
		flatFileWriter.close();
		Assertions.assertEquals("    2016-10-14                     \n", outputStream.toString());
	}

	@Test
	void writeCodeDescription() throws KhelanTechnicalException, IOException {
		flatFileWriter.initLine(FlatFilePosition.LONGUEUR);
		flatFileWriter.writeCode(FlatFileCodeDescription.TEST, FlatFilePosition.CINQUIEME);
		flatFileWriter.writeDescription(FlatFileCodeDescription.TEST, FlatFilePosition.SECOND);
		flatFileWriter.endLine();
		flatFileWriter.flush();
		flatFileWriter.close();
		Assertions.assertEquals("    DescTest    TEST               \n", outputStream.toString());
	}

	@Test
	void writeNull() throws KhelanTechnicalException, IOException {
		flatFileWriter.initLine(FlatFilePosition.LONGUEUR);
		flatFileWriter.setLineSeparator(StringUtils.EMPTY);
		flatFileWriter.write((BigDecimal) null, FlatFilePosition.PREMIER);
		flatFileWriter.write((Boolean) null, FlatFilePosition.SECOND);
		flatFileWriter.write((Integer) null, FlatFilePosition.TROISIEME);
		flatFileWriter.write((Long) null, FlatFilePosition.QUATRIEME);
		flatFileWriter.write((String) null, FlatFilePosition.CINQUIEME);
		flatFileWriter.write(null, DATE_FORMAT, FlatFilePosition.SIXIEME);
		flatFileWriter.writeCode(null, FlatFilePosition.SEPTIEME);
		flatFileWriter.writeDescription(null, FlatFilePosition.HUITIEME);
		flatFileWriter.endLine();
		flatFileWriter.flush();
		flatFileWriter.close();
		Assertions.assertEquals("    F                              ", outputStream.toString());
	}

	@Test
	void initLineTestNegativeLength() throws KhelanTechnicalException {
		flatFileWriter.initLine(-1);
		Assertions.assertThrows(KhelanTechnicalException.class, () -> {
			flatFileWriter.write("TEST", FlatFilePosition.PREMIER);
		});
	}

	@Test
	void initLineTestKo() throws KhelanTechnicalException {
		Assertions.assertThrows(KhelanTechnicalException.class, () -> {
			flatFileWriter.write("TEST", FlatFilePosition.PREMIER);
		});
	}

	@Test
	void writeTestAfterEndOfLine() throws KhelanTechnicalException {
		flatFileWriter.initLine(FlatFilePosition.LONGUEUR);
		Assertions.assertThrows(KhelanTechnicalException.class, () -> {
			flatFileWriter.write("123", FlatFilePosition.OVERFLOW);
		});
	}

	@Test
	void writeTestDataTooBig() throws KhelanTechnicalException {
		flatFileWriter.initLine(FlatFilePosition.LONGUEUR);
		Assertions.assertThrows(KhelanTechnicalException.class, () -> {
			flatFileWriter.write("12345", FlatFilePosition.PREMIER);
		});
	}

	@Test
	void GetterSettetTest() {
		flatFileWriter.setNumberOfDecimalsAmount(3);
		Assertions.assertEquals(3, flatFileWriter.getNumberOfDecimalsAmount());

		flatFileWriter.setBooleanFalse("0");
		Assertions.assertEquals("0", flatFileWriter.getBooleanFalse());

		flatFileWriter.setBooleanTrue("1");
		Assertions.assertEquals("1", flatFileWriter.getBooleanTrue());

		flatFileWriter.setDefaultFillerCharacter('_');
		Assertions.assertEquals('_', flatFileWriter.getDefaultFillerCharacter());

		flatFileWriter.setNumberFillerCharacter('*');
		Assertions.assertEquals('*', flatFileWriter.getNumberFillerCharacter());

		flatFileWriter.setLineSeparator("\r\n");
		Assertions.assertEquals("\r\n", flatFileWriter.getLineSeparator());
	}
}
