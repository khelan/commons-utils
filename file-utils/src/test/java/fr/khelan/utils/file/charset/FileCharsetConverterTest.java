package fr.khelan.utils.file.charset;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import fr.khelan.commons.exception.KhelanTechnicalException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class FileCharsetConverterTest {
	private static final String RACINE = "src/test/resources/";
	private static final String IN = RACINE + "charset/in/";
	private static final String OUT = RACINE + "charset/out/";

	@Test
	void asciiToUtf8Test() {
		final FileCharsetConverter converter = new FileCharsetConverter(Charset.forName("ISO-8859-15"),
																		StandardCharsets.UTF_8);
		Assertions.assertDoesNotThrow(() -> {
			try (final FileInputStream input = new FileInputStream(IN + "LesFleursDuMal_LeSoleil_ASCII.txt");
				 final FileOutputStream output = new FileOutputStream(OUT + "LesFleursDuMal_LeSoleil_UTF-8.txt")) {
				converter.convert(input, output);
			}
		});
	}

	@Test
	void utf8ToAsciiTestReportOnUnmappableChar() throws KhelanTechnicalException, IOException {
		Assertions.assertThrows(KhelanTechnicalException.class, () -> {
			final FileCharsetConverter converter = new FileCharsetConverter(StandardCharsets.UTF_8,
																			Charset.forName("ISO-8859-15"));
			try (final FileInputStream input = new FileInputStream(IN + "UnmappableChars_UTF-8.txt");
				 final FileOutputStream output = new FileOutputStream(OUT + "UnmappableChars_ASCII_Report.txt")) {
				converter.convert(input, output);
			} catch (final KhelanTechnicalException e) {
				System.out.println("Exception : " + e.getErrorCode().code() + " ; " + e.getErrorCode().description());
				throw e;
			}
		});
	}

	@Test
	void utf8ToAsciiTestReplaceOnUnmappableChar() {
		final FileCharsetConverter converter = new FileCharsetConverter(StandardCharsets.UTF_8,
																		Charset.forName("ISO-8859-15"), "_");
		Assertions.assertDoesNotThrow(() -> {
			try (final FileInputStream input = new FileInputStream(IN + "UnmappableChars_UTF-8.txt");
				 final FileOutputStream output = new FileOutputStream(OUT + "UnmappableChars_ASCII_Replace.txt")) {
				converter.convert(input, output);
			}
		});
	}
}
