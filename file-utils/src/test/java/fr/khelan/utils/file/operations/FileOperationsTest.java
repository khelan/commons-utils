package fr.khelan.utils.file.operations;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;

import fr.khelan.commons.exception.KhelanException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class FileOperationsTest {
	private static final Path RACINE = Path.of("target/operations/");
	private static final Path FILE_TO_COPY = RACINE.resolve("file_to_copy.txt");
	private static final Path FILE_TO_MOVE = RACINE.resolve("file_to_move.txt");
	private static final Path FILE_TO_RENAME = RACINE.resolve("file_to_rename.txt");

	private static final String LOREM_IPSUM_CICERON =
			"Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit, amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt, ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur?\n"
			+
			"At vero eos et accusamus et iusto odio dignissimos ducimus, qui blanditiis praesentium voluptatum deleniti atque corrupti, quos dolores et quas molestias excepturi sint, obcaecati cupiditate non provident, similique sunt in culpa, qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio, cumque nihil impedit, quo minus id, quod maxime placeat, facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet, ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.";

	private static final String LOREM_IPSUM_FR =
			"Pour vous faire mieux connaitre d’où vient l’erreur de ceux qui blâment la volupté, et qui louent en quelque sorte la douleur, je vais entrer dans une explication plus étendue, et vous faire voir tout ce qui a été dit là-dessus par l’inventeur de la vérité, et, pour ainsi dire, par l’architecte de la vie heureuse.\n"
			+
			"Personne [dit Épicure] ne craint ni ne fuit la volupté en tant que volupté, mais en tant qu’elle attire de grandes douleurs à ceux qui ne savent pas en faire un usage modéré et raisonnable ; et personne n’aime ni ne recherche la douleur comme douleur, mais parce qu’il arrive quelquefois que, par le travail et par la peine, on parvienne à jouir d’une grande volupté. En effet, pour descendre jusqu’aux petites choses, qui de vous ne fait point quelque exercice pénible pour en retirer quelque sorte d’utilité ? Et qui pourrait justement blâmer, ou celui qui rechercherait une volupté qui ne pourrait être suivie de rien de fâcheux, ou celui qui éviterait une douleur dont il ne pourrait espérer aucun plaisir.\n"
			+
			"Au contraire, nous blâmons avec raison et nous croyons dignes de mépris et de haine ceux qui, se laissant corrompre par les attraits d’une volupté présente, ne prévoient pas à combien de maux et de chagrins une passion aveugle les peut exposer.\n"
			+
			"J’en dis autant de ceux qui, par mollesse d’esprit, c’est-à-dire par la crainte de la peine et de la douleur, manquent aux devoirs de la vie. Et il est très facile de rendre raison de ce que j’avance. Car, lorsque nous sommes tout à fait libres, et que rien ne nous empêche de faire ce qui peut nous donner le plus de plaisir, nous pouvons nous livrer entièrement à la volupté et chasser toute sorte de douleur ; mais, dans les temps destinés aux devoirs de la société ou à la nécessité des affaires, souvent il faut faire divorce avec la volupté, et ne se point refuser à la peine.\n"
			+
			"La règle que suit en cela un homme sage, c’est de renoncer à de légères voluptés pour en avoir de plus grandes, et de savoir supporter des douleurs légères pour en éviter de plus fâcheuses.";

	private static final String LOREM_IPSUM_POPULAR =
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue. Ut in risus volutpat libero pharetra tempor. Cras vestibulum bibendum augue. Praesent egestas leo in pede. Praesent blandit odio eu enim. Pellentesque sed dui ut augue blandit sodales. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam nibh. Mauris ac mauris sed pede pellentesque fermentum. Maecenas adipiscing ante non diam sodales hendrerit.\n"
			+ "\n"
			+
			"Ut velit mauris, egestas sed, gravida nec, ornare ut, mi. Aenean ut orci vel massa suscipit pulvinar. Nulla sollicitudin. Fusce varius, ligula non tempus aliquam, nunc turpis ullamcorper nibh, in tempus sapien eros vitae ligula. Pellentesque rhoncus nunc et augue. Integer id felis. Curabitur aliquet pellentesque diam. Integer quis metus vitae elit lobortis egestas. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi vel erat non mauris convallis vehicula. Nulla et sapien. Integer tortor tellus, aliquam faucibus, convallis id, congue eu, quam. Mauris ullamcorper felis vitae erat. Proin feugiat, augue non elementum posuere, metus purus iaculis lectus, et tristique ligula justo vitae magna.\n"
			+
			"Aliquam convallis sollicitudin purus. Praesent aliquam, enim at fermentum mollis, ligula massa adipiscing nisl, ac euismod nibh nisl eu lectus. Fusce vulputate sem at sapien. Vivamus leo. Aliquam euismod libero eu enim. Nulla nec felis sed leo placerat imperdiet. Aenean suscipit nulla in justo. Suspendisse cursus rutrum augue. Nulla tincidunt tincidunt mi. Curabitur iaculis, lorem vel rhoncus faucibus, felis magna fermentum augue, et ultricies lacus lorem varius purus. Curabitur eu amet.";

	private final FileOperations fileOperations = new FileOperations();

	@BeforeAll
	static void initFiles() throws IOException {
		Files.createDirectories(RACINE);
		Files.find(RACINE, 3, (p, o) -> o.isRegularFile()).forEach(p -> {
			try {
				Files.delete(p);
			} catch (final IOException e) {
				e.printStackTrace();
			}
		});
		try (final Writer writer = new PrintWriter(FILE_TO_COPY.toFile())) {
			writer.append(LOREM_IPSUM_CICERON);
		}

		try (final Writer writer = new PrintWriter(FILE_TO_MOVE.toFile())) {
			writer.append(LOREM_IPSUM_FR);
		}

		try (final Writer writer = new PrintWriter(FILE_TO_RENAME.toFile())) {
			writer.append(LOREM_IPSUM_POPULAR);
		}
	}

	@Test
	void copyFileTest() throws KhelanException {
		final Path fileToCopy = RACINE.resolve("file_to_copy.txt");
		final Path newFolder = RACINE.resolve("copied/");
		fileOperations.copy(fileToCopy, newFolder);

		Assertions.assertTrue(Files.exists(newFolder.resolve("file_to_copy.txt")));
		Assertions.assertTrue(Files.isRegularFile(newFolder.resolve("file_to_copy.txt")));
		Assertions.assertTrue(Files.exists(fileToCopy));
	}

	@Test
	void moveFileTest() throws KhelanException {
		final Path fileToMove = RACINE.resolve("file_to_move.txt");
		final Path newFolder = RACINE.resolve("moved/");
		fileOperations.move(fileToMove, newFolder);

		Assertions.assertTrue(Files.exists(newFolder.resolve("file_to_move.txt")));
		Assertions.assertTrue(Files.isRegularFile(newFolder.resolve("file_to_move.txt")));
		Assertions.assertFalse(Files.exists(fileToMove));
	}

	@Test
	void renameFileTest() throws KhelanException {
		final Path fileToRename = RACINE.resolve("file_to_rename.txt");
		final String newFilename = "file_renamed.txt";
		fileOperations.rename(fileToRename, newFilename);

		Assertions.assertTrue(Files.exists(RACINE.resolve(newFilename)));
		Assertions.assertTrue(Files.isRegularFile(RACINE.resolve(newFilename)));
		Assertions.assertFalse(Files.exists(fileToRename));
	}
}
