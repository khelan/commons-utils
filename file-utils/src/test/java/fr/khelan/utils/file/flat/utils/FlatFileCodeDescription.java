package fr.khelan.utils.file.flat.utils;

import fr.khelan.commons.label.CodeDescription;

public enum FlatFileCodeDescription implements CodeDescription {
	TEST("TEST", "DescTest");

	private final String code;
	private final String description;

	FlatFileCodeDescription(final String code, final String description) {
		this.code = code;
		this.description = description;
	}

	/**
	 * @return the code
	 */
	@Override
	public String code() {
		return code;
	}

	/**
	 * @return the description
	 */
	@Override
	public String description() {
		return description;
	}
}
