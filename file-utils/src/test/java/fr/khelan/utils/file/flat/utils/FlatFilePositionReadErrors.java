package fr.khelan.utils.file.flat.utils;

import fr.khelan.utils.file.flat.field.FlatFileField;

public enum FlatFilePositionReadErrors implements FlatFileField {
	OVERFLOW(34, 4), INCORRECT_START(-1, 1), INCORRECT_LENGTH(1, 0);

	public static final int LONGUEUR = 36;

	private int starts;
	private int length;

	private FlatFilePositionReadErrors(final int starts, final int length) {
		this.starts = starts;
		this.length = length;
	}

	/**
	 * @return the starts
	 */
	@Override
	public int starts() {
		return starts;
	}

	/**
	 * @return the length
	 */
	@Override
	public int length() {
		return length;
	}
}
