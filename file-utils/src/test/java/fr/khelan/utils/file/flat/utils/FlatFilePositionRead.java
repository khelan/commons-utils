package fr.khelan.utils.file.flat.utils;

import java.util.HashMap;
import java.util.Map;

import fr.khelan.utils.file.flat.field.FlatFileField;

public enum FlatFilePositionRead implements FlatFileField {
	P1_1_4(1, 4), P2_5_10(5, 10), P3_15_1(15, 1), P4_16_5(16, 5), P5_21_6(21, 6), P6_27_10(27, 10);

	public static final int LONGUEUR = 36;

	private int starts;
	private int length;

	private FlatFilePositionRead(final int starts, final int length) {
		this.starts = starts;
		this.length = length;
	}

	public static Map<String, FlatFilePositionRead> mapValues() {
		final Map<String, FlatFilePositionRead> map = new HashMap<>();
		for (final FlatFilePositionRead field : values()) {
			map.put(field.name(), field);
		}
		return map;
	}

	/**
	 * @return the starts
	 */
	@Override
	public int starts() {
		return starts;
	}

	/**
	 * @return the length
	 */
	@Override
	public int length() {
		return length;
	}
}
