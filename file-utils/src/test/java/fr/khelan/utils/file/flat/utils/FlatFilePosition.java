package fr.khelan.utils.file.flat.utils;

import fr.khelan.utils.file.flat.field.FlatFileField;

public enum FlatFilePosition implements FlatFileField {
	PREMIER(1, 4),
	SECOND(5, 10),
	TROISIEME(15, 1),
	QUATRIEME(16, 1),
	CINQUIEME(17, 4),
	SIXIEME(21, 4),
	SEPTIEME(25,
			 5),
	HUITIEME(30, 6),
	OVERFLOW(33, 4);

	public static final int LONGUEUR = 35;

	private final int starts;
	private final int length;

	FlatFilePosition(final int starts, final int length) {
		this.starts = starts;
		this.length = length;
	}

	/**
	 * @return the starts
	 */
	@Override
	public int starts() {
		return starts;
	}

	/**
	 * @return the length
	 */
	@Override
	public int length() {
		return length;
	}
}
