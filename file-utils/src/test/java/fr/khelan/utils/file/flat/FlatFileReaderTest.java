package fr.khelan.utils.file.flat;

import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

import fr.khelan.commons.exception.KhelanException;
import fr.khelan.commons.exception.KhelanTechnicalException;
import fr.khelan.utils.file.flat.utils.FlatFilePositionRead;
import fr.khelan.utils.file.flat.utils.FlatFilePositionReadErrors;
import org.apache.commons.io.input.ReaderInputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class FlatFileReaderTest {
	private static final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	private FlatFileReader flatFileReader;

	@AfterEach
	void close() throws IOException {
		if (flatFileReader != null) {
			flatFileReader.close();
			flatFileReader = null;
		}
	}

	@Test
	void readLineTestNotRead() throws KhelanException {
		flatFileReader = new FlatFileReader(new StringReader(ALPHABET));
		Assertions.assertThrows(KhelanTechnicalException.class, () -> {
			flatFileReader.readField(FlatFilePositionReadErrors.OVERFLOW);
		});
	}

	@Test
	void readLineTestGetField() throws KhelanException {
		flatFileReader = new FlatFileReader(new StringReader(ALPHABET));
		Assertions.assertTrue(flatFileReader.readNextLine());

		Assertions.assertEquals("ABCD", flatFileReader.readField(FlatFilePositionRead.P1_1_4));
		Assertions.assertEquals("EFGHIJKLMN", flatFileReader.readField(FlatFilePositionRead.P2_5_10));
		Assertions.assertEquals("O", flatFileReader.readField(FlatFilePositionRead.P3_15_1));
		Assertions.assertEquals("PQRST", flatFileReader.readField(FlatFilePositionRead.P4_16_5));
		Assertions.assertEquals("UVWXYZ", flatFileReader.readField(FlatFilePositionRead.P5_21_6));
		Assertions.assertEquals("0123456789", flatFileReader.readField(FlatFilePositionRead.P6_27_10));
	}

	@Test
	void readLineTestGetFieldsList() throws KhelanException {
		flatFileReader = new FlatFileReader(new StringReader(ALPHABET));
		Assertions.assertTrue(flatFileReader.readNextLine());

		final List<String> valuesList = flatFileReader.readFields(FlatFilePositionRead.values());
		Assertions.assertEquals("ABCD", valuesList.get(0));
		Assertions.assertEquals("EFGHIJKLMN", valuesList.get(1));
		Assertions.assertEquals("O", valuesList.get(2));
		Assertions.assertEquals("PQRST", valuesList.get(3));
		Assertions.assertEquals("UVWXYZ", valuesList.get(4));
		Assertions.assertEquals("0123456789", valuesList.get(5));
	}

	@Test
	void readLineTestGetFieldsMap() throws KhelanException {
		flatFileReader = new FlatFileReader(new StringReader(ALPHABET));
		Assertions.assertTrue(flatFileReader.readNextLine());

		final Map<String, String> valuesList = flatFileReader.readFields(FlatFilePositionRead.mapValues());
		Assertions.assertEquals("ABCD", valuesList.get(FlatFilePositionRead.P1_1_4.name()));
		Assertions.assertEquals("EFGHIJKLMN", valuesList.get(FlatFilePositionRead.P2_5_10.name()));
		Assertions.assertEquals("O", valuesList.get(FlatFilePositionRead.P3_15_1.name()));
		Assertions.assertEquals("PQRST", valuesList.get(FlatFilePositionRead.P4_16_5.name()));
		Assertions.assertEquals("UVWXYZ", valuesList.get(FlatFilePositionRead.P5_21_6.name()));
		Assertions.assertEquals("0123456789", valuesList.get(FlatFilePositionRead.P6_27_10.name()));
	}

	@Test
	void readLineTestGetNullField() throws KhelanException {
		flatFileReader = new FlatFileReader(new StringReader(ALPHABET));
		Assertions.assertTrue(flatFileReader.readNextLine());
		Assertions.assertThrows(KhelanTechnicalException.class, () -> {
			flatFileReader.readField(null);
		});
	}

	@Test
	void readLineTestGetIncorrectFieldStart() throws KhelanException {
		flatFileReader = new FlatFileReader(new StringReader(ALPHABET));
		Assertions.assertTrue(flatFileReader.readNextLine());
		Assertions.assertThrows(KhelanTechnicalException.class, () -> {
			flatFileReader.readField(FlatFilePositionReadErrors.INCORRECT_START);
		});
	}

	@Test
	void readLineTestGetIncorrectFieldLength() throws KhelanException {
		flatFileReader = new FlatFileReader(new StringReader(ALPHABET));
		Assertions.assertTrue(flatFileReader.readNextLine());
		Assertions.assertThrows(KhelanTechnicalException.class, () -> {
			flatFileReader.readField(FlatFilePositionReadErrors.INCORRECT_LENGTH);
		});
	}

	@Test
	void readLineTestOverflow() throws KhelanException {
		flatFileReader = new FlatFileReader(new StringReader(ALPHABET));
		Assertions.assertTrue(flatFileReader.readNextLine());
		Assertions.assertThrows(KhelanTechnicalException.class, () -> {
			flatFileReader.readField(FlatFilePositionReadErrors.OVERFLOW);
		});
	}

	@Test
	void readLineTestOneLine() throws KhelanException {
		flatFileReader = new FlatFileReader(new StringReader(ALPHABET));
		Assertions.assertTrue(flatFileReader.readNextLine());
		Assertions.assertFalse(flatFileReader.readNextLine());
	}

	@Test
	void readLineTestEmptyLine() throws KhelanException {
		flatFileReader = new FlatFileReader(new StringReader(ALPHABET + "\n\n" + ALPHABET));
		// Reads first not empty line.
		Assertions.assertTrue(flatFileReader.readNextLine());
		// Reads second empty line.
		Assertions.assertTrue(flatFileReader.readNextLine());
		Assertions.assertTrue(flatFileReader.isEmptyLine());
		// Reads third not empty line.
		Assertions.assertTrue(flatFileReader.readNextLine());
		Assertions.assertFalse(flatFileReader.isEmptyLine());
	}

	@Test
	void readLineTestInputStreamWithoutCharset() throws KhelanException {
		flatFileReader = new FlatFileReader(
				new ReaderInputStream(new StringReader(ALPHABET + "\r\n\r\n" + ALPHABET), StandardCharsets.UTF_8));
		// Reads first not empty line.
		Assertions.assertTrue(flatFileReader.readNextLine());
		// Reads second empty line.
		Assertions.assertTrue(flatFileReader.readNextLine());
		Assertions.assertTrue(flatFileReader.isEmptyLine());
		// Reads third not empty line.
		Assertions.assertTrue(flatFileReader.readNextLine());
		Assertions.assertFalse(flatFileReader.isEmptyLine());
	}

	@Test
	void readLineTestInputStreamWithCharset() throws KhelanException {
		flatFileReader = new FlatFileReader(new StringReader(ALPHABET + "\n\n" + ALPHABET));
		flatFileReader.setLineSeparator('\n');
		// Reads first not empty line.
		Assertions.assertTrue(flatFileReader.readNextLine());
		// Reads second empty line.
		Assertions.assertTrue(flatFileReader.readNextLine());
		Assertions.assertTrue(flatFileReader.isEmptyLine());
		// Reads third not empty line.
		Assertions.assertTrue(flatFileReader.readNextLine());
		Assertions.assertFalse(flatFileReader.isEmptyLine());

		Assertions.assertFalse(flatFileReader.readNextLine());
		Assertions.assertFalse(flatFileReader.readNextLine());
	}

	@Test
	void readLineTestLineSeparator() throws KhelanException {
		flatFileReader = new FlatFileReader(
				new ReaderInputStream(new StringReader(ALPHABET + "\r\n\r\n" + ALPHABET), StandardCharsets.ISO_8859_1),
				StandardCharsets.UTF_8);
		// Reads first not empty line.
		Assertions.assertTrue(flatFileReader.readNextLine());
		// Reads second empty line.
		Assertions.assertTrue(flatFileReader.readNextLine());
		Assertions.assertTrue(flatFileReader.isEmptyLine());
		// Reads third not empty line.
		Assertions.assertTrue(flatFileReader.readNextLine());
		Assertions.assertFalse(flatFileReader.isEmptyLine());

		Assertions.assertFalse(flatFileReader.readNextLine());
		Assertions.assertFalse(flatFileReader.readNextLine());
	}

	@Test
	void readLineTestGettersSetters() {
		flatFileReader = new FlatFileReader(new StringReader(ALPHABET));

		flatFileReader.setDefaultLineLength(1000);
		Assertions.assertEquals(1000, flatFileReader.getDefaultLineLength());

		flatFileReader.setLineSeparator(' ');
		Assertions.assertEquals(' ', flatFileReader.getLineSeparator());
	}
}
