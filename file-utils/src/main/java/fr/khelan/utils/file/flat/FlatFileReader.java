package fr.khelan.utils.file.flat;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import fr.khelan.commons.exception.KhelanTechnicalException;
import fr.khelan.utils.file.exception.FileTechnicalErrorCode;
import fr.khelan.utils.file.flat.exception.FlatFileErrorCode;
import fr.khelan.utils.file.flat.field.FlatFileField;

/**
 * Class for reading flat files, in which each field is fixed-sized on the line.
 */
public class FlatFileReader implements Closeable {
	/** The reader. */
	private final BufferedReader reader;

	/** New line separator. */
	private Character lineSeparator;
	/** Default line length. */
	private int defaultLineLength = 1024;
	/** The line buffer. */
	private char[] buffer = null;

	/**
	 * Initializes a {@link FlatFileReader}.
	 *
	 * @param reader
	 * 		The input reader.
	 */
	public FlatFileReader(final Reader reader) {
		this.reader = new BufferedReader(reader);
	}

	/**
	 * Initializes a {@link FlatFileReader}.
	 *
	 * @param stream
	 * 		The input stream.
	 */
	public FlatFileReader(final InputStream stream) {
		this(new InputStreamReader(stream));
	}

	/**
	 * Initializes a {@link FlatFileReader}.
	 *
	 * @param stream
	 * 		The input stream.
	 * @param charset
	 * 		The charset.
	 */
	public FlatFileReader(final InputStream stream, final Charset charset) {
		this(new InputStreamReader(stream, charset));
	}

	/**
	 * Reads a single character.
	 *
	 * @return The character read, as an integer in the range 0 to 65535, or -1 if the end of the stream has been
	 * reached
	 *
	 * @throws KhelanTechnicalException
	 */
	private int read() throws KhelanTechnicalException {
		int read = -1;
		try {
			read = reader.read();
		} catch (final IOException e) {
			throw new KhelanTechnicalException(FileTechnicalErrorCode.IO, e);
		}
		return read;
	}

	/**
	 * Reads the next line, to let access to the fields of this line.
	 *
	 * @return True if the new line is read, false if the end of the stream has been reached.
	 *
	 * @throws KhelanTechnicalException
	 * 		If the reading operation fails.
	 */
	public boolean readNextLine() throws KhelanTechnicalException {
		buffer = null;
		if (lineSeparator == null) {
			try {
				final String line = reader.readLine();
				if (line == null) {
					return false;
				}
				buffer = line.toCharArray();
			} catch (final IOException e) {
				throw new KhelanTechnicalException(FileTechnicalErrorCode.IO, e);
			}
		} else {
			final StringBuilder stringBuilder = new StringBuilder(defaultLineLength);
			int read = read();
			while (read >= 0 && read != lineSeparator) {
				final char character = (char) read;
				stringBuilder.append(character);
				read = read();
			}
			if (stringBuilder.length() == 0 && read < 0) {
				return false;
			}
			buffer = new char[stringBuilder.length()];
			stringBuilder.getChars(0, stringBuilder.length(), buffer, 0);
		}
		return true;
	}

	/**
	 * Indicates if the current line is empty.
	 *
	 * @return True if the current line is empty. False otherwise.
	 */
	public boolean isEmptyLine() {
		return buffer.length == 0;
	}

	/**
	 * Reads the value of the field.
	 *
	 * @param field
	 * 		The field.
	 *
	 * @return The string value of the field.
	 *
	 * @throws KhelanTechnicalException
	 * 		If it fails.
	 */
	public String readField(final FlatFileField field) throws KhelanTechnicalException {
		String fieldValue;
		if (buffer == null) {
			throw new KhelanTechnicalException(FlatFileErrorCode.LINE_READ);
		}
		if (field == null || field.starts() <= 0 || field.length() < 1
			|| field.starts() + field.length() - 1 > buffer.length) {
			throw new KhelanTechnicalException(FlatFileErrorCode.FIELD_SIZE);
		}
		try {
			fieldValue = new String(buffer, field.starts() - 1, field.length());
		} catch (final IndexOutOfBoundsException e) {
			// Should not happen because of previous controls
			throw new KhelanTechnicalException(FlatFileErrorCode.FIELD_SIZE, e);
		}
		return fieldValue;
	}

	/**
	 * Reads the values of the listed fields.
	 *
	 * @param fields
	 * 		The list of fields.
	 *
	 * @return The list string values of the fields.
	 *
	 * @throws KhelanTechnicalException
	 * 		If it fails.
	 */
	public List<String> readFields(final FlatFileField... fields) throws KhelanTechnicalException {
		return readFields(Arrays.asList(fields));
	}

	/**
	 * Reads the values of the listed fields.
	 *
	 * @param fields
	 * 		The list of fields.
	 *
	 * @return The list string values of the fields.
	 *
	 * @throws KhelanTechnicalException
	 * 		If it fails.
	 */
	public List<String> readFields(final List<? extends FlatFileField> fields) throws KhelanTechnicalException {
		final List<String> fieldValues = new ArrayList<>();
		for (final FlatFileField field : fields) {
			fieldValues.add(readField(field));
		}
		return fieldValues;
	}

	/**
	 * Reads the values of the fields.
	 *
	 * @param fields
	 * 		The {@link Map} of the fields: the key is the fieldname, the value is the field definition.
	 *
	 * @return The {@link Map} of the values: the key is the fieldname, the value is the string value of the field.
	 *
	 * @throws KhelanTechnicalException
	 * 		If it fails.
	 */
	public Map<String, String> readFields(final Map<String, ? extends FlatFileField> fields)
			throws KhelanTechnicalException {
		final Map<String, String> fieldValues = new HashMap<>();
		for (final Entry<String, ? extends FlatFileField> field : fields.entrySet()) {
			fieldValues.put(field.getKey(), readField(field.getValue()));
		}
		return fieldValues;
	}

	@Override
	public void close() throws IOException {
		reader.close();
	}

	/**
	 * @return the lineSeparator
	 */
	public char getLineSeparator() {
		return lineSeparator;
	}

	/**
	 * @param lineSeparator
	 * 		the lineSeparator to set
	 */
	public void setLineSeparator(final char lineSeparator) {
		this.lineSeparator = lineSeparator;
	}

	/**
	 * @return the defaultLineLength
	 */
	public int getDefaultLineLength() {
		return defaultLineLength;
	}

	/**
	 * @param defaultLineLength
	 * 		the defaultLineLength to set
	 */
	public void setDefaultLineLength(final int defaultLineLength) {
		this.defaultLineLength = defaultLineLength;
	}
}
