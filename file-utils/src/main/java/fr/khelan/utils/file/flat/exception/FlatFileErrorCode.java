package fr.khelan.utils.file.flat.exception;

import fr.khelan.commons.label.CodeDescription;

/**
 * Error codes for the FlatFileWriter.
 */
public enum FlatFileErrorCode implements CodeDescription {
	/** Line initialization. */
	LINE_INIT("FILE-FLAT-001", "The line have to be initialized before writing data."),
	/** Line initialization. */
	LINE_END("FILE-FLAT-002", "The line have to be initialized before writing data."),
	/** Field size out of line bounds. */
	FIELD_SIZE("FILE-FLAT-011", "The field is out of the line bounds."),
	/** Data size out of field bounds. */
	FIELD_DATA_SIZE("FILE-FLAT-012", "The data size is out of the field bounds."),
	/** Line not read. */
	LINE_READ("FILE-FLAT-021", "The line have to be read before reading field.");

	private final String code;

	private final String description;

	private FlatFileErrorCode(final String code, final String description) {
		this.code = code;
		this.description = description;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String code() {
		return code;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String description() {
		return description;
	}

	@Override
	public String toString() {
		return code + ": " + description;
	}
}
