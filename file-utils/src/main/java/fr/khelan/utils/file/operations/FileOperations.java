package fr.khelan.utils.file.operations;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import fr.khelan.commons.exception.KhelanException;
import fr.khelan.commons.exception.KhelanFunctionalException;
import fr.khelan.commons.exception.KhelanTechnicalException;
import fr.khelan.utils.file.exception.FileTechnicalErrorCode;
import fr.khelan.utils.file.operations.exception.FileOperationsErrorCode;

/**
 * Utility to manipulate files (copy, move, rename...).
 */
public class FileOperations {
	/**
	 * Renames the file.
	 *
	 * @param file        The file to rename.
	 * @param newFilename The new file name.
	 *
	 * @throws KhelanException If an I/O Exception occurred.
	 */
	public void rename(final Path file, final String newFilename) throws KhelanException {
		checkFileExists(file);
		final Path newFile = file.getParent().resolve(newFilename);
		checkFileNotExists(newFile);
		try {
			Files.move(file, newFile);
		} catch (final IOException e) {
			throw new KhelanTechnicalException(FileTechnicalErrorCode.IO, e);
		}
	}

	/**
	 * Copy the file to a new folder.
	 *
	 * @param file      The file to copy.
	 * @param newFolder The new folder to copy file.
	 *
	 * @throws KhelanException If an I/O Exception occurred.
	 */
	public void copy(final Path file, final Path newFolder) throws KhelanException {
		checkFileExists(file);
		checkDirectoryOrCreate(newFolder);
		final Path newFile = newFolder.resolve(file.getFileName());
		checkFileNotExists(newFile);
		if (Files.exists(newFile)) {
			throw new KhelanFunctionalException(FileOperationsErrorCode.ALREADY_EXISTS);
		}
		try {
			Files.copy(file, newFile);
		} catch (final IOException e) {
			throw new KhelanTechnicalException(FileTechnicalErrorCode.IO, e);
		}
	}

	/**
	 * Moves the file to a new folder.
	 *
	 * @param file      The file to move.
	 * @param newFolder The new folder to move file.
	 *
	 * @throws KhelanException If an I/O Exception occurred.
	 */
	public void move(final Path file, final Path newFolder) throws KhelanException {
		checkFileExists(file);
		checkDirectoryOrCreate(newFolder);
		final Path newFile = newFolder.resolve(file.getFileName());
		checkFileNotExists(newFile);
		try {
			Files.move(file, newFile);
		} catch (final IOException e) {
			throw new KhelanTechnicalException(FileTechnicalErrorCode.IO, e);
		}
	}

	/**
	 * Checks if the file exists.
	 *
	 * @param file The file to check.
	 *
	 * @throws KhelanException If the file doesn't exist.
	 */
	private void checkFileExists(final Path file) throws KhelanException {
		if (Files.notExists(file)) {
			throw new KhelanFunctionalException(FileOperationsErrorCode.NOT_EXISTS);
		}
	}

	/**
	 * Checks if the file doesn't exist.
	 *
	 * @param file The file to check.
	 *
	 * @throws KhelanException If the file exists.
	 */
	private void checkFileNotExists(final Path file) throws KhelanException {
		if (Files.exists(file)) {
			throw new KhelanFunctionalException(FileOperationsErrorCode.ALREADY_EXISTS);
		}
	}

	/**
	 * Check if it is a directory. If it doesn't exist, creates this directory.
	 *
	 * @param directory The directory to check or create.
	 *
	 * @throws KhelanException If the path exists and is not a directory, or if the
	 *                         directory creation fails.
	 */
	private void checkDirectoryOrCreate(final Path directory) throws KhelanException {
		if (Files.exists(directory)) {
			if (!Files.isDirectory(directory)) {
				throw new KhelanFunctionalException(FileOperationsErrorCode.NOT_A_DIRECTORY);
			}
		} else {
			try {
				Files.createDirectories(directory);
			} catch (final IOException e) {
				throw new KhelanTechnicalException(FileTechnicalErrorCode.IO, e);
			}
		}
	}
}
