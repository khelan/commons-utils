package fr.khelan.utils.file.exception;

import fr.khelan.commons.label.CodeDescription;

/**
 * Error codes for the FlatFileWriter.
 */
public enum FileTechnicalErrorCode implements CodeDescription {
	/** I/O Exception. */
	IO("FILE-TECH-001", "I/O Exception.");

	private final String code;

	private final String description;

	private FileTechnicalErrorCode(final String code, final String description) {
		this.code = code;
		this.description = description;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String code() {
		return code;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String description() {
		return description;
	}

	@Override
	public String toString() {
		return code + ": " + description;
	}
}
