package fr.khelan.utils.file.flat;

import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import fr.khelan.commons.exception.KhelanTechnicalException;
import fr.khelan.commons.label.CodeDescription;
import fr.khelan.utils.file.exception.FileTechnicalErrorCode;
import fr.khelan.utils.file.flat.exception.FlatFileErrorCode;
import fr.khelan.utils.file.flat.field.FlatFileField;

/**
 * Class for writing flat files, in which each field is fixed-sized on the line.
 */
public class FlatFileWriter implements Closeable, Flushable {
	/** New line separator. */
	private String lineSeparator = System.getProperty("line.separator");

	/** Default filler character. */
	private char defaultFillerCharacter = ' ';

	/** Number filler character. */
	private char numberFillerCharacter = '0';

	private int numberOfDecimalsAmount = 2;

	/** String representation for true boolean value. */
	private String booleanTrue = "T";

	/** String representation for false boolean value. */
	private String booleanFalse = "F";

	/** The writer. */
	private final OutputStreamWriter writer;

	/** The line buffer. */
	private char[] buffer = null;

	/**
	 * Construct the {@link FlatFileWriter} from a stream.
	 *
	 * @param stream The output stream.
	 */
	public FlatFileWriter(final OutputStream stream) {
		writer = new OutputStreamWriter(stream);
	}

	/**
	 * Construct the {@link FlatFileWriter} from a stream for the specified
	 * charset.
	 *
	 * @param stream  The output stream.
	 * @param charset The charset.
	 */
	public FlatFileWriter(final OutputStream stream, final Charset charset) {
		writer = new OutputStreamWriter(stream, charset);
	}

	/**
	 * Init a line.
	 *
	 * @param length The length of the new line.
	 */
	public void initLine(final int length) {
		if (length <= 0) {
			buffer = null;
		} else {
			buffer = new char[length];
			Arrays.fill(buffer, defaultFillerCharacter);
		}
	}

	/**
	 * Writes the data of the line.
	 *
	 * @throws KhelanTechnicalException If it fails.
	 */
	public void endLine() throws KhelanTechnicalException {
		if (buffer != null) {
			try {
				// Writes data
				writer.write(buffer);

				// New Line
				if (StringUtils.isNotEmpty(lineSeparator)) {
					writer.write(lineSeparator);
				}
			} catch (final IOException e) {
				throw new KhelanTechnicalException(FileTechnicalErrorCode.IO, e);
			}

			// Clear buffer
			buffer = null;
		}
	}

	/**
	 * Writes a string field in the line.
	 *
	 * @param field    The data.
	 * @param position Where to write data in the line.
	 *
	 * @throws KhelanTechnicalException If it fails.
	 */
	public void write(final String field, final FlatFileField position) throws KhelanTechnicalException {
		if (field != null) {
			write(field, false, position);
		}
	}

	/**
	 * Writes a number field in the line.
	 *
	 * @param field    The data.
	 * @param position Where to write data in the line.
	 *
	 * @throws KhelanTechnicalException If it fails.
	 */
	public void write(final Integer field, final FlatFileField position) throws KhelanTechnicalException {
		if (field != null) {
			write(field.toString(), true, position);
		}
	}

	/**
	 * Writes a number field in the line.
	 *
	 * @param field    The data.
	 * @param position Where to write data in the line.
	 *
	 * @throws KhelanTechnicalException If it fails.
	 */
	public void write(final Long field, final FlatFileField position) throws KhelanTechnicalException {
		if (field != null) {
			write(field.toString(), true, position);
		}
	}

	/**
	 *
	 * Writes a date field in the line.
	 *
	 * @param date       The date.
	 * @param dateFormat The format of the date.
	 * @param position   Where to write data in the line.
	 *
	 * @throws KhelanTechnicalException If it fails.
	 *
	 */
	public void write(final Date date, final String dateFormat, final FlatFileField position)
			throws KhelanTechnicalException {
		if (date != null) {
			write(new SimpleDateFormat(dateFormat).format(date), false, position);
		}
	}

	/**
	 * Writes a boolean field in the line.
	 *
	 * @param field    The data.
	 * @param position Where to write data in the line.
	 *
	 * @throws KhelanTechnicalException If it fails.
	 */
	public void write(final Boolean field, final FlatFileField position) throws KhelanTechnicalException {
		write(Boolean.TRUE.equals(field) ? booleanTrue : booleanFalse, false, position);
	}

	/**
	 * Writes an amount field in the line.
	 *
	 * @param field    The data.
	 * @param position Where to write data in the line.
	 *
	 * @throws KhelanTechnicalException If it fails.
	 */
	public void write(final BigDecimal field, final FlatFileField position) throws KhelanTechnicalException {
		if (field != null) {
			write(field.multiply(BigDecimal.TEN.pow(numberOfDecimalsAmount)).setScale(0, RoundingMode.HALF_UP)
					.toString(), true, position);
		}
	}

	/**
	 * Writes the code of a CodeDescription Enumeration in the line.
	 *
	 * @param codeDescription The code.
	 * @param position        Where to write data in the line.
	 *
	 * @throws KhelanTechnicalException If it fails.
	 */
	public void writeCode(final CodeDescription codeDescription, final FlatFileField position)
			throws KhelanTechnicalException {
		if (codeDescription != null) {
			write(codeDescription.code(), position);
		}
	}

	/**
	 * Writes the description of a CodeDescription Enumeration in the line.
	 *
	 * @param codeDescription Le code/description.
	 * @param position        Where to write data in the line.
	 *
	 * @throws KhelanTechnicalException If it fails.
	 */
	public void writeDescription(final CodeDescription codeDescription, final FlatFileField position)
			throws KhelanTechnicalException {
		if (codeDescription != null) {
			write(codeDescription.description(), position);
		}
	}

	/**
	 * Writes the field in the line.
	 *
	 * @param field    The data.
	 * @param isNumber If true, writes like a number (number filler before data).
	 *                 Otherwise, writes default (default filler after data).
	 * @param position Where to write data in the line.
	 *
	 * @throws KhelanTechnicalException If it fails.
	 */
	protected void write(final String field, final boolean isNumber, final FlatFileField position)
			throws KhelanTechnicalException {
		if (buffer == null) {
			throw new KhelanTechnicalException(FlatFileErrorCode.LINE_INIT);
		}

		if (position.starts() + position.length() - 1 > buffer.length) {
			throw new KhelanTechnicalException(FlatFileErrorCode.FIELD_SIZE);
		}

		if (field.length() > position.length()) {
			throw new KhelanTechnicalException(FlatFileErrorCode.FIELD_DATA_SIZE);
		}

		int start = position.starts() - 1;
		int end;

		if (isNumber) {
			end = start + position.length() - field.length();
			for (int i = start; i < end; i++) {
				buffer[i] = numberFillerCharacter;
			}
			start = end;
			end = start + field.length();
			for (int i = start; i < end; i++) {
				buffer[i] = field.charAt(i - start);
			}
		} else {
			end = start + Math.min(position.length(), field.length());
			for (int i = start; i < end; i++) {
				buffer[i] = field.charAt(i - start);
			}
		}
	}

	/**
	 * End line and flush the writer.
	 */
	@Override
	public void flush() throws IOException {
		try {
			endLine();
		} catch (final KhelanTechnicalException e) {
			throw new IOException(e);
		}
		writer.flush();
	}

	/**
	 * Close the writer.
	 */
	@Override
	public void close() throws IOException {
		writer.close();
	}

	/**
	 * @return the lineSeparator
	 */
	public String getLineSeparator() {
		return lineSeparator;
	}

	/**
	 * @param lineSeparator the lineSeparator to set
	 */
	public void setLineSeparator(final String lineSeparator) {
		this.lineSeparator = lineSeparator;
	}

	/**
	 * @return the defaultFillerCharacter
	 */
	public char getDefaultFillerCharacter() {
		return defaultFillerCharacter;
	}

	/**
	 * @param defaultFillerCharacter the defaultFillerCharacter to set
	 */
	public void setDefaultFillerCharacter(final char defaultFillerCharacter) {
		this.defaultFillerCharacter = defaultFillerCharacter;
	}

	/**
	 * @return the numberFillerCharacter
	 */
	public char getNumberFillerCharacter() {
		return numberFillerCharacter;
	}

	/**
	 * @param numberFillerCharacter the numberFillerCharacter to set
	 */
	public void setNumberFillerCharacter(final char numberFillerCharacter) {
		this.numberFillerCharacter = numberFillerCharacter;
	}

	/**
	 * @return the numberOfDecimalsAmount
	 */
	public int getNumberOfDecimalsAmount() {
		return numberOfDecimalsAmount;
	}

	/**
	 * @param numberOfDecimalsAmount the numberOfDecimalsAmount to set
	 */
	public void setNumberOfDecimalsAmount(final int numberOfDecimalsAmount) {
		this.numberOfDecimalsAmount = numberOfDecimalsAmount;
	}

	/**
	 * @return the booleanTrue
	 */
	public String getBooleanTrue() {
		return booleanTrue;
	}

	/**
	 * @param booleanTrue the booleanTrue to set
	 */
	public void setBooleanTrue(final String booleanTrue) {
		this.booleanTrue = booleanTrue;
	}

	/**
	 * @return the booleanFalse
	 */
	public String getBooleanFalse() {
		return booleanFalse;
	}

	/**
	 * @param booleanFalse the booleanFalse to set
	 */
	public void setBooleanFalse(final String booleanFalse) {
		this.booleanFalse = booleanFalse;
	}
}
