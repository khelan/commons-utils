package fr.khelan.utils.file.charset;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CodingErrorAction;
import java.nio.file.Path;

import fr.khelan.commons.exception.KhelanTechnicalException;
import fr.khelan.utils.file.exception.FileTechnicalErrorCode;

/**
 * <p>
 * Utility to convert from a file in a defined charset to another file in a
 * defined charset.
 * </p>
 * <p>
 * This utility should work with other input types and output types than files,
 * as it uses {@link InputStream} and {@link OutputStream} to read and write
 * data.
 * </p>
 */
public class FileCharsetConverter {
	/** The buffer length to read/write data. */
	private static final int BUFFER_LENGTH = 12288;

	/** The charset decoder used to read bytes. */
	private final CharsetDecoder decoder;

	/** The charset encoder used to write bytes. */
	private final CharsetEncoder encoder;

	/**
	 * Creates a FileCharsetConverter with defined input and output charsets.
	 *
	 * @param inputCharset  The charset used to read input data.
	 * @param outputCharset The charset used to write output data.
	 */
	public FileCharsetConverter(final Charset inputCharset, final Charset outputCharset) {
		this(inputCharset, outputCharset, null);
	}

	/**
	 * Creates a FileCharsetConverter with defined input and output charsets.
	 *
	 * @param inputCharset  The charset used to read input data.
	 * @param outputCharset The charset used to write output data.
	 * @param replacement   The replacement character for unknown ones in outputCharset.
	 */
	public FileCharsetConverter(final Charset inputCharset, final Charset outputCharset, final String replacement) {
		decoder = inputCharset.newDecoder();
		encoder = outputCharset.newEncoder();
		if (replacement != null) {
			decoder.replaceWith(replacement);
			decoder.onUnmappableCharacter(CodingErrorAction.REPLACE);
			encoder.replaceWith(replacement.getBytes(outputCharset));
			encoder.onUnmappableCharacter(CodingErrorAction.REPLACE);
		}
	}

	/**
	 * Converts bytes from the {@link InputStream} to the {@link OutputStream},
	 * converting bytes by using charsets defined at the initialization of this
	 * {@link FileCharsetConverter}.
	 *
	 * @param input  The {@link InputStream} from which bytes data is read.
	 * @param output The {@link OutputStream} to which bytes data is written.
	 *
	 * @throws KhelanTechnicalException If an error occurs on reading or writing
	 *                                  data.
	 */
	public void convert(final InputStream input, final OutputStream output) throws KhelanTechnicalException {
		try (final Reader reader = new InputStreamReader(input, decoder);
				final Writer writer = new OutputStreamWriter(output, encoder)) {
			final char[] buffer = new char[BUFFER_LENGTH];
			int n;
			while ((n = reader.read(buffer)) > 0) {
				writer.write(buffer, 0, n);
			}
		} catch (final IOException e) {
			throw new KhelanTechnicalException(FileTechnicalErrorCode.IO, e);
		}
	}

	/**
	 * Converts bytes from the {@link InputStream} to the {@link OutputStream},
	 * converting bytes by using charsets defined at the initialization of this
	 * {@link FileCharsetConverter}.
	 *
	 * @param input  The {@link InputStream} from which bytes data is read.
	 * @param output The {@link OutputStream} to which bytes data is written.
	 *
	 * @throws KhelanTechnicalException If an error occurs on reading or writing
	 *                                  data.
	 */
	public void convert(final Path input, final Path output) throws KhelanTechnicalException {
		try (InputStream inputStream = new FileInputStream(input.toFile());
				OutputStream outputStream = new FileOutputStream(output.toFile())) {
			convert(inputStream, outputStream);
		} catch (final IOException e) {
			throw new KhelanTechnicalException(FileTechnicalErrorCode.IO, e);
		}
	}
}
