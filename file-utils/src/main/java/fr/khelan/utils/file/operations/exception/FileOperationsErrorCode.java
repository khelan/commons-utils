package fr.khelan.utils.file.operations.exception;

import fr.khelan.commons.label.CodeDescription;

/**
 * Error codes for the FlatFileWriter.
 */
public enum FileOperationsErrorCode implements CodeDescription {
	/** File doesn't exist. */
	NOT_EXISTS("FILE-OPE-001", "File doesn't exist."),
	/** File doesn't exists. */
	ALREADY_EXISTS("FILE-OPE-002", "File already exists."),
	/** File doesn't exists. */
	NOT_A_DIRECTORY("FILE-OPE-003", "It is not a directory.");

	private final String code;

	private final String description;

	private FileOperationsErrorCode(final String code, final String description) {
		this.code = code;
		this.description = description;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String code() {
		return code;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String description() {
		return description;
	}

	@Override
	public String toString() {
		return code + ": " + description;
	}
}
