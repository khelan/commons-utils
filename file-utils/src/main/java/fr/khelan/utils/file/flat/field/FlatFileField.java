package fr.khelan.utils.file.flat.field;

/**
 * Interface for flat file fields enumerations.
 */
public interface FlatFileField {
	/**
	 * Get the start index of the field in the line.
	 *
	 * @return The position of the field in the line. The position starts from 1.
	 */
	int starts();

	/**
	 * Get the length of the field.
	 *
	 * @return The length of the field.
	 */
	int length();
}
