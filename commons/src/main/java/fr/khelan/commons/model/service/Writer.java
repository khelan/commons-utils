package fr.khelan.commons.model.service;

public interface Writer<E, K> {
	E create(E model);

	E update(K key, E model);

	void delete(K key);
}
