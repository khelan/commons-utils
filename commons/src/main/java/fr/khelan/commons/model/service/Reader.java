package fr.khelan.commons.model.service;

import java.util.List;
import java.util.Optional;

public interface Reader<E, K> {
	Optional<E> findById(K key);

	List<E> findAll();
}
