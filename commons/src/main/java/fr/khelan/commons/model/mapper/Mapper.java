package fr.khelan.commons.model.mapper;

import java.util.Collection;
import java.util.List;

public interface Mapper<M, E> {
	M toModel(final E element);

	E fromModel(final M model);

	default List<M> toModelList(Collection<E> elements) {
		return elements.stream().map(this::toModel).toList();
	}

	default List<E> fromModelCollection(Collection<M> models) {
		return models.stream().map(this::fromModel).toList();
	}
}
