package fr.khelan.commons.observer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * A notifier (ie Observable) which can notify registered listeners when an event occured.
 *
 * @param <T>
 * 		The type of listener supported. Should be an interface provided by the notifier implementation.
 */
public abstract class Notifier<T extends Listener> {
	/**
	 * Collection of listeners to notify.<br/> There is a default implementation, but it can be provided by the
	 * implementation, for example if a call order is required.
	 */
	private final Collection<T> listeners;

	/**
	 * Creates a notifier with the default implementation of the collection of listeners.
	 */
	protected Notifier() {
		listeners = new ArrayList<>();
	}

	/**
	 * <p>
	 * Creates a notifier with the defined type of collection for the collection of listeners.
	 * </p>
	 * <p>
	 * Useful to create a {@link List} (keeping order), a {@link Set} like a {@link TreeSet} (logical order of
	 * elements).
	 * </p>
	 *
	 * @param collection
	 * 		The collection to store listeners.
	 */
	protected Notifier(final Collection<T> collection) {
		listeners = collection;
		listeners.clear();
	}

	/**
	 * Adds a listener to the list of notified listeners.
	 *
	 * @param listener
	 * 		The listener to add.
	 *
	 * @return The added listener.
	 */
	public T registerListener(final T listener) {
		listeners.add(listener);
		return listener;
	}

	/**
	 * Removes a listener from the list of notified listeners.
	 *
	 * @param listener
	 * 		The listener to remove.
	 */
	public void unregisterListener(final T listener) {
		listeners.remove(listener);
	}

	/**
	 * Returns the collection of listeners.
	 *
	 * @return The collection of notified listeners.
	 */
	protected Collection<T> getListeners() {
		return listeners;
	}
}
