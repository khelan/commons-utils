package fr.khelan.commons.observer;

/**
 * <p>
 * A listener (ie Observer) which can be notified by a {@link Notifier} (ie * Observable) for the Observer Pattern.
 * </p>
 *
 * <p>
 * This is the generic interface.
 * </p>
 */
public interface Listener {

}
