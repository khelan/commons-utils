package fr.khelan.commons.collection;

import java.io.Serial;
import java.io.Serializable;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

/**
 * Implements a doubly-linked list of elements.
 *
 * @param <E>
 * 		The generic type of this list.
 */
public class DoublyLinkedList<E> implements List<E>, Deque<E>, Cloneable, Serializable {
	@Serial
	private static final long serialVersionUID = 1L;

	/** Reference to first node of the linked list. */
	private transient Node<E> first;
	/** Reference to last node of the linked list. */
	private transient Node<E> last;
	private transient int size;

	/**
	 * Initializes an empty Doubly-linked list.
	 */
	public DoublyLinkedList() {

	}

	/**
	 * Initializes a new Doubly-linked list containing items of the specified collection, in the order they are returned
	 * by the collection's iterator.
	 *
	 * @param collection
	 * 		The collection whose items are added in the new list.
	 */
	public DoublyLinkedList(final Collection<E> collection) {
		this();
		addAll(collection);
	}

	/**
	 * Initializes a new Doubly-linked list containing items of the specified array in the same order.
	 *
	 * @param elements
	 * 		The array whose items are added in the new list.
	 */
	@SafeVarargs
	public DoublyLinkedList(final E... elements) {
		this();
		for (final E element : elements) {
			add(element);
		}
	}

	/**
	 * Returns the node at the index.
	 *
	 * @param index
	 * 		The index of the node to be returned.
	 *
	 * @return The node.
	 */
	private Node<E> node(final int index) {
		checkElementIndex(index);
		Node<E> item;
		if (index < size >> 1) {
			item = first;
			for (int i = 0; i < index; i++) {
				item = item.next;
			}
		} else {
			item = last;
			for (int i = size - 1; i > index; i--) {
				item = item.previous;
			}
		}
		return item;
	}

	/**
	 * Removes the node from the linked list.
	 *
	 * @param node
	 * 		The node to remove.
	 *
	 * @return The item of the removed node.
	 */
	private E removeNode(final Node<E> node) {
		E element = null;
		if (node != null) {
			element = node.item;
			final Node<E> previous = node.previous;
			final Node<E> next = node.next;

			if (previous != null) {
				previous.next = next;
				node.previous = null;
			} else {
				first = next;
			}
			if (next != null) {
				next.previous = previous;
				node.next = null;
			} else {
				last = previous;
			}
			node.item = null;
			size--;
		}
		return element;
	}

	/**
	 * Returns if the index is one of an existing node.
	 */
	private boolean isElementIndex(final int index) {
		return index >= 0 && index < size;
	}

	/**
	 * Returns if the index is one of a valid position to add a node.
	 */
	private boolean isPositionIndex(final int index) {
		return index >= 0 && index <= size;
	}

	private String outOfBoundsMessage(final int index) {
		return "Index: " + index + ", Size: " + size;
	}

	/**
	 * Checks if the index is one of an existing element.
	 *
	 * @param index
	 * 		the index to check
	 *
	 * @throws IndexOutOfBoundsException
	 * 		if the index is not one of an existing node.
	 */
	protected void checkElementIndex(final int index) throws IndexOutOfBoundsException {
		if (!isElementIndex(index)) {
			throw new IndexOutOfBoundsException(outOfBoundsMessage(index));
		}
	}

	/**
	 * Checks if the index is one of a valid position to add a node.
	 *
	 * @param index
	 * 		the index to check
	 *
	 * @throws IndexOutOfBoundsException
	 * 		if the index is not one of a valid position to add a node.
	 */
	protected void checkPositionIndex(final int index) throws IndexOutOfBoundsException {
		if (!isPositionIndex(index)) {
			throw new IndexOutOfBoundsException(outOfBoundsMessage(index));
		}
	}

	protected void addBefore(final E element, final Node<E> next) {
		final Node<E> previous = next.previous;
		final Node<E> newNode = new Node<>(previous, element, next);
		next.previous = newNode;
		if (previous != null) {
			previous.next = newNode;
		} else {
			first = newNode;
		}
		size++;
	}

	@Override
	public boolean offerFirst(final E e) {
		addFirst(e);
		return true;
	}

	@Override
	public boolean offerLast(final E e) {
		addLast(e);
		return true;
	}

	@Override
	public E pollFirst() {
		return removeNode(first);
	}

	@Override
	public E pollLast() {
		return removeNode(last);
	}

	@Override
	public E peekFirst() {
		return first != null ? first.item : null;
	}

	@Override
	public E peekLast() {
		return last != null ? last.item : null;
	}

	@Override
	public boolean removeFirstOccurrence(final Object o) {
		final NodePosition<E> nodePosition = firstOccurence(o);
		return removeNode(nodePosition.node) != null;
	}

	@Override
	public boolean removeLastOccurrence(final Object o) {
		final NodePosition<E> nodePosition = lastOccurence(o);
		return removeNode(nodePosition.node) != null;
	}

	@Override
	public boolean offer(final E e) {
		return offerLast(e);
	}

	@Override
	public E remove() {
		return removeFirst();
	}

	@Override
	public E poll() {
		return pollFirst();
	}

	@Override
	public E element() {
		return getFirst();
	}

	@Override
	public E peek() {
		return peekFirst();
	}

	@Override
	public void push(final E e) {
		addFirst(e);
	}

	@Override
	public E pop() {
		return removeFirst();
	}

	@Override
	public Iterator<E> descendingIterator() {
		return new DescendingItr();
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public boolean contains(final Object o) {
		return indexOf(o) != -1;
	}

	@Override
	public Iterator<E> iterator() {
		return new AscendingItr();
	}

	@Override
	public Object[] toArray() {
		return toArray(new Object[size]);
	}

	@Override
	public <T> T[] toArray(final T[] a) {
		T[] result = a;
		if (a.length < size) {
			result = (T[]) java.lang.reflect.Array.newInstance(a.getClass().getComponentType(), size);
		}
		int i = 0;
		final Object[] objectResult = result;
		for (Node<E> x = first; x != null; x = x.next) {
			objectResult[i++] = x.item;
		}

		if (result.length > size) {
			result[size] = null;
		}

		return result;
	}

	@Override
	public boolean add(final E e) {
		addLast(e);
		return true;
	}

	@Override
	public boolean remove(final Object o) {
		return removeFirstOccurrence(o);
	}

	@Override
	public boolean containsAll(final Collection<?> c) {
		boolean result = true;
		for (final Object o : c) {
			if (!contains(o)) {
				result = false;
				break;
			}
		}
		return result;
	}

	@Override
	public boolean addAll(final Collection<? extends E> c) {
		return addAll(size, c);
	}

	@Override
	public boolean addAll(final int index, final Collection<? extends E> c) {
		checkPositionIndex(index);
		Objects.requireNonNull(c);

		if (index == size) {
			for (final E e : c) {
				addLast(e);
			}
		} else {
			final Node<E> node = node(index);
			for (final E e : c) {
				addBefore(e, node);
			}
		}

		return true;
	}

	@Override
	public boolean removeAll(final Collection<?> c) {
		Objects.requireNonNull(c);
		boolean modified = false;
		final Iterator<E> iterator = iterator();
		while (iterator.hasNext()) {
			if (c.contains(iterator.next())) {
				iterator.remove();
				modified = true;
			}
		}
		return modified;
	}

	@Override
	public boolean retainAll(final Collection<?> c) {
		Objects.requireNonNull(c);
		boolean modified = false;
		final Iterator<E> iterator = iterator();
		while (iterator.hasNext()) {
			if (!c.contains(iterator.next())) {
				iterator.remove();
				modified = true;
			}
		}
		return modified;
	}

	@Override
	public void clear() {
		Node<E> x = first;
		while (x != null) {
			final Node<E> next = x.next;
			x.previous = null;
			x.item = null;
			x.next = null;
			x = next;
		}
		first = null;
		last = null;
		size = 0;
	}

	@Override
	public E get(final int index) {
		checkElementIndex(index);
		return node(index).item;
	}

	@Override
	public E set(final int index, final E element) {
		checkElementIndex(index);
		final Node<E> node = node(index);
		final E oldElement = node.item;
		node.item = element;

		return oldElement;
	}

	@Override
	public void add(final int index, final E element) {
		checkPositionIndex(index);

		if (index == size) {
			addLast(element);
		} else {
			addBefore(element, node(index));
		}
	}

	@Override
	public E remove(final int index) {
		checkElementIndex(index);
		return removeNode(node(index));
	}

	@Override
	public int indexOf(final Object o) {
		return firstOccurence(o).position;
	}

	@Override
	public int lastIndexOf(final Object o) {
		return lastOccurence(o).position;
	}

	@Override
	public ListIterator<E> listIterator() {
		return listIterator(0);
	}

	@Override
	public ListIterator<E> listIterator(final int index) {
		return new ListItr(index);
	}

	@Override
	public List<E> subList(final int fromIndex, final int toIndex) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void addFirst(final E e) {
		final Node<E> newNode = new Node<>(null, e, first);
		if (first != null) {
			first.previous = newNode;
		} else {
			last = newNode;
		}
		first = newNode;
		size++;
	}

	@Override
	public void addLast(final E e) {
		final Node<E> newNode = new Node<>(last, e, null);
		if (last != null) {
			last.next = newNode;
		} else {
			first = newNode;
		}
		last = newNode;
		size++;
	}

	@Override
	public E getFirst() {
		if (first == null) {
			throw new NoSuchElementException();
		}
		return first.item;
	}

	@Override
	public E getLast() {
		if (last == null) {
			throw new NoSuchElementException();
		}
		return last.item;
	}

	@Override
	public E removeFirst() {
		if (first == null) {
			throw new NoSuchElementException();
		}
		return removeNode(first);
	}

	@Override
	public E removeLast() {
		if (last == null) {
			throw new NoSuchElementException();
		}
		return removeNode(last);
	}

	@Override
	public DoublyLinkedList<E> reversed() {
		return new ReverseOrderLinkedListView<>(this, List.super.reversed(), Deque.super.reversed());
	}

	private NodePosition<E> firstOccurence(final Object o) {
		int index = 0;
		final NodePosition<E> nodePosition = new NodePosition<>(-1, null);

		if (o != null) {
			for (Node<E> node = first; node != null; node = node.next) {
				if (o.equals(node.item)) {
					nodePosition.position = index;
					nodePosition.node = node;
					break;
				}
				index++;
			}
		} else {
			for (Node<E> node = first; node != null; node = node.next) {
				if (node.item == null) {
					nodePosition.position = index;
					nodePosition.node = node;
					break;
				}
				index++;
			}
		}
		return nodePosition;
	}

	private NodePosition<E> lastOccurence(final Object o) {
		int index = size;
		final NodePosition<E> nodePosition = new NodePosition<>(-1, null);

		if (o != null) {
			for (Node<E> node = last; node != null; node = node.previous) {
				index--;
				if (o.equals(node.item)) {
					nodePosition.position = index;
					nodePosition.node = node;
					break;
				}
			}
		} else {
			for (Node<E> node = last; node != null; node = node.previous) {
				index--;
				if (node.item == null) {
					nodePosition.position = index;
					nodePosition.node = node;
					break;
				}
			}
		}
		return nodePosition;
	}

	@Override
	public int hashCode() {
		int hashCode = 1;
		for (final E element : this) {
			hashCode = 31 * hashCode + (element == null ? 0 : element.hashCode());
		}
		return hashCode;
	}

	@Override
	public boolean equals(final Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof List)) {
			return false;
		}

		final ListIterator<E> iterator1 = listIterator();
		final ListIterator<?> iterator2 = ((List<?>) o).listIterator();
		while (iterator1.hasNext() && iterator2.hasNext()) {
			final E item1 = iterator1.next();
			final Object item2 = iterator2.next();
			if (!(Objects.equals(item1, item2))) {
				return false;
			}
		}
		return !(iterator1.hasNext() || iterator2.hasNext());
	}

	@Override
	public DoublyLinkedList<E> clone() throws CloneNotSupportedException {
		@SuppressWarnings("unchecked") final DoublyLinkedList<E> clone = (DoublyLinkedList<E>) super.clone();
		clone.first = null;
		clone.last = null;
		clone.size = 0;

		clone.addAll(this);

		return clone;
	}

	@Override
	public String toString() {
		final Iterator<E> it = iterator();
		final StringBuilder sb = new StringBuilder();
		sb.append('[');
		while (it.hasNext()) {
			sb.append(it.next());
			if (it.hasNext()) {
				sb.append(", ");
			}
		}
		sb.append(']');

		return sb.toString();
	}

	private static class Node<E> {
		private E item;
		private Node<E> previous, next;

		private Node(final Node<E> previous, final E element, final Node<E> next) {
			this.previous = previous;
			this.item = element;
			this.next = next;
		}
	}

	private static class NodePosition<E> {
		private int position;
		private Node<E> node;

		private NodePosition(final int position, final Node<E> node) {
			this.position = position;
			this.node = node;
		}
	}

	private static class ReverseOrderLinkedListView<E> extends DoublyLinkedList<E> {
		private final transient DoublyLinkedList<E> list;
		private final transient List<E> rlist;
		private final transient Deque<E> rdeque;

		private ReverseOrderLinkedListView(DoublyLinkedList<E> list, List<E> rlist, Deque<E> rdeque) {
			this.list = list;
			this.rlist = rlist;
			this.rdeque = rdeque;
		}

		@Override
		public <T> T[] toArray(IntFunction<T[]> generator) {
			return rlist.toArray(generator);
		}

		@Override
		public boolean removeIf(Predicate<? super E> filter) {
			return rlist.removeIf(filter);
		}

		@Override
		public Stream<E> stream() {
			return rlist.stream();
		}

		@Override
		public Stream<E> parallelStream() {
			return rlist.parallelStream();
		}

		@Override
		public void forEach(Consumer<? super E> action) {
			rlist.forEach(action);
		}

		@Override
		public void replaceAll(UnaryOperator<E> operator) {
			rlist.replaceAll(operator);
		}

		@Override
		public void sort(Comparator<? super E> c) {
			rlist.sort(c);
		}

		@Override
		public Spliterator<E> spliterator() {
			return rlist.spliterator();
		}

		@Override
		public boolean offerFirst(E e) {
			return rdeque.offerFirst(e);
		}

		@Override
		public boolean offerLast(E e) {
			return rdeque.offerLast(e);
		}

		@Override
		public E pollFirst() {
			return rdeque.pollFirst();
		}

		@Override
		public E pollLast() {
			return rdeque.pollLast();
		}

		@Override
		public E peekFirst() {
			return rdeque.peekFirst();
		}

		@Override
		public E peekLast() {
			return rdeque.peekLast();
		}

		@Override
		public boolean removeFirstOccurrence(Object o) {
			return rdeque.removeFirstOccurrence(o);
		}

		@Override
		public boolean removeLastOccurrence(Object o) {
			return rdeque.removeLastOccurrence(o);
		}

		@Override
		public boolean offer(E e) {
			return rdeque.offer(e);
		}

		@Override
		public E remove() {
			return rdeque.remove();
		}

		@Override
		public E poll() {
			return rdeque.poll();
		}

		@Override
		public E element() {
			return rdeque.element();
		}

		@Override
		public E peek() {
			return rdeque.peek();
		}

		@Override
		public void push(E e) {
			rdeque.push(e);
		}

		@Override
		public E pop() {
			return rdeque.pop();
		}

		@Override
		public Iterator<E> descendingIterator() {
			return rdeque.descendingIterator();
		}

		@Override
		public int size() {
			return rlist.size();
		}

		@Override
		public boolean isEmpty() {
			return rlist.isEmpty();
		}

		@Override
		public boolean contains(Object o) {
			return rlist.contains(o);
		}

		@Override
		public Iterator<E> iterator() {
			return rlist.iterator();
		}

		@Override
		public Object[] toArray() {
			return rlist.toArray();
		}

		@Override
		public <T> T[] toArray(T[] a) {
			return rlist.toArray(a);
		}

		@Override
		public boolean add(E e) {
			return rlist.add(e);
		}

		@Override
		public boolean remove(Object o) {
			return rlist.remove(o);
		}

		@Override
		public boolean containsAll(Collection<?> c) {
			return rlist.containsAll(c);
		}

		@Override
		public boolean addAll(Collection<? extends E> c) {
			return rlist.addAll(c);
		}

		@Override
		public boolean addAll(int index, Collection<? extends E> c) {
			return rlist.addAll(index, c);
		}

		@Override
		public boolean removeAll(Collection<?> c) {
			return rlist.removeAll(c);
		}

		@Override
		public boolean retainAll(Collection<?> c) {
			return rlist.retainAll(c);
		}

		@Override
		public void clear() {
			rlist.clear();
		}

		@Override
		public E get(int index) {
			return rlist.get(index);
		}

		@Override
		public E set(int index, E element) {
			return rlist.set(index, element);
		}

		@Override
		public void add(int index, E element) {
			rlist.add(index, element);
		}

		@Override
		public E remove(int index) {
			return rlist.remove(index);
		}

		@Override
		public int indexOf(Object o) {
			return rlist.indexOf(o);
		}

		@Override
		public int lastIndexOf(Object o) {
			return rlist.lastIndexOf(o);
		}

		@Override
		public ListIterator<E> listIterator() {
			return rlist.listIterator();
		}

		@Override
		public ListIterator<E> listIterator(int index) {
			return rlist.listIterator(index);
		}

		@Override
		public List<E> subList(int fromIndex, int toIndex) {
			return rlist.subList(fromIndex, toIndex);
		}

		@Override
		public void addFirst(E e) {
			rdeque.addFirst(e);
		}

		@Override
		public void addLast(E e) {
			rdeque.addLast(e);
		}

		@Override
		public E getFirst() {
			return rdeque.getFirst();
		}

		@Override
		public E getLast() {
			return rdeque.getLast();
		}

		@Override
		public E removeFirst() {
			return rdeque.removeFirst();
		}

		@Override
		public E removeLast() {
			return rdeque.removeLast();
		}

		@Override
		public DoublyLinkedList<E> reversed() {
			return list;
		}

		@Override
		public int hashCode() {
			return rlist.hashCode();
		}

		@Override
		public boolean equals(Object o) {
			return rlist.equals(o);
		}

		@Override
		public String toString() {
			return rlist.toString();
		}
	}

	private class AscendingItr implements Iterator<E> {
		private final ListItr iterator = new ListItr(0);

		@Override
		public boolean hasNext() {
			return iterator.hasNext();
		}

		@Override
		public E next() {
			return iterator.next();
		}

		@Override
		public void remove() {
			iterator.remove();
		}
	}

	private class DescendingItr implements Iterator<E> {
		private final ListItr iterator = new ListItr(size);

		@Override
		public boolean hasNext() {
			return iterator.hasPrevious();
		}

		@Override
		public E next() {
			return iterator.previous();
		}

		@Override
		public void remove() {
			iterator.remove();
		}
	}

	private class ListItr implements ListIterator<E> {
		private Node<E> currentNode;
		private int currentIndex;
		private boolean lastReturned;

		private ListItr(final int index) {
			checkPositionIndex(index);
			if (index == 0) {
				currentNode = new Node<>(null, null, first);
			} else if (index == size) {
				currentNode = new Node<>(last, null, null);
			} else {
				currentNode = node(index);
			}
			currentIndex = index;
		}

		@Override
		public boolean hasNext() {
			return currentNode.next != null;
		}

		@Override
		public E next() {
			if (!hasNext()) {
				throw new NoSuchElementException();
			}
			currentNode = currentNode.next;
			lastReturned = true;
			currentIndex++;
			return currentNode.item;
		}

		@Override
		public boolean hasPrevious() {
			return currentNode.previous != null;
		}

		@Override
		public E previous() {
			if (!hasPrevious()) {
				throw new NoSuchElementException();
			}
			currentNode = currentNode.previous;
			lastReturned = true;
			currentIndex--;
			return currentNode.item;
		}

		@Override
		public int nextIndex() {
			return currentIndex + 1;
		}

		@Override
		public int previousIndex() {
			return currentIndex - 1;
		}

		@Override
		public void remove() {
			if (!lastReturned) {
				throw new IllegalStateException();
			}
			lastReturned = false;
			final Node<E> node = new Node<>(currentNode.previous, null, currentNode.next);
			removeNode(currentNode);
			currentNode = node;
		}

		@Override
		public void set(final E e) {
			if (!lastReturned) {
				throw new IllegalStateException();
			}
			currentNode.item = e;
		}

		@Override
		public void add(final E e) {
			lastReturned = false;
			if (currentNode.next != null) {
				addBefore(e, currentNode.next);
			} else {
				addLast(e);
			}
			currentNode = currentNode.next;
		}
	}
}
