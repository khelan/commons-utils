package fr.khelan.commons.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Utils to concatenate strings by inserting a separator between each string.
 */
public class StringBuilderSeparator {
	private static final int DEFAULT_LENGTH = 20;

	/** The separator to insert between each string */
	private final String separator;

	private final List<CharSequence> stringList;

	/**
	 * Constructor with separator definition.
	 *
	 * @param separator
	 * 		The separator.
	 */
	protected StringBuilderSeparator(final char separator) {
		this(DEFAULT_LENGTH, String.valueOf(separator));
	}

	/**
	 * Constructor with separator definition.
	 *
	 * @param separator
	 * 		The separator.
	 */
	protected StringBuilderSeparator(final String separator) {
		this(DEFAULT_LENGTH, separator);
	}

	/**
	 * Constructor with separator definition, and an estimation of the default length of the {@link StringBuilder}.
	 *
	 * @param defaultLength
	 * 		The default number of the {@link String} to concatenate.
	 * @param separator
	 * 		The separator.
	 */
	protected StringBuilderSeparator(final int defaultLength, final String separator) {
		this.separator = separator;
		stringList = new ArrayList<>(defaultLength);
	}

	/**
	 * Appends the string and return this {@link StringBuilderSeparator}.
	 *
	 * @param string
	 * 		The string to append.
	 *
	 * @return This {@link StringBuilderSeparator}.
	 */
	public StringBuilderSeparator append(final CharSequence string) {
		stringList.add(string);
		return this;
	}

	/**
	 * Appends each string element and return the concatenated string.
	 *
	 * @param list
	 * 		The list of strings.
	 *
	 * @return The concatenated string.
	 */
	public String append(final CharSequence... list) {
		for (final CharSequence string : list) {
			append(string);
		}
		return toString();
	}

	/**
	 * Appends each string element and return the concatenated string.
	 *
	 * @param list
	 * 		The collection of strings.
	 *
	 * @return The concatenated string.
	 */
	public String append(final Collection<? extends CharSequence> list) {
		for (final CharSequence string : list) {
			append(string);
		}
		return toString();
	}

	/**
	 * Repeats and concatenate a motif.
	 *
	 * @param motif
	 * 		The motif to repeat and concatenate.
	 * @param repeatNumber
	 * 		The number of times the motif have to be repeated.
	 *
	 * @return The concatenated string.
	 */
	public String append(final CharSequence motif, final int repeatNumber) {
		for (int i = 0; i < repeatNumber; i++) {
			append(motif);
		}
		return toString();
	}

	/**
	 * Returns the string concatenated by this {@link StringBuilderSeparator}.
	 *
	 * @return The concatenated string.
	 */
	@Override
	public String toString() {
		return String.join(separator, stringList);
	}
}
