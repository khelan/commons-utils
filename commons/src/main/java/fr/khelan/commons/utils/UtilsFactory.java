package fr.khelan.commons.utils;

public class UtilsFactory {

	/**
	 * Creates a {@link StringBuilderSeparator}.
	 *
	 * @param separator
	 * 		The separator.
	 *
	 * @return An initialized {@link StringBuilderSeparator}.
	 *
	 * @see StringBuilderSeparator#StringBuilderSeparator(char)
	 */
	public StringBuilderSeparator createStringBuilderSeparator(final char separator) {
		return new StringBuilderSeparator(separator);
	}

	/**
	 * Creates a {@link StringBuilderSeparator}.
	 *
	 * @param separator
	 * 		The separator.
	 *
	 * @return An initialized {@link StringBuilderSeparator}.
	 *
	 * @see StringBuilderSeparator#StringBuilderSeparator(String)
	 */
	public StringBuilderSeparator createStringBuilderSeparator(final String separator) {
		return new StringBuilderSeparator(separator);
	}

	/**
	 * Creates a {@link StringBuilderSeparator}.
	 *
	 * @param defaultLength
	 * 		The default number of {@link String} to concatenate.
	 * @param separator
	 * 		The separator.
	 *
	 * @return An initialized {@link StringBuilderSeparator}.
	 *
	 * @see StringBuilderSeparator#StringBuilderSeparator(int, String)
	 */
	public StringBuilderSeparator createStringBuilderSeparator(final int defaultLength, final String separator) {
		return new StringBuilderSeparator(defaultLength, separator);
	}
}
