package fr.khelan.commons.label;

import org.apache.commons.lang3.ObjectUtils;

/**
 * Interface for ordered elements
 */
public interface Ordered extends Comparable<Ordered> {
	/**
	 * Returns the order.
	 *
	 * @return The order.
	 */
	public Integer order();

	@Override
	default int compareTo(final Ordered elt) {
		return ObjectUtils.compare(order(), elt.order());
	}
}
