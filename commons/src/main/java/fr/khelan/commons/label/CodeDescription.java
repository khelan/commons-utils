package fr.khelan.commons.label;

import java.io.Serializable;

/**
 * Interface for code/description enumerations.
 */
public interface CodeDescription extends Serializable {
	/**
	 * Returns the code.
	 *
	 * @return The code.
	 */
	String code();

	/**
	 * Returns the description.
	 *
	 * @return The description.
	 */
	String description();
}
