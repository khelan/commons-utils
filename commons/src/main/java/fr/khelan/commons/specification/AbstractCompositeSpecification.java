package fr.khelan.commons.specification;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class AbstractCompositeSpecification<T> extends AbstractSpecification<T> {
	private final List<Specification<T>> specifications;

	@SafeVarargs
	protected AbstractCompositeSpecification(final Specification<T>... specifications) {
		this.specifications = new ArrayList<>();
		this.specifications.addAll(Arrays.asList(specifications));
	}

	protected List<Specification<T>> getSpecifications() {
		return specifications;
	}
}
