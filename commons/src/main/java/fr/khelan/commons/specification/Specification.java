package fr.khelan.commons.specification;

public interface Specification<T> {
	Specification<T> and(Specification<T> specification);

	Specification<T> or(Specification<T> specification);

	Specification<T> not();

	boolean isSatisfiedBy(T element);
}
