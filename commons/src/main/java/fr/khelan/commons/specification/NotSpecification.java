package fr.khelan.commons.specification;

public final class NotSpecification<T> extends AbstractSpecification<T> {
	private final Specification<T> specification;

	public NotSpecification(final Specification<T> specification) {
		this.specification = specification;
	}

	@Override
	public boolean isSatisfiedBy(final T element) {
		return !specification.isSatisfiedBy(element);

	}
}
