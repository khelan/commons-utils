package fr.khelan.commons.specification;

public final class OrSpecification<T> extends AbstractCompositeSpecification<T> {
	@SafeVarargs
	public OrSpecification(final Specification<T>... specifications) {
		super(specifications);
	}

	@Override
	public boolean isSatisfiedBy(final T element) {
		boolean result = false;
		for (final Specification<T> specification : getSpecifications()) {
			if (specification.isSatisfiedBy(element)) {
				result = true;
				break;
			}
		}
		return result;
	}
}
