package fr.khelan.commons.specification;

public final class AndSpecification<T> extends AbstractCompositeSpecification<T> {
	@SafeVarargs
	public AndSpecification(final Specification<T>... specifications) {
		super(specifications);
	}

	@Override
	public boolean isSatisfiedBy(final T element) {
		boolean result = true;
		for (final Specification<T> specification : getSpecifications()) {
			if (!specification.isSatisfiedBy(element)) {
				result = false;
				break;
			}
		}
		return result;
	}
}
