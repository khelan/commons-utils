package fr.khelan.commons.specification;

public abstract class AbstractSpecification<T> implements Specification<T> {

	@Override
	public final Specification<T> and(final Specification<T> specification) {
		return new AndSpecification<>(this, specification);
	}

	@Override
	public final Specification<T> or(final Specification<T> specification) {
		return new OrSpecification<>(this, specification);
	}

	@Override
	public final Specification<T> not() {
		return new NotSpecification<>(this);
	}
}
