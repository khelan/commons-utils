package fr.khelan.commons.exception;

import fr.khelan.commons.label.CodeDescription;
import lombok.Getter;

/**
 * Abstract class for application's exceptions.
 */
@Getter
public abstract class KhelanException extends Exception {
	private static final long serialVersionUID = 1L;

	private final CodeDescription errorCode;

	private final boolean blocking;

	/**
	 * Constructs an exception with an error and without cause.
	 *
	 * @param errorCode
	 * 		The error code of the exception. Cannot be null.
	 */
	protected KhelanException(final CodeDescription errorCode) {
		this(errorCode, true);
	}

	/**
	 * Constructs an exception with an error and without cause, specifying if it is blocking.
	 *
	 * @param errorCode
	 * 		The error code of the exception. Cannot be null.
	 * @param isBlocking
	 * 		The blocking indicator.
	 */
	protected KhelanException(final CodeDescription errorCode, final boolean isBlocking) {
		super(errorCode.toString());
		this.errorCode = errorCode;
		blocking = isBlocking;
	}

	/**
	 * Constructs an exception with a detail message and a cause.
	 *
	 * @param errorCode
	 * 		The error code of the exception. Cannot be null.
	 * @param cause
	 * 		The cause.
	 */
	protected KhelanException(final CodeDescription errorCode, final Throwable cause) {
		this(errorCode, true, cause);
	}

	/**
	 * Constructs an exception with a detail message and a cause, specifying if it is blocking.
	 *
	 * @param errorCode
	 * 		The error code of the exception. Cannot be null.
	 * @param isBlocking
	 * 		The blocking indicator.
	 * @param cause
	 * 		The cause.
	 */
	protected KhelanException(final CodeDescription errorCode, final boolean isBlocking, final Throwable cause) {
		super(errorCode.toString(), cause);
		this.errorCode = errorCode;
		blocking = isBlocking;
	}
}
