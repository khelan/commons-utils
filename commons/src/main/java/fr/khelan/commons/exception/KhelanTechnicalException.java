package fr.khelan.commons.exception;

import fr.khelan.commons.label.CodeDescription;

/**
 * Class for technical exceptions.
 */
public class KhelanTechnicalException extends KhelanException {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructs an exception with an error and without cause.
	 *
	 * @param errorCode
	 * 		The error code of the exception.
	 */
	public KhelanTechnicalException(final CodeDescription errorCode) {
		super(errorCode);
	}

	/**
	 * Constructs an exception with an error and a cause.
	 *
	 * @param errorCode
	 * 		The error code of the exception.
	 * @param cause
	 * 		The cause of the exception.
	 */
	public KhelanTechnicalException(final CodeDescription errorCode, final Throwable cause) {
		super(errorCode, cause);
	}
}
