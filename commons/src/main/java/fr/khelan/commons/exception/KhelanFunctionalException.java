package fr.khelan.commons.exception;

import fr.khelan.commons.label.CodeDescription;

/**
 * Class for functional exceptions.
 */
public class KhelanFunctionalException extends KhelanException {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructs an exception with an error and without cause.
	 *
	 * @param errorCode
	 * 		The error code of the exception.
	 */
	public KhelanFunctionalException(final CodeDescription errorCode) {
		super(errorCode);
	}

	/**
	 * Constructs an exception with an error and a cause.
	 *
	 * @param errorCode
	 * 		The error code of the exception.
	 * @param cause
	 * 		The cause of the exception.
	 */
	public KhelanFunctionalException(final CodeDescription errorCode, final Throwable cause) {
		super(errorCode, cause);
		isBlocking();
	}
}
