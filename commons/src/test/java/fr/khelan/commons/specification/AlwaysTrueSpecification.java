package fr.khelan.commons.specification;

public class AlwaysTrueSpecification extends AbstractSpecification<Object> {
	@Override
	public boolean isSatisfiedBy(Object element) {
		return true;
	}
}
