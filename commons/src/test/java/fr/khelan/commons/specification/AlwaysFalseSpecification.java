package fr.khelan.commons.specification;

public class AlwaysFalseSpecification extends AbstractSpecification<Object> {
	@Override
	public boolean isSatisfiedBy(Object element) {
		return false;
	}
}
