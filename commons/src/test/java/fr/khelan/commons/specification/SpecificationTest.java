package fr.khelan.commons.specification;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SpecificationTest {
	private final Specification<Object> alwaysFalseSpecification = new AlwaysFalseSpecification();
	private final Specification<Object> alwaysTrueSpecification = new AlwaysTrueSpecification();

	@Test
	void notSpecificationTest() {
		Assertions.assertTrue(alwaysFalseSpecification.not().isSatisfiedBy(null));
		Assertions.assertFalse(alwaysTrueSpecification.not().isSatisfiedBy(null));
	}

	@Test
	void andSpecificationTest() {
		final Specification<Object> alwaysTrueSpecificationBis = new AlwaysTrueSpecification();
		final Specification<Object> alwaysFalseSpecificationBis = new AlwaysFalseSpecification();
		Assertions.assertTrue(alwaysTrueSpecificationBis.and(alwaysTrueSpecification).isSatisfiedBy(null));

		Assertions.assertFalse(alwaysFalseSpecification.and(alwaysTrueSpecification).isSatisfiedBy(null));
		Assertions.assertFalse(alwaysTrueSpecification.and(alwaysFalseSpecification).isSatisfiedBy(null));
		Assertions.assertFalse(alwaysFalseSpecification.and(alwaysFalseSpecificationBis).isSatisfiedBy(null));
	}

	@Test
	void orSpecificationTest() {
		final Specification<Object> alwaysTrueSpecificationBis = new AlwaysFalseSpecification();
		final Specification<Object> alwaysFalseSpecificationBis = new AlwaysFalseSpecification();
		Assertions.assertTrue(alwaysFalseSpecification.or(alwaysTrueSpecification).isSatisfiedBy(null));
		Assertions.assertTrue(alwaysTrueSpecification.or(alwaysFalseSpecification).isSatisfiedBy(null));
		Assertions.assertTrue(alwaysTrueSpecification.or(alwaysTrueSpecificationBis).isSatisfiedBy(null));

		Assertions.assertFalse(alwaysFalseSpecification.or(alwaysFalseSpecificationBis).isSatisfiedBy(null));
	}
}
