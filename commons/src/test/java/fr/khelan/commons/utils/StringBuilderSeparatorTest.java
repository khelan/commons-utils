package fr.khelan.commons.utils;

import java.util.Arrays;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class StringBuilderSeparatorTest {
	private final UtilsFactory utilsFactory = new UtilsFactory();

	private StringBuilderSeparator stringBuilderSeparator;

	@BeforeEach
	void init() {
		stringBuilderSeparator = utilsFactory.createStringBuilderSeparator('|');
	}

	@Test
	void constructorsTest() {
		stringBuilderSeparator = utilsFactory.createStringBuilderSeparator('|');
		stringBuilderSeparator.append("A").append("B").append("C");
		Assertions.assertEquals("A|B|C", stringBuilderSeparator.toString());

		stringBuilderSeparator = utilsFactory.createStringBuilderSeparator("|");
		stringBuilderSeparator.append("A").append("B").append("C");
		Assertions.assertEquals("A|B|C", stringBuilderSeparator.toString());

		stringBuilderSeparator = utilsFactory.createStringBuilderSeparator(5, "|");
		stringBuilderSeparator.append("A").append("B").append("C");
		Assertions.assertEquals("A|B|C", stringBuilderSeparator.toString());
	}

	@Test
	void appendSingleElementTest() {
		stringBuilderSeparator.append("A").append("B").append("C");
		Assertions.assertEquals("A|B|C", stringBuilderSeparator.toString());
	}

	@Test
	void appendArrayTest() {
		Assertions.assertEquals("A|B|C", stringBuilderSeparator.append("A", "B", "C"));
	}

	@Test
	void appendListTest() {
		Assertions.assertEquals("A|B|C", stringBuilderSeparator.append(Arrays.asList("A", "B", "C")));
	}

	@Test
	void appendRepeatMotifTest() {
		Assertions.assertEquals("Z|Z|Z|Z|Z", stringBuilderSeparator.append("Z", 5));
	}
}
