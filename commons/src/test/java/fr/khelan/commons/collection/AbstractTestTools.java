package fr.khelan.commons.collection;

import java.util.Collection;

/**
 * Tools for tests.
 */
class AbstractTestTools {
	/**
	 * Concatenates each character of the collection into a {@link String}.
	 *
	 * @param collection
	 * 		The collection of characters.
	 *
	 * @return The {@link String} containing each character of the collection.
	 */
	protected String toString(final Collection<Character> collection) {
		final StringBuilder string = new StringBuilder(collection.size());
		for (final char character : collection) {
			string.append(character);
		}
		return string.toString();
	}
}
