package fr.khelan.commons.collection;

import java.util.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DoublyLinkedListTest extends AbstractTestTools {
	private final List<Character> controlList = Arrays.asList('A', 'B', 'C', 'D', 'E', 'F', 'G');
	private DoublyLinkedList<Character> list;

	private void initList() {
		list.clear();
		list.addAll(controlList);
	}

	@BeforeEach
	void init() {
		list = new DoublyLinkedList<>();
	}

	@Test
	void constructorsTest() {
		list = new DoublyLinkedList<>();
		Assertions.assertEquals(0, list.size());

		list = new DoublyLinkedList<>(Arrays.asList('A', 'B', 'C'));
		Assertions.assertEquals(3, list.size());
		Assertions.assertEquals(Character.valueOf('A'), list.get(0));
		Assertions.assertEquals(Character.valueOf('B'), list.get(1));
		Assertions.assertEquals(Character.valueOf('C'), list.get(2));

		list = new DoublyLinkedList<>('A', 'B', 'C');
		Assertions.assertEquals(3, list.size());
		Assertions.assertEquals(Character.valueOf('A'), list.get(0));
		Assertions.assertEquals(Character.valueOf('B'), list.get(1));
		Assertions.assertEquals(Character.valueOf('C'), list.get(2));
	}

	@Test
	void setTest() {
		initList();
		list.set(0, 'X');
		list.set(3, 'Y');
		list.set(6, 'Z');
		Assertions.assertEquals("XBCYEFZ", toString(list));
	}

	@Test
	void addTest() {
		list.add('A');
		list.add('B');
		list.add('C');
		Assertions.assertEquals(3, list.size());
		Assertions.assertEquals(Character.valueOf('A'), list.get(0));
		Assertions.assertEquals(Character.valueOf('B'), list.get(1));
		Assertions.assertEquals(Character.valueOf('C'), list.get(2));

		list.add(1, 'D');
		list.add(0, 'E');
		list.add(5, 'F');
		Assertions.assertEquals(6, list.size());
		Assertions.assertEquals(Character.valueOf('A'), list.get(1));
		Assertions.assertEquals(Character.valueOf('B'), list.get(3));
		Assertions.assertEquals(Character.valueOf('C'), list.get(4));
		Assertions.assertEquals(Character.valueOf('D'), list.get(2));
		Assertions.assertEquals(Character.valueOf('E'), list.get(0));
		Assertions.assertEquals(Character.valueOf('F'), list.get(5));
	}

	@Test
	void addFirstTest() {
		list.addFirst('A');
		list.addFirst('B');
		list.addFirst('C');
		Assertions.assertEquals(3, list.size());
		Assertions.assertEquals(Character.valueOf('A'), list.get(2));
		Assertions.assertEquals(Character.valueOf('B'), list.get(1));
		Assertions.assertEquals(Character.valueOf('C'), list.get(0));
	}

	@Test
	void addLastTest() {
		list.addLast('A');
		list.addLast('B');
		list.addLast('C');
		Assertions.assertEquals(3, list.size());
		Assertions.assertEquals(Character.valueOf('A'), list.get(0));
		Assertions.assertEquals(Character.valueOf('B'), list.get(1));
		Assertions.assertEquals(Character.valueOf('C'), list.get(2));
	}

	@Test
	void offerPeekPollTest() {
		Assertions.assertNull(list.peek());

		list.offer('A');
		list.offer('B');
		list.offer('C');
		Assertions.assertEquals(3, list.size());
		Assertions.assertEquals("ABC", toString(list));

		Assertions.assertEquals('A', list.peek().charValue());
		Assertions.assertEquals('A', list.poll().charValue());
		Assertions.assertEquals('B', list.peek().charValue());
		Assertions.assertEquals('B', list.poll().charValue());
		Assertions.assertEquals('C', list.peek().charValue());
		Assertions.assertEquals('C', list.poll().charValue());
	}

	@Test
	void offerPeekPollFirstTest() {
		Assertions.assertNull(list.peekFirst());

		list.offerFirst('A');
		list.offerFirst('B');
		list.offerFirst('C');
		Assertions.assertEquals(3, list.size());
		Assertions.assertEquals("CBA", toString(list));

		Assertions.assertEquals('C', list.peekFirst().charValue());
		Assertions.assertEquals('C', list.pollFirst().charValue());
		Assertions.assertEquals('B', list.peekFirst().charValue());
		Assertions.assertEquals('B', list.pollFirst().charValue());
		Assertions.assertEquals('A', list.peekFirst().charValue());
		Assertions.assertEquals('A', list.pollFirst().charValue());
	}

	@Test
	void offerPeekPollLastTest() {
		Assertions.assertNull(list.peekLast());

		list.offerLast('A');
		list.offerLast('B');
		list.offerLast('C');
		Assertions.assertEquals(3, list.size());
		Assertions.assertEquals("ABC", toString(list));

		Assertions.assertEquals('C', list.peekLast().charValue());
		Assertions.assertEquals('C', list.pollLast().charValue());
		Assertions.assertEquals('B', list.peekLast().charValue());
		Assertions.assertEquals('B', list.pollLast().charValue());
		Assertions.assertEquals('A', list.peekLast().charValue());
		Assertions.assertEquals('A', list.pollLast().charValue());
	}

	@Test
	void addAllAndCleanTest() {
		list.addAll(Arrays.asList('A', 'B', 'C'));
		Assertions.assertEquals(3, list.size());
		Assertions.assertEquals(Character.valueOf('A'), list.get(0));
		Assertions.assertEquals(Character.valueOf('B'), list.get(1));
		Assertions.assertEquals(Character.valueOf('C'), list.get(2));

		list.addAll(0, Arrays.asList('D', 'E'));
		Assertions.assertEquals(5, list.size());
		Assertions.assertEquals(Character.valueOf('A'), list.get(2));
		Assertions.assertEquals(Character.valueOf('B'), list.get(3));
		Assertions.assertEquals(Character.valueOf('C'), list.get(4));
		Assertions.assertEquals(Character.valueOf('D'), list.get(0));
		Assertions.assertEquals(Character.valueOf('E'), list.get(1));

		list.addAll(2, Arrays.asList('F', 'G'));
		Assertions.assertEquals(7, list.size());
		Assertions.assertEquals(Character.valueOf('A'), list.get(4));
		Assertions.assertEquals(Character.valueOf('B'), list.get(5));
		Assertions.assertEquals(Character.valueOf('C'), list.get(6));
		Assertions.assertEquals(Character.valueOf('D'), list.get(0));
		Assertions.assertEquals(Character.valueOf('E'), list.get(1));
		Assertions.assertEquals(Character.valueOf('F'), list.get(2));
		Assertions.assertEquals(Character.valueOf('G'), list.get(3));

		list.addAll(7, Arrays.asList('H', 'I'));
		Assertions.assertEquals(9, list.size());
		Assertions.assertEquals(Character.valueOf('A'), list.get(4));
		Assertions.assertEquals(Character.valueOf('B'), list.get(5));
		Assertions.assertEquals(Character.valueOf('C'), list.get(6));
		Assertions.assertEquals(Character.valueOf('D'), list.get(0));
		Assertions.assertEquals(Character.valueOf('E'), list.get(1));
		Assertions.assertEquals(Character.valueOf('F'), list.get(2));
		Assertions.assertEquals(Character.valueOf('G'), list.get(3));
		Assertions.assertEquals(Character.valueOf('H'), list.get(7));
		Assertions.assertEquals(Character.valueOf('I'), list.get(8));

		list.addAll(Arrays.asList('J', 'K'));
		Assertions.assertEquals(11, list.size());
		Assertions.assertEquals(Character.valueOf('A'), list.get(4));
		Assertions.assertEquals(Character.valueOf('B'), list.get(5));
		Assertions.assertEquals(Character.valueOf('C'), list.get(6));
		Assertions.assertEquals(Character.valueOf('D'), list.get(0));
		Assertions.assertEquals(Character.valueOf('E'), list.get(1));
		Assertions.assertEquals(Character.valueOf('F'), list.get(2));
		Assertions.assertEquals(Character.valueOf('G'), list.get(3));
		Assertions.assertEquals(Character.valueOf('H'), list.get(7));
		Assertions.assertEquals(Character.valueOf('I'), list.get(8));
		Assertions.assertEquals(Character.valueOf('J'), list.get(9));
		Assertions.assertEquals(Character.valueOf('K'), list.get(10));

		list.clear();
		Assertions.assertEquals(0, list.size());
	}

	@Test
	void getRemoveTest() {
		Assertions.assertTrue(list.isEmpty());
		initList();
		Assertions.assertFalse(list.isEmpty());

		list.remove((Character) 'C');
		Assertions.assertEquals("ABDEFG", toString(list));

		Assertions.assertEquals('A', list.getFirst().charValue());
		list.removeFirst();
		Assertions.assertEquals("BDEFG", toString(list));

		Assertions.assertEquals('G', list.getLast().charValue());
		list.removeLast();
		Assertions.assertEquals("BDEF", toString(list));

		Assertions.assertEquals('E', list.get(2).charValue());
		Assertions.assertEquals('E', list.remove(2).charValue());
		Assertions.assertEquals("BDF", toString(list));

		list.addAll(Arrays.asList('B', 'C', 'D', 'B', 'A', 'B', 'F', 'D', 'B'));

		Assertions.assertEquals(4, list.indexOf('C'));
		Assertions.assertEquals(1, list.indexOf('D'));
		list.remove((Character) 'D');
		Assertions.assertEquals("BFBCDBABFDB", toString(list));

		Assertions.assertEquals(0, list.indexOf('B'));
		Assertions.assertTrue(list.removeFirstOccurrence('B'));
		Assertions.assertEquals("FBCDBABFDB", toString(list));

		Assertions.assertEquals(9, list.lastIndexOf('B'));
		Assertions.assertTrue(list.removeLastOccurrence('B'));
		Assertions.assertEquals("FBCDBABFD", toString(list));

		Assertions.assertEquals(1, list.indexOf('B'));
		Assertions.assertTrue(list.removeFirstOccurrence('B'));
		Assertions.assertEquals("FCDBABFD", toString(list));

		Assertions.assertEquals(5, list.lastIndexOf('B'));
		Assertions.assertTrue(list.removeLastOccurrence('B'));
		Assertions.assertEquals("FCDBAFD", toString(list));

		Assertions.assertTrue(list.contains('B'));
		Assertions.assertEquals(3, list.lastIndexOf('B'));
		Assertions.assertTrue(list.removeLastOccurrence('B'));
		Assertions.assertEquals("FCDAFD", toString(list));

		Assertions.assertFalse(list.contains('B'));
		Assertions.assertEquals(-1, list.lastIndexOf('B'));
		Assertions.assertFalse(list.removeFirstOccurrence('B'));
		Assertions.assertFalse(list.removeLastOccurrence('B'));
		Assertions.assertEquals("FCDAFD", toString(list));

		Assertions.assertEquals('F', list.remove().charValue());

		list.add(4, null);
		Assertions.assertEquals(4, list.indexOf(null));
		Assertions.assertEquals(4, list.lastIndexOf(null));
	}

	@Test
	void removeTestEmptyList() {
		Assertions.assertThrows(NoSuchElementException.class, () -> list.remove());
	}

	@Test
	void getFirstTestEmptyList() {
		Assertions.assertThrows(NoSuchElementException.class, () -> list.getFirst());
	}

	@Test
	void removeFirstTestEmptyList() {
		Assertions.assertThrows(NoSuchElementException.class, () -> list.removeFirst());
	}

	@Test
	void getLastTestEmptyList() {
		Assertions.assertThrows(NoSuchElementException.class, () -> list.getLast());
	}

	@Test
	void removeLastTestEmptyList() {
		Assertions.assertThrows(NoSuchElementException.class, () -> list.removeLast());
	}

	@Test
	void retainRemoveAllTest() {
		initList();

		list.removeAll(Arrays.asList('A', 'C', 'G'));
		Assertions.assertEquals("BDEF", toString(list));

		list.retainAll(Arrays.asList('E', 'B'));
		Assertions.assertEquals("BE", toString(list));
	}

	@Test
	void ascendingIteratorTest() {
		initList();
		Iterator<Character> iterator = list.iterator();
		int index = 0;
		while (iterator.hasNext()) {
			Assertions.assertEquals(controlList.get(index++), iterator.next());
		}
		Assertions.assertEquals(controlList.size(), index);

		iterator = list.iterator();
		iterator.next();
		iterator.remove();
		iterator.next();
		iterator.next();
		iterator.remove();
		iterator.next();
		iterator.next();
		iterator.remove();
		iterator.next();
		iterator.next();
		iterator.remove();

		Assertions.assertEquals(controlList.size() - 4, list.size());
		Assertions.assertEquals("BDF", toString(list));
	}

	@Test
	void descendingIteratorTest() {
		initList();
		Iterator<Character> iterator = list.descendingIterator();
		int index = controlList.size();
		while (iterator.hasNext()) {
			Assertions.assertEquals(controlList.get(--index), iterator.next());
		}
		Assertions.assertEquals(0, index);
		iterator = list.descendingIterator();
		iterator.next();
		iterator.remove();
		iterator.next();
		iterator.next();
		iterator.next();
		iterator.remove();
		iterator.next();
		iterator.next();
		iterator.next();
		iterator.remove();

		Assertions.assertEquals(controlList.size() - 3, list.size());
		Assertions.assertEquals("BCEF", toString(list));
	}

	@Test
	void listIteratorTest() {
		initList();
		ListIterator<Character> iterator = list.listIterator();
		StringBuilder string = new StringBuilder(2 * controlList.size());
		while (iterator.hasNext()) {
			string.append(iterator.next());
		}
		while (iterator.hasPrevious()) {
			string.append(iterator.previous());
		}
		Assertions.assertEquals("ABCDEFGFEDCBA", string.toString());

		iterator = list.listIterator(controlList.size());
		string = new StringBuilder(2 * controlList.size());
		Assertions.assertFalse(iterator.hasNext());
		while (iterator.hasPrevious()) {
			string.append(iterator.previous());
		}
		while (iterator.hasNext()) {
			string.append(iterator.next());
		}
		Assertions.assertEquals("GFEDCBABCDEFG", string.toString());

		iterator = list.listIterator(5);
		string = new StringBuilder(2 * controlList.size());
		Assertions.assertTrue(iterator.hasPrevious());
		Assertions.assertTrue(iterator.hasNext());
		while (iterator.hasPrevious()) {
			string.append(iterator.previous());
		}
		while (iterator.hasNext()) {
			string.append(iterator.next());
		}
		Assertions.assertEquals("EDCBABCDEFG", string.toString());

		iterator = list.listIterator(5);
		string = new StringBuilder(2 * controlList.size());
		Assertions.assertTrue(iterator.hasPrevious());
		Assertions.assertTrue(iterator.hasNext());
		while (iterator.hasNext()) {
			string.append(iterator.next());
		}
		while (iterator.hasPrevious()) {
			string.append(iterator.previous());
		}
		Assertions.assertEquals("GFEDCBA", string.toString());

		iterator = list.listIterator(3);
		Assertions.assertTrue(iterator.hasPrevious());
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals(4, iterator.nextIndex());
		Assertions.assertEquals(2, iterator.previousIndex());
		Assertions.assertEquals('E', iterator.next().charValue());

		iterator = list.listIterator(3);
		Assertions.assertTrue(iterator.hasPrevious());
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals(4, iterator.nextIndex());
		Assertions.assertEquals(2, iterator.previousIndex());
		Assertions.assertEquals('C', iterator.previous().charValue());

		iterator = list.listIterator();
		Assertions.assertFalse(iterator.hasPrevious());
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals(1, iterator.nextIndex());
		Assertions.assertEquals(-1, iterator.previousIndex());
		Assertions.assertEquals('A', iterator.next().charValue());

		iterator = list.listIterator(controlList.size());
		Assertions.assertTrue(iterator.hasPrevious());
		Assertions.assertFalse(iterator.hasNext());
		Assertions.assertEquals(controlList.size() + 1, iterator.nextIndex());
		Assertions.assertEquals(controlList.size() - 1, iterator.previousIndex());
		Assertions.assertEquals('G', iterator.previous().charValue());
		iterator.remove();
		Assertions.assertEquals('F', iterator.previous().charValue());
		Assertions.assertEquals('E', iterator.previous().charValue());
		iterator.remove();
		Assertions.assertEquals('D', iterator.previous().charValue());
		Assertions.assertEquals('C', iterator.previous().charValue());
		Assertions.assertEquals('B', iterator.previous().charValue());
		Assertions.assertEquals('A', iterator.previous().charValue());
		iterator.remove();
		Assertions.assertEquals('B', iterator.next().charValue());
		iterator.set('Z');
		Assertions.assertEquals('C', iterator.next().charValue());
		iterator.add('Y');
		Assertions.assertEquals('D', iterator.next().charValue());
		Assertions.assertEquals('F', iterator.next().charValue());
		iterator.add('X');
		Assertions.assertEquals("ZCYDFX", toString(list));

		list.clear();
		iterator = list.listIterator();
		Assertions.assertFalse(iterator.hasPrevious());
		Assertions.assertFalse(iterator.hasNext());
		Assertions.assertEquals(-1, iterator.previousIndex());
		Assertions.assertEquals(1, iterator.nextIndex());
		iterator.add('M');
		Assertions.assertEquals("M", toString(list));
	}

	@Test
	void listIteratorPreviousTestNoSuchElement() {
		initList();
		final ListIterator<Character> iterator = list.listIterator();
		Assertions.assertThrows(NoSuchElementException.class, () -> iterator.previous());
	}

	@Test
	void listIteratorNextTestNoSuchElement() {
		initList();
		final ListIterator<Character> iterator = list.listIterator();
		int i = 0;
		while (iterator.hasNext()) {
			i++;
			iterator.next();
		}
		Assertions.assertEquals(7, i);
		Assertions.assertThrows(NoSuchElementException.class, () -> iterator.next());
	}

	@Test
	void listIteratorSetTestNoSuchElement() {
		initList();
		final ListIterator<Character> iterator = list.listIterator();
		Assertions.assertThrows(IllegalStateException.class, () -> iterator.set('A'));
	}

	@Test
	void listIteratorRemoveTestNoSuchElement() {
		initList();
		final ListIterator<Character> iterator = list.listIterator();
		Assertions.assertThrows(IllegalStateException.class, () -> iterator.remove());
	}

	@Test
	void listIteratorRemoveTestNoMoreElement() {
		initList();
		final ListIterator<Character> iterator = list.listIterator();
		iterator.next();
		iterator.remove();
		Assertions.assertThrows(IllegalStateException.class, () -> iterator.remove());
	}

	@Test
	@SuppressWarnings("unlikely-arg-type")
	void containsTest() {
		initList();
		Assertions.assertTrue(list.contains('A'));
		Assertions.assertTrue(list.contains('D'));
		Assertions.assertTrue(list.contains('G'));
		Assertions.assertFalse(list.contains('Z'));
		Assertions.assertFalse(list.contains(null));
		Assertions.assertFalse(list.contains("A"));
		list.add(null);
		Assertions.assertTrue(list.contains(null));

		initList();
		Assertions.assertTrue(list.containsAll(List.of()));
		Assertions.assertTrue(list.contains('A'));
		Assertions.assertTrue(list.contains('D'));
		Assertions.assertTrue(list.contains('G'));
		Assertions.assertTrue(list.containsAll(Arrays.asList('A', 'B', 'C')));
		Assertions.assertTrue(list.containsAll(Arrays.asList('E', 'F', 'G')));
		Assertions.assertFalse(list.containsAll(Arrays.asList('A', 'B', null)));
		Assertions.assertFalse(list.containsAll(Arrays.asList(null, 'F', 'G')));
		Assertions.assertFalse(list.contains(null));
	}

	@Test
	void toArrayTest() {
		initList();
		Object[] array = list.toArray();
		Assertions.assertEquals('A', array[0]);
		Assertions.assertEquals('C', array[2]);
		Assertions.assertEquals('G', array[6]);

		array = new Character[3];
		array = list.toArray(array);
		Assertions.assertEquals('A', array[0]);
		Assertions.assertEquals('C', array[2]);
		Assertions.assertEquals('G', array[6]);

		array = new Character[8];
		array = list.toArray(array);
		Assertions.assertEquals('A', array[0]);
		Assertions.assertEquals('C', array[2]);
		Assertions.assertEquals('G', array[6]);
	}

	@Test
	void toStringTest() {
		Assertions.assertEquals("[]", list.toString());
		initList();
		Assertions.assertEquals("[A, B, C, D, E, F, G]", list.toString());
	}

	@Test
	void checkIndexTestOK() {
		list.checkPositionIndex(0);
		initList();
		list.checkPositionIndex(0);
		list.checkPositionIndex(3);
		list.checkPositionIndex(7);

		list.checkElementIndex(0);
		list.checkElementIndex(3);
		list.checkElementIndex(6);
	}

	@Test
	void pushPopElementTest() {
		list.push('A');
		list.push('B');
		list.push('C');
		Assertions.assertEquals('C', list.element().charValue());
		Assertions.assertEquals('C', list.pop().charValue());
	}

	@Test
	@SuppressWarnings("unlikely-arg-type")
	void equalsHashcodeTest() {
		List<Character> list2 = new ArrayList<>();
		final Set<Character> set = new HashSet<>();

		Assertions.assertTrue(list.equals(list2));
		Assertions.assertFalse(list.equals(set));

		initList();
		Assertions.assertTrue(list.equals(controlList));
		Assertions.assertFalse(list.equals(list2));

		list2 = new DoublyLinkedList<>(controlList);
		Assertions.assertTrue(list.equals(list2));
		Assertions.assertEquals(list2.hashCode(), list.hashCode());

		list.add(null);
		Assertions.assertFalse(list.equals(list2));
		Assertions.assertNotEquals(list2.hashCode(), list.hashCode());

		list2.add(null);
		Assertions.assertTrue(list.equals(list2));
		Assertions.assertEquals(list2.hashCode(), list.hashCode());

		list.remove(null);
		Assertions.assertFalse(list.equals(list2));
		Assertions.assertNotEquals(list2.hashCode(), list.hashCode());

		list2.remove(null);
		Assertions.assertTrue(list.equals(list2));
		Assertions.assertEquals(list2.hashCode(), list.hashCode());

		list.add('H');
		Assertions.assertFalse(list.equals(list2));
		Assertions.assertNotEquals(list2.hashCode(), list.hashCode());

		list2.add('I');
		Assertions.assertFalse(list.equals(list2));
		Assertions.assertNotEquals(list2.hashCode(), list.hashCode());

		list.remove(Character.valueOf('H'));
		Assertions.assertFalse(list.equals(list2));
		Assertions.assertNotEquals(list2.hashCode(), list.hashCode());

		list2.remove(Character.valueOf('I'));
		Assertions.assertTrue(list.equals(list2));
		Assertions.assertEquals(list2.hashCode(), list.hashCode());
	}

	@Test
	void cloneTest() throws CloneNotSupportedException {
		List<Character> clone = list.clone();
		Assertions.assertTrue(clone != list);
		Assertions.assertTrue(clone.getClass().equals(list.getClass()));
		Assertions.assertEquals(list, clone);

		initList();
		clone = list.clone();
		Assertions.assertTrue(clone != list);
		Assertions.assertTrue(clone.getClass().equals(list.getClass()));
		Assertions.assertEquals(list, clone);
	}

	@Test
	void checkIndexTestNegativePosition() {
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> list.checkPositionIndex(-1));
	}

	@Test
	void checkIndexTestPositionUpperThanZero() {
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> list.checkPositionIndex(1));
	}

	@Test
	void checkIndexTestPositionUpperThanSize() {
		initList();
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> list.checkPositionIndex(8));
	}

	@Test
	void checkIndexTestNegativeElement() {
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> list.checkElementIndex(-1));
	}

	@Test
	void checkIndexTestElementZero() {
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> list.checkElementIndex(0));
	}

	@Test
	void checkIndexTestPositionEqualsToSize() {
		initList();
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> list.checkElementIndex(7));
	}
}
