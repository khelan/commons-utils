package fr.khelan.utils.password.rule;

public final class CharacterSets {
	public static final String ALPHA_LOWERCASE = "abcdefghijklmnopqrstuvwxyz";
	public static final String ALPHA_LOWERCASE_DISAMBIGUATION = "abcdefghijkmnopqrstuvwxyz";
	public static final String ALPHA_UPPERCASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	public static final String ALPHA_UPPERCASE_DISAMBIGUATION = "ABCDEFGHJKLMNPQRSTUVWXYZ";
	public static final String ALPHABETIC = ALPHA_UPPERCASE + ALPHA_LOWERCASE;
	public static final String NUMERIC = "0123456789";
	public static final String ALPHANUMERIC = ALPHABETIC + NUMERIC;
	public static final String NUMERIC_DISAMBIGUATION = "23456789";
	public static final String MINUS = "-";
	public static final String UNDERLINE = "_";
	public static final String SPACE = " ";
	public static final String BRACKET = "{([])}";
	public static final String SPECIAL = "!:/;.,?\\&#%$";
	public static final String SPECIALS = SPECIAL + MINUS + UNDERLINE + BRACKET;
	public static final String ALL_COMMONS = ALPHANUMERIC + SPECIALS;

	private CharacterSets() {
		throw new AssertionError("Constants class that must not be instantiate.");
	}
}
