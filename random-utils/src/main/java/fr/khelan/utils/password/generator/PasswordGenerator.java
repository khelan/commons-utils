package fr.khelan.utils.password.generator;

import java.nio.CharBuffer;
import java.security.SecureRandom;
import java.util.HashSet;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

import fr.khelan.utils.password.generator.utils.PasswordGeneratorUtils;
import fr.khelan.utils.password.rule.CharacterRule;

public class PasswordGenerator {
	private static final int DEFAULT_LENGTH = 12;

	private final Random random;
	private final Set<CharacterRule> rules;
	private final PasswordGeneratorUtils generatorUtils;
	private Integer minLength, maxLength;

	public PasswordGenerator() {
		this(new SecureRandom());
	}

	public PasswordGenerator(final Random random) {
		this.random = random;
		rules = new HashSet<>();

		this.generatorUtils = new PasswordGeneratorUtils();
	}

	public PasswordGenerator setMinLength(final Integer minLength) {
		this.minLength = minLength;
		return this;
	}

	public PasswordGenerator setMaxLength(final Integer maxLength) {
		this.maxLength = maxLength;
		return this;
	}

	public PasswordGenerator addRule(final CharacterRule rule) {
		rules.add(rule);
		return this;
	}

	public String generatePassword() {
		int length = defineLength();

		final StringBuilder possibleChars = new StringBuilder();
		final CharBuffer buffer = CharBuffer.allocate(length);
		for (final CharacterRule rule : rules) {
			processRule(buffer, rule).ifPresent(possibleChars::append);
		}
		if (buffer.position() < length) {
			generatorUtils.fillWithRandomCharacters(possibleChars, length - buffer.position(), buffer,
													random);
		}

		buffer.flip();
		randomizeBuffer(buffer);

		return buffer.toString();
	}

	private int defineLength() {
		int length;
		if (minLength == null) {
			if (maxLength == null) {
				length = DEFAULT_LENGTH;
			} else {
				length = maxLength;
			}
		} else if (maxLength == null) {
			length = minLength;
		} else {
			length = random.nextInt(maxLength - minLength + 1) + minLength;
		}
		return length;
	}

	private Optional<String> processRule(final CharBuffer buffer, final CharacterRule rule) {
		Optional<String> possibleChars;
		rule.randomCharacters(buffer, random);
		if (rule.hasNoMaximumNumberOfChars()) {
			possibleChars = Optional.of(rule.getValidCharacters());
		} else {
			possibleChars = Optional.empty();
		}
		return possibleChars;
	}

	private void randomizeBuffer(final CharBuffer buffer) {
		char targetChar;
		int targetPosition;
		for (int currentPosition = buffer.position(); currentPosition < buffer.limit(); currentPosition++) {
			targetPosition = random.nextInt(buffer.length());
			targetChar = buffer.get(targetPosition);
			buffer.put(targetPosition, buffer.get(currentPosition));
			buffer.put(currentPosition, targetChar);
		}
	}
}
