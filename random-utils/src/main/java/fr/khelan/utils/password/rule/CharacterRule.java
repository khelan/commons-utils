package fr.khelan.utils.password.rule;

import java.util.Random;

import fr.khelan.utils.password.generator.utils.PasswordGeneratorUtils;

public class CharacterRule implements Rule {
	private final Integer min;
	private final Integer max;
	private final String validCharacters;

	private final PasswordGeneratorUtils generatorUtils;

	public CharacterRule(final String validCharacters) {
		this(validCharacters, null, null);
	}

	public CharacterRule(final String validCharacters, final Integer min) {
		this(validCharacters, min, null);
	}

	public CharacterRule(final String validCharacters, final Integer min, final Integer max) {
		this.validCharacters = validCharacters;
		this.min = min;
		this.max = max;

		this.generatorUtils = new PasswordGeneratorUtils();
	}

	public String getValidCharacters() {
		return validCharacters;
	}

	public boolean hasNoMaximumNumberOfChars() {
		return max == null;
	}

	public void randomCharacters(final Appendable append, final Random random) {
		int length;
		if (min == null) {
			length = 0;
		} else if (max == null) {
			length = min;
		} else {
			length = random.nextInt(max - min + 1) + min;
		}
		if (length > 0) {
			generatorUtils.fillWithRandomCharacters(validCharacters, length, append, random);
		}
	}
}
