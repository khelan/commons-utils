package fr.khelan.utils.password.generator.exception;

public class WritePasswordException extends RuntimeException {
	public WritePasswordException(String message) {
		super(message);
	}

	public WritePasswordException(String message, Exception e) {
		super(message, e);
	}
}
