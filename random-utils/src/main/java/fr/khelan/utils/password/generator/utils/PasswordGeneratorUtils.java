package fr.khelan.utils.password.generator.utils;

import java.io.IOException;
import java.util.Random;

import fr.khelan.utils.password.generator.exception.WritePasswordException;

public class PasswordGeneratorUtils {
	public void fillWithRandomCharacters(final CharSequence chars, final int length, final Appendable append,
			final Random random) {
		for (int i = 0; i < length; i++) {
			try {
				append.append(chars.charAt(random.nextInt(chars.length())));
			} catch (final IOException e) {
				throw new WritePasswordException("Error appending char.", e);
			}
		}
	}
}
