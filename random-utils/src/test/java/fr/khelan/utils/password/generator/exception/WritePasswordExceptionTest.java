package fr.khelan.utils.password.generator.exception;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class WritePasswordExceptionTest {
	private final String exceptionMessage = "Exception Message";

	private final Exception sourceException = new RuntimeException();

	@Test
	void writePasswordExceptionTest() {
		WritePasswordException wpe = new WritePasswordException(exceptionMessage);
		Assertions.assertEquals(exceptionMessage, wpe.getMessage());

		wpe = new WritePasswordException(exceptionMessage, sourceException);
		Assertions.assertEquals(exceptionMessage, wpe.getMessage());
		Assertions.assertEquals(sourceException, wpe.getCause());
	}
}
