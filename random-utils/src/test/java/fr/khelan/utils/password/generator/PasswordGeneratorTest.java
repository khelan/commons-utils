package fr.khelan.utils.password.generator;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import fr.khelan.utils.password.generator.utils.PasswordGeneratorUtils;
import fr.khelan.utils.password.rule.CharacterRule;
import fr.khelan.utils.password.rule.CharacterSets;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class PasswordGeneratorTest {
	@Mock
	private PasswordGeneratorUtils generatorUtils;

	private PasswordGenerator generator;

	@Test
	void passwordGeneratorTestLength() {
		generator = new PasswordGenerator();
		generator.addRule(new CharacterRule(CharacterSets.ALPHABETIC, 1));

		Assertions.assertEquals(12, generator.generatePassword().length());

		generator.setMinLength(2);
		Assertions.assertTrue(generator.generatePassword().length() >= 2);

		generator.setMaxLength(15);
		Assertions.assertTrue(generator.generatePassword().length() >= 2);
		Assertions.assertTrue(generator.generatePassword().length() <= 15);

		generator = new PasswordGenerator();
		generator.addRule(new CharacterRule(
				CharacterSets.SPECIAL + CharacterSets.MINUS + CharacterSets.UNDERLINE + CharacterSets.BRACKET, 1));
		generator.setMaxLength(6);
		Assertions.assertTrue(generator.generatePassword().length() <= 6);
		generator.setMinLength(2);
		Assertions.assertTrue(generator.generatePassword().length() >= 2);
		Assertions.assertTrue(generator.generatePassword().length() <= 6);
	}

	@Test
	void passwordGeneratorTestComplexPassword() {
		generator = new PasswordGenerator();
		generator.setMinLength(8);
		generator.setMaxLength(12);
		generator.addRule(new CharacterRule(CharacterSets.ALPHA_LOWERCASE, 3));
		generator.addRule(new CharacterRule(CharacterSets.ALPHA_UPPERCASE, 3));
		generator.addRule(new CharacterRule(CharacterSets.NUMERIC, 2, 2));

		String password = generator.generatePassword();

		AtomicInteger lowercase = new AtomicInteger();
		AtomicInteger uppercase = new AtomicInteger();
		AtomicInteger numeric = new AtomicInteger();
		password.chars().mapToObj(c -> String.valueOf((char) c)).forEach(c -> {
			if (CharacterSets.ALPHA_LOWERCASE.contains(c)) {
				lowercase.getAndIncrement();
			}
			if (CharacterSets.ALPHA_UPPERCASE.contains(c)) {
				uppercase.getAndIncrement();
			}
			if (CharacterSets.NUMERIC.contains(c)) {
				numeric.getAndIncrement();
			}
		});
		System.out.println(password);
		Assertions.assertTrue(lowercase.get() >= 3);
		Assertions.assertTrue(uppercase.get() >= 3);
		Assertions.assertEquals(2, numeric.get());
	}

	@Test
	void passwordGeneratorTestComplexDisambiguationPassword() {
		generator = new PasswordGenerator();
		generator.setMinLength(8);
		generator.setMaxLength(12);
		generator.addRule(new CharacterRule(CharacterSets.ALPHA_LOWERCASE_DISAMBIGUATION, 3));
		generator.addRule(new CharacterRule(CharacterSets.ALPHA_UPPERCASE_DISAMBIGUATION, 3));
		generator.addRule(new CharacterRule(CharacterSets.NUMERIC_DISAMBIGUATION, 2, 2));

		String password = generator.generatePassword();

		AtomicInteger lowercase = new AtomicInteger();
		AtomicInteger uppercase = new AtomicInteger();
		AtomicInteger numeric = new AtomicInteger();
		AtomicBoolean hasDisambiguationChar = new AtomicBoolean(false);
		password.chars().mapToObj(c -> String.valueOf((char) c)).forEach(c -> {
			if (CharacterSets.ALPHA_LOWERCASE_DISAMBIGUATION.contains(c)) {
				lowercase.getAndIncrement();
			}
			if (CharacterSets.ALPHA_UPPERCASE_DISAMBIGUATION.contains(c)) {
				uppercase.getAndIncrement();
			}
			if (CharacterSets.NUMERIC_DISAMBIGUATION.contains(c)) {
				numeric.getAndIncrement();
			}
			if ("0OIl1".contains(c)) {
				hasDisambiguationChar.set(true);
			}
		});

		System.out.println(password);
		Assertions.assertTrue(lowercase.get() >= 3);
		Assertions.assertTrue(uppercase.get() >= 3);
		Assertions.assertEquals(2, numeric.get());
		Assertions.assertFalse(hasDisambiguationChar.get());
	}
}
