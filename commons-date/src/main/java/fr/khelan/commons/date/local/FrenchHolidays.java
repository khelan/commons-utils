package fr.khelan.commons.date.local;

import java.time.LocalDate;
import java.util.List;

import fr.khelan.commons.date.Holidays;
import fr.khelan.commons.date.holiday.HolidayDate;

public class FrenchHolidays implements LocalHolidays {
	private static final List<HolidayDate> holidays = List.of(
			Holidays.NewYear,
			Holidays.Easter,
			Holidays.EasterMonday,
			Holidays.LabourDay,
			Holidays.VictoryDay,
			Holidays.Ascension,
			Holidays.Pentecost,
			Holidays.PentecostMonday,
			Holidays.FrenchNationalDay,
			Holidays.Assumption,
			Holidays.AllSaints,
			Holidays.RemembranceDay,
			Holidays.Christmas);

	@Override
	public boolean isHoliday(final LocalDate date) {
		return holidays.stream().anyMatch(h -> h.isHoliday(date));
	}

	@Override
	public List<HolidayDate> holidays() {
		return holidays;
	}
}
