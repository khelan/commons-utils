package fr.khelan.commons.date.local;

import java.time.LocalDate;
import java.util.List;

import fr.khelan.commons.date.Holiday;
import fr.khelan.commons.date.holiday.HolidayDate;

public interface LocalHolidays extends Holiday {
	List<HolidayDate> holidays();

	default List<LocalDate> holidays(Integer year) {
		return holidays().stream().map(h -> h.holidayDate(year)).toList();
	}
}
