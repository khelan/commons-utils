package fr.khelan.commons.date;

import java.time.LocalDate;

public interface Holiday {
	boolean isHoliday(LocalDate date);
}
