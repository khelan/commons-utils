package fr.khelan.commons.date.holiday;

import java.time.LocalDate;
import java.time.Month;

public class FixedYearlyHoliday implements HolidayDate {
	private final Month month;

	private final Integer dayOfMonth;

	public FixedYearlyHoliday(Integer dayOfMonth, Month month) {
		this.dayOfMonth = dayOfMonth;
		this.month = month;
	}

	@Override
	public boolean isHoliday(final LocalDate date) {
		return this.month.equals(date.getMonth()) && this.dayOfMonth == date.getDayOfMonth();
	}

	@Override
	public LocalDate holidayDate(final Integer year) {
		return LocalDate.of(year, month, dayOfMonth);
	}
}
