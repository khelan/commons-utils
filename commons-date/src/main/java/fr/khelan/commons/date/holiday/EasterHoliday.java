package fr.khelan.commons.date.holiday;

import java.time.LocalDate;

public class EasterHoliday extends EasterBasedHoliday {
	public EasterHoliday() {
		super(0);
	}

	@Override
	public LocalDate holidayDate(final Integer year) {
		return easterDate(year);
	}
}
