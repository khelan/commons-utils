package fr.khelan.commons.date;

import java.time.Month;

import fr.khelan.commons.date.holiday.EasterBasedHoliday;
import fr.khelan.commons.date.holiday.EasterHoliday;
import fr.khelan.commons.date.holiday.FixedYearlyHoliday;
import fr.khelan.commons.date.holiday.HolidayDate;

public final class Holidays {
	public static final HolidayDate NewYear = new FixedYearlyHoliday(1, Month.JANUARY);

	public static final HolidayDate Easter = new EasterHoliday();

	public static final HolidayDate EasterMonday = new EasterBasedHoliday(1);

	public static final HolidayDate LabourDay = new FixedYearlyHoliday(1, Month.MAY);

	public static final HolidayDate VictoryDay = new FixedYearlyHoliday(8, Month.MAY);

	public static final HolidayDate Ascension = new EasterBasedHoliday(39);

	public static final HolidayDate Pentecost = new EasterBasedHoliday(49);

	public static final HolidayDate PentecostMonday = new EasterBasedHoliday(50);

	public static final HolidayDate FrenchNationalDay = new FixedYearlyHoliday(14, Month.JULY);

	public static final HolidayDate Assumption = new FixedYearlyHoliday(15, Month.AUGUST);

	public static final HolidayDate AllSaints = new FixedYearlyHoliday(1, Month.NOVEMBER);

	public static final HolidayDate RemembranceDay = new FixedYearlyHoliday(11, Month.NOVEMBER);

	public static final HolidayDate Christmas = new FixedYearlyHoliday(25, Month.DECEMBER);

	private Holidays() {
	}
}
