package fr.khelan.commons.date.holiday;

import java.time.LocalDate;

import fr.khelan.commons.date.Holiday;

public interface HolidayDate extends Holiday {

	LocalDate holidayDate(Integer year);
}
