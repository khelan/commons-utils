package fr.khelan.commons.date.holiday;

import java.time.LocalDate;

public class EasterBasedHoliday extends CalculatedHoliday {
	private final Integer daysFromEasterDate;

	public EasterBasedHoliday(Integer daysFromEasterDate) {
		this.daysFromEasterDate = daysFromEasterDate;
	}

	protected LocalDate easterDate(Integer year) {
		return meeusJonesButcherAlgorithm(year);
	}

	protected LocalDate meeusJonesButcherAlgorithm(Integer year) {
		int a = year % 19;
		int b = year / 100;
		int c = year % 100;
		int d = b / 4;
		int e = b % 4;
		int g = (8 * b + 13) / 25;
		int h = (19 * a + b - d - g + 15) % 30;
		int i = c / 4;
		int k = c % 4;
		int l = (32 + 2 * e + 2 * i - h - k) % 7;
		int m = (a + 11 * h + 19 * l) / 433;
		int n = (h + l - 7 * m + 90) / 25;
		int p = (h + l - 7 * m + 33 * n + 19) % 32;

		return LocalDate.of(year, n, p);
	}

	@Override
	public LocalDate holidayDate(final Integer year) {
		return easterDate(year).plusDays(daysFromEasterDate.longValue());
	}
}
