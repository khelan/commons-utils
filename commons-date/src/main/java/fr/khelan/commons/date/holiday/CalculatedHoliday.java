package fr.khelan.commons.date.holiday;

import java.time.LocalDate;

public abstract class CalculatedHoliday implements HolidayDate {
	@Override
	public boolean isHoliday(final LocalDate date) {
		return holidayDate(date.getYear()).equals(date);
	}
}
