package fr.khelan.commons.date.holiday;

import java.time.LocalDate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import fr.khelan.commons.date.Holidays;

class EasterHolidayTest {
	private final HolidayDate holiday = Holidays.Easter;

	@Test
	void easterDateTest() {
		Assertions.assertEquals(LocalDate.of(1901, 4, 7),
								holiday.holidayDate(1901));
		Assertions.assertEquals(LocalDate.of(1909, 4, 11),
								holiday.holidayDate(1909));
		Assertions.assertEquals(LocalDate.of(1912, 4, 7),
								holiday.holidayDate(1912));
		Assertions.assertEquals(LocalDate.of(1929, 3, 31),
								holiday.holidayDate(1929));
		Assertions.assertEquals(LocalDate.of(1931, 4, 5),
								holiday.holidayDate(1931));
		Assertions.assertEquals(LocalDate.of(1943, 4, 25),
								holiday.holidayDate(1943));
		Assertions.assertEquals(LocalDate.of(1962, 4, 22),
								holiday.holidayDate(1962));
		Assertions.assertEquals(LocalDate.of(1967, 3, 26),
								holiday.holidayDate(1967));
		Assertions.assertEquals(LocalDate.of(2000, 4, 23),
								holiday.holidayDate(2000));
		Assertions.assertEquals(LocalDate.of(2008, 3, 23),
								holiday.holidayDate(2008));
		Assertions.assertEquals(LocalDate.of(2024, 3, 31),
								holiday.holidayDate(2024));
		Assertions.assertEquals(LocalDate.of(2046, 3, 25),
								holiday.holidayDate(2046));
		Assertions.assertEquals(LocalDate.of(2050, 4, 10),
								holiday.holidayDate(2050));
	}

	@Test
	void isHolidayTest() {
		Assertions.assertTrue(holiday.isHoliday(LocalDate.of(1901, 4, 7)));
		Assertions.assertTrue(holiday.isHoliday(LocalDate.of(1909, 4, 11)));
		Assertions.assertTrue(holiday.isHoliday(LocalDate.of(1912, 4, 7)));
		Assertions.assertTrue(holiday.isHoliday(LocalDate.of(1929, 3, 31)));
		Assertions.assertTrue(holiday.isHoliday(LocalDate.of(1931, 4, 5)));
		Assertions.assertTrue(holiday.isHoliday(LocalDate.of(1943, 4, 25)));
		Assertions.assertTrue(holiday.isHoliday(LocalDate.of(1962, 4, 22)));
		Assertions.assertTrue(holiday.isHoliday(LocalDate.of(1967, 3, 26)));
		Assertions.assertTrue(holiday.isHoliday(LocalDate.of(2000, 4, 23)));
		Assertions.assertTrue(holiday.isHoliday(LocalDate.of(2008, 3, 23)));
		Assertions.assertTrue(holiday.isHoliday(LocalDate.of(2024, 3, 31)));
		Assertions.assertTrue(holiday.isHoliday(LocalDate.of(2046, 3, 25)));
		Assertions.assertTrue(holiday.isHoliday(LocalDate.of(2050, 4, 10)));
	}

	@Test
	void isNotHolidayTest() {
		Assertions.assertFalse(holiday.isHoliday(LocalDate.of(2024, 3, 22)));
		Assertions.assertFalse(holiday.isHoliday(LocalDate.of(2024, 3, 30)));
		Assertions.assertFalse(holiday.isHoliday(LocalDate.of(2024, 4, 1)));
		Assertions.assertFalse(holiday.isHoliday(LocalDate.of(2024, 4, 2)));
		Assertions.assertFalse(holiday.isHoliday(LocalDate.of(2024, 4, 11)));
		Assertions.assertFalse(holiday.isHoliday(LocalDate.of(2024, 3, 22)));
	}
}
