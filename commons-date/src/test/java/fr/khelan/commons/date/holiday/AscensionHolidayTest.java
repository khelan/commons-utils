package fr.khelan.commons.date.holiday;

import java.time.DayOfWeek;
import java.time.LocalDate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import fr.khelan.commons.date.Holidays;

class AscensionHolidayTest {
	private final HolidayDate holiday = Holidays.Ascension;

	@Test
	void easterDateTest() {
		LocalDate ascensionHolidayDate;

		ascensionHolidayDate = holiday.holidayDate(1901);
		Assertions.assertEquals(LocalDate.of(1901, 5, 16), ascensionHolidayDate);
		Assertions.assertEquals(DayOfWeek.THURSDAY, ascensionHolidayDate.getDayOfWeek());

		ascensionHolidayDate = holiday.holidayDate(1943);
		Assertions.assertEquals(LocalDate.of(1943, 6, 3), ascensionHolidayDate);
		Assertions.assertEquals(DayOfWeek.THURSDAY, ascensionHolidayDate.getDayOfWeek());

		ascensionHolidayDate = holiday.holidayDate(2008);
		Assertions.assertEquals(LocalDate.of(2008, 5, 1), ascensionHolidayDate);
		Assertions.assertEquals(DayOfWeek.THURSDAY, ascensionHolidayDate.getDayOfWeek());

		ascensionHolidayDate = holiday.holidayDate(2024);
		Assertions.assertEquals(LocalDate.of(2024, 5, 9), ascensionHolidayDate);
		Assertions.assertEquals(DayOfWeek.THURSDAY, ascensionHolidayDate.getDayOfWeek());

		ascensionHolidayDate = holiday.holidayDate(2046);
		Assertions.assertEquals(LocalDate.of(2046, 5, 3), ascensionHolidayDate);
		Assertions.assertEquals(DayOfWeek.THURSDAY, ascensionHolidayDate.getDayOfWeek());

		ascensionHolidayDate = holiday.holidayDate(2050);
		Assertions.assertEquals(LocalDate.of(2050, 5, 19), ascensionHolidayDate);
		Assertions.assertEquals(DayOfWeek.THURSDAY, ascensionHolidayDate.getDayOfWeek());
	}
}
