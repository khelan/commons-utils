package fr.khelan.commons.date.local;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class FrenchHolidaysTest {
	private final LocalHolidays holidays = new FrenchHolidays();

	@Test
	void isHolidayTest() {
		Assertions.assertTrue(holidays.isHoliday(LocalDate.of(2024, 1, 1)));
		Assertions.assertTrue(holidays.isHoliday(LocalDate.of(2024, 3, 31)));
		Assertions.assertTrue(holidays.isHoliday(LocalDate.of(2024, 4, 1)));
		Assertions.assertTrue(holidays.isHoliday(LocalDate.of(2024, 5, 1)));
		Assertions.assertTrue(holidays.isHoliday(LocalDate.of(2024, 5, 8)));
		Assertions.assertTrue(holidays.isHoliday(LocalDate.of(2024, 5, 9)));
		Assertions.assertTrue(holidays.isHoliday(LocalDate.of(2024, 5, 19)));
		Assertions.assertTrue(holidays.isHoliday(LocalDate.of(2024, 5, 20)));
		Assertions.assertTrue(holidays.isHoliday(LocalDate.of(2024, 7, 14)));
		Assertions.assertTrue(holidays.isHoliday(LocalDate.of(2024, 8, 15)));
		Assertions.assertTrue(holidays.isHoliday(LocalDate.of(2024, 11, 1)));
		Assertions.assertTrue(holidays.isHoliday(LocalDate.of(2024, 11, 11)));
		Assertions.assertTrue(holidays.isHoliday(LocalDate.of(2024, 12, 25)));
	}

	@Test
	void isNotHolidayTest() {
		Assertions.assertFalse(holidays.isHoliday(LocalDate.of(2024, 1, 2)));
		Assertions.assertFalse(holidays.isHoliday(LocalDate.of(2024, 3, 30)));
		Assertions.assertFalse(holidays.isHoliday(LocalDate.of(2024, 5, 2)));
		Assertions.assertFalse(holidays.isHoliday(LocalDate.of(2024, 5, 10)));
		Assertions.assertFalse(holidays.isHoliday(LocalDate.of(2024, 12, 30)));
	}

	@Test
	void holidaysTest() {
		List<LocalDate> holidaysList = holidays.holidays(2024);

		Assertions.assertEquals(13, holidaysList.size());
		Assertions.assertTrue(holidaysList.contains(LocalDate.of(2024, 1, 1)));
		Assertions.assertTrue(holidaysList.contains(LocalDate.of(2024, 3, 31)));
		Assertions.assertTrue(holidaysList.contains(LocalDate.of(2024, 4, 1)));
		Assertions.assertTrue(holidaysList.contains(LocalDate.of(2024, 5, 1)));
		Assertions.assertTrue(holidaysList.contains(LocalDate.of(2024, 5, 8)));
		Assertions.assertTrue(holidaysList.contains(LocalDate.of(2024, 5, 9)));
		Assertions.assertTrue(holidaysList.contains(LocalDate.of(2024, 5, 19)));
		Assertions.assertTrue(holidaysList.contains(LocalDate.of(2024, 5, 20)));
		Assertions.assertTrue(holidaysList.contains(LocalDate.of(2024, 7, 14)));
		Assertions.assertTrue(holidaysList.contains(LocalDate.of(2024, 8, 15)));
		Assertions.assertTrue(holidaysList.contains(LocalDate.of(2024, 11, 1)));
		Assertions.assertTrue(holidaysList.contains(LocalDate.of(2024, 11, 11)));
		Assertions.assertTrue(holidaysList.contains(LocalDate.of(2024, 12, 25)));
	}
}
