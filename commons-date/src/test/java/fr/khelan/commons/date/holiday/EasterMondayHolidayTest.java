package fr.khelan.commons.date.holiday;

import java.time.LocalDate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import fr.khelan.commons.date.Holidays;

class EasterMondayHolidayTest {
	private final HolidayDate holiday = Holidays.EasterMonday;

	@Test
	void easterDateTest() {
		Assertions.assertEquals(LocalDate.of(1901, 4, 8),
								holiday.holidayDate(1901));
		Assertions.assertEquals(LocalDate.of(1909, 4, 12),
								holiday.holidayDate(1909));
		Assertions.assertEquals(LocalDate.of(1912, 4, 8),
								holiday.holidayDate(1912));
		Assertions.assertEquals(LocalDate.of(1929, 4, 1),
								holiday.holidayDate(1929));
		Assertions.assertEquals(LocalDate.of(1931, 4, 6),
								holiday.holidayDate(1931));
		Assertions.assertEquals(LocalDate.of(1943, 4, 26),
								holiday.holidayDate(1943));
		Assertions.assertEquals(LocalDate.of(1962, 4, 23),
								holiday.holidayDate(1962));
		Assertions.assertEquals(LocalDate.of(1967, 3, 27),
								holiday.holidayDate(1967));
		Assertions.assertEquals(LocalDate.of(2000, 4, 24),
								holiday.holidayDate(2000));
		Assertions.assertEquals(LocalDate.of(2008, 3, 24),
								holiday.holidayDate(2008));
		Assertions.assertEquals(LocalDate.of(2024, 4, 1),
								holiday.holidayDate(2024));
		Assertions.assertEquals(LocalDate.of(2046, 3, 26),
								holiday.holidayDate(2046));
		Assertions.assertEquals(LocalDate.of(2050, 4, 11),
								holiday.holidayDate(2050));
	}
}
