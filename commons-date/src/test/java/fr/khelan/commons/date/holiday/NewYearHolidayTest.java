package fr.khelan.commons.date.holiday;

import java.time.LocalDate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import fr.khelan.commons.date.Holidays;

class NewYearHolidayTest {
	private final HolidayDate holiday = Holidays.NewYear;

	@Test
	void newYearDateTest() {
		Assertions.assertEquals(LocalDate.of(2024, 1, 1),
								holiday.holidayDate(2024));
	}

	@Test
	void isHolidayTest() {
		Assertions.assertTrue(holiday.isHoliday(LocalDate.of(2024, 1, 1)));
		Assertions.assertTrue(holiday.isHoliday(LocalDate.of(2025, 1, 1)));
	}

	@Test
	void isNotHolidayTest() {
		Assertions.assertFalse(holiday.isHoliday(LocalDate.of(2023, 12, 31)));
		Assertions.assertFalse(holiday.isHoliday(LocalDate.of(2024, 1, 2)));
		Assertions.assertFalse(holiday.isHoliday(LocalDate.of(2024, 2, 1)));
	}
}
