package fr.khelan.utils.sql;

import java.util.Arrays;

import fr.khelan.utils.sql.impl.QueryGeneratorImpl;
import fr.khelan.utils.sql.type.Aggregation;
import fr.khelan.utils.sql.type.Comparator;
import fr.khelan.utils.sql.type.Operator;
import fr.khelan.utils.sql.type.Order;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class QueryGeneratorTest {

	@Test
	void simpleQueryTest() {
		final String query = new QueryGeneratorImpl("table")
				.addField("champ1")
				.addField("champ2", Aggregation.MAXIMUM)
				.addField("champ3", "valeur3")
				.createSelectRequest()
				.toString();
		Assertions.assertEquals("SELECT champ1,MAX(champ2) AS champ2,'valeur3' AS champ3 FROM table", query);
	}

	@Test
	void simpleWhereQueryTest() {
		final String query = new QueryGeneratorImpl("table")
				.addField("champ1")
				.addField("champ2", Aggregation.MAXIMUM)
				.addField("champ3", "valeur3")
				.createSelectRequest()
				.where()
				.addFieldClause("champ4", "'valeur4'")
				.endWhere()
				.toString();
		Assertions.assertEquals(
				"SELECT champ1,MAX(champ2) AS champ2,'valeur3' AS champ3 FROM table WHERE champ4='valeur4'", query);
	}

	@Test
	void complexWhereQueryTest() {
		final String query = new QueryGeneratorImpl("table")
				.addField("champ1")
				.addField("champ2", Aggregation.MAXIMUM)
				.addField("champ3", "valeur3")
				.createSelectRequest()
				.where(Operator.AND)
				.startSubClause(Operator.OR)
				.addFieldClause("champ5", Comparator.NOT_NULL)
				.addFieldClause("champ5")
				.endSubClause()
				.addFieldClause("champ6")
				.addFieldClause("champ7", Comparator.IN)
				.addFieldClause("champ8", Comparator.LOWER_OR_EQUAL, "8")
				.endWhere()
				.toString();
		Assertions.assertEquals(
				"SELECT champ1,MAX(champ2) AS champ2,'valeur3' AS champ3 FROM table WHERE (champ5 IS NOT NULL OR champ5=:champ5) AND champ6=:champ6 AND champ7 IN (:champ7) AND champ8<=8",
				query);
	}

	@Test
	void groupByOrderByQueryTest() {
		final String query = new QueryGeneratorImpl("table")
				.addField("champ1")
				.addField("champ2", Aggregation.MAXIMUM)
				.addField("champ3", "valeur3")
				.createSelectRequest()
				.groupBy("champ1")
				.groupBy("champ3")
				.orderBy("champ3", Order.DESCENDING)
				.toString();
		Assertions.assertEquals(
				"SELECT champ1,MAX(champ2) AS champ2,'valeur3' AS champ3 FROM table GROUP BY champ1,champ3 ORDER BY champ3 DESC",
				query);
	}

	@Test
	void insertSimpleTest() {
		final String query = new QueryGeneratorImpl("table")
				.addField("champ1")
				.addField("champ2")
				.addField("champ3")
				.createInsertRequest()
				.toString();
		Assertions.assertEquals("INSERT INTO table(champ1,champ2,champ3) VALUES (:champ1,:champ2,:champ3)", query);
	}

	@Test
	void insertSubstituteTest() {
		final String query = new QueryGeneratorImpl("table")
				.addField("champ1")
				.addField("champ2")
				.addField("champ3")
				.addField("champ4")
				.addField("champ5")
				.createInsertRequest(Arrays.asList("champ2", "champ3", "champ4"),
									 Arrays.asList("'TEST'", "1", "SYSDATE"))
				.toString();
		Assertions.assertEquals(
				"INSERT INTO table(champ1,champ2,champ3,champ4,champ5) VALUES (:champ1,'TEST',1,SYSDATE,:champ5)",
				query);
	}

	@Test
	void updateSimpleTest() {
		final String query = new QueryGeneratorImpl("table")
				.addField("champ1")
				.addField("champ2")
				.addField("champ3")
				.createUpdateRequest()
				.where()
				.addFieldClause("champ4", "'valeur4'")
				.endWhere()
				.toString();
		Assertions.assertEquals("UPDATE table SET champ1=:champ1,champ2=:champ2,champ3=:champ3 WHERE champ4='valeur4'",
								query);
	}

	@Test
	void updateSubstituteTest() {
		final String query = new QueryGeneratorImpl("table")
				.addField("champ1")
				.addField("champ2")
				.addField("champ3")
				.addField("champ4")
				.addField("champ5")
				.createUpdateRequest(Arrays.asList("champ2", "champ3", "champ4"),
									 Arrays.asList("'TEST'", "1", "SYSDATE"))
				.where()
				.addFieldClause("champ6", "'valeur6'")
				.endWhere()
				.toString();
		Assertions.assertEquals(
				"UPDATE table SET champ1=:champ1,champ2='TEST',champ3=1,champ4=SYSDATE,champ5=:champ5 WHERE champ6='valeur6'",
				query);
	}

	@Test
	void deleteTest() {
		final String query = new QueryGeneratorImpl("table")
				.createDeleteRequest()
				.where(Operator.AND)
				.addFieldClause("champ1", "'valeur1'")
				.addFieldClause("champ2", "2")
				.endWhere()
				.toString();
		Assertions.assertEquals("DELETE FROM table WHERE champ1='valeur1' AND champ2=2", query);
	}
}
