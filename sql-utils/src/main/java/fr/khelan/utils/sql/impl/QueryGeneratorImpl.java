package fr.khelan.utils.sql.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import fr.khelan.commons.utils.StringBuilderSeparator;
import fr.khelan.commons.utils.UtilsFactory;
import fr.khelan.utils.sql.ParameterValue;
import fr.khelan.utils.sql.QueryGenerator;
import fr.khelan.utils.sql.WhereClause;
import fr.khelan.utils.sql.WhereGenerator;
import fr.khelan.utils.sql.type.Aggregation;
import fr.khelan.utils.sql.type.Operator;
import fr.khelan.utils.sql.type.Order;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

public class QueryGeneratorImpl implements QueryGenerator, WhereGenerator {
	private final String table;
	private final List<String> fields;
	private final List<String> groupFields;
	private final List<String> orderFields;
	private final UtilsFactory utilsFactory = new UtilsFactory();
	private final ParameterValue parameterValue = new NamedParameterValue();
	private WhereClause whereClause;
	private StringBuilder request;

	/**
	 * Initialise une requête sur la table.
	 *
	 * @param table
	 * 		La table de la requête.
	 */
	public QueryGeneratorImpl(final String table) {
		this(table, null);
	}

	/**
	 * Initialise une requête sur la table avec la liste de champs.
	 *
	 * @param table
	 * 		La table de la requête.
	 * @param fields
	 * 		La liste des champs de la requête.
	 */
	public QueryGeneratorImpl(final String table, final Collection<String> fields) {
		this.table = table;
		if (fields != null) {
			this.fields = new ArrayList<>(fields);
		} else {
			this.fields = new ArrayList<>();
		}
		groupFields = new ArrayList<>();
		orderFields = new ArrayList<>();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public QueryGenerator addField(final String field) {
		fields.add(field);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public QueryGenerator addField(final String field, final Aggregation aggregation) {
		String select;
		if (aggregation != null) {
			select = aggregation.getAggregationKeyword() + "(" + field + ") AS " + field;
		} else {
			select = field;
		}
		fields.add(select);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public QueryGenerator addField(final String field, final String value) {
		String select;
		if (value == null) {
			select = "NULL AS " + field;
		} else if (StringUtils.isEmpty(value)) {
			select = "'' AS " + field;
		} else {
			select = "'" + value + "' AS " + field;
		}
		fields.add(select);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public WhereGenerator createSelectRequest() {
		if (!fields.isEmpty()) {
			final StringBuilderSeparator fieldsGenerator = utilsFactory.createStringBuilderSeparator(",");
			request = new StringBuilder(100)
					.append("SELECT ").append(fieldsGenerator.append(fields))
					.append(" FROM ").append(table);
		} else {
			throw new IllegalStateException("The fields must be defined.");
		}
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public WhereGenerator createInsertRequest() {
		return createInsertRequest(null, null);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public WhereGenerator createInsertRequest(final List<String> substitutedFields,
			final List<String> substitutionValues) {
		request = new StringBuilder(100)
				.append("INSERT INTO ").append(table)
				.append("(").append(utilsFactory.createStringBuilderSeparator(",").append(fields))
				.append(") VALUES (");

		final StringBuilderSeparator valuesBuilder = utilsFactory.createStringBuilderSeparator(",");
		for (final String field : fields) {
			if (substitutedFields != null && substitutedFields.contains(field)) {
				final String substitutionValue = substitutionValues.get(substitutedFields.indexOf(field));
				if (substitutionValue != null) {
					valuesBuilder.append(substitutionValue);
				}
			} else {
				valuesBuilder.append(parameterValue.getValue(field));
			}
		}

		request.append(valuesBuilder).append(")");

		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public WhereGenerator createUpdateRequest() {
		return createUpdateRequest(null, null);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public WhereGenerator createUpdateRequest(final List<String> substitutedFields,
			final List<String> substitutionValues) {
		request = new StringBuilder(100)
				.append("UPDATE ").append(table)
				.append(" SET ");

		final StringBuilderSeparator valuesBuilder = utilsFactory.createStringBuilderSeparator(",");
		for (final String field : fields) {
			if (substitutedFields != null && substitutedFields.contains(field)) {
				final String substitutionValue = substitutionValues.get(substitutedFields.indexOf(field));
				if (substitutionValue != null) {
					valuesBuilder.append(field + "=" + substitutionValue);
				}
			} else {
				valuesBuilder.append(field + "=" + parameterValue.getValue(field));
			}
		}

		request.append(valuesBuilder);

		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public WhereGenerator createDeleteRequest() {
		request = new StringBuilder(40).append("DELETE FROM ").append(table);
		return this;
	}

	@Override
	public WhereClause where() {
		whereClause = new WhereClauseImpl(this);
		return whereClause;
	}

	@Override
	public WhereClause where(final Operator operator) {
		whereClause = new WhereClauseImpl(this, operator);
		return whereClause;
	}

	@Override
	public WhereGenerator groupBy(final String field) {
		groupFields.add(field);
		return this;
	}

	@Override
	public WhereGenerator groupBy(final Collection<String> fields) {
		groupFields.addAll(fields);
		return this;
	}

	@Override
	public WhereGenerator orderBy(final String field) {
		return orderBy(field, Order.ASCENDING);
	}

	@Override
	public WhereGenerator orderBy(final String field, final Order order) {
		orderFields.add(field + " " + order.getOrderKeyword());
		return this;
	}

	@Override
	public String toString() {
		if (whereClause != null) {
			request.append(" WHERE ").append(whereClause);
		}
		if (CollectionUtils.isNotEmpty(groupFields)) {
			final StringBuilderSeparator stringBuilderSeparator = utilsFactory.createStringBuilderSeparator(",");
			request.append(" GROUP BY ").append(stringBuilderSeparator.append(groupFields));
		}
		if (CollectionUtils.isNotEmpty(orderFields)) {
			final StringBuilderSeparator stringBuilderSeparator = utilsFactory.createStringBuilderSeparator(",");
			request.append(" ORDER BY ").append(stringBuilderSeparator.append(orderFields));
		}

		return request.toString();
	}
}
