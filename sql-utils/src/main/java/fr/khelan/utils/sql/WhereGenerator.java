package fr.khelan.utils.sql;

import java.util.Collection;

import fr.khelan.utils.sql.type.Operator;
import fr.khelan.utils.sql.type.Order;

/**
 * Interface to generate Where, Group by and Order by blocks.
 */
public interface WhereGenerator {
	/**
	 * Generates a Where block for a single field.
	 *
	 * @return The {@link WhereClause} created.
	 */
	WhereClause where();

	/**
	 * Generates a Where block for many clauses.
	 *
	 * @param operator
	 * 		The operator between clauses.
	 *
	 * @return The {@link WhereClause} created.
	 */
	WhereClause where(final Operator operator);

	/**
	 * Add a field to the group by block.
	 *
	 * @param field
	 * 		The field to add.
	 *
	 * @return The {@link WhereGenerator} itself.
	 */
	WhereGenerator groupBy(final String field);

	/**
	 * Add a list of fields to the group by block.
	 *
	 * @param field
	 * 		The list of fields.
	 *
	 * @return The {@link WhereGenerator} itself.
	 */
	WhereGenerator groupBy(final Collection<String> field);

	/**
	 * Add a field to the order by block.
	 *
	 * @param field
	 * 		The field to add.
	 *
	 * @return The {@link WhereGenerator} itself.
	 */
	WhereGenerator orderBy(final String field);

	/**
	 * Add a field to the order by block, with the specified order.
	 *
	 * @param field
	 * 		The field to add.
	 * @param order
	 * 		The order for the field.
	 *
	 * @return The {@link WhereGenerator} itself.
	 */
	WhereGenerator orderBy(final String field, Order order);

	/**
	 * Creates a request from the collected data.
	 *
	 * @return The String request.
	 */
	@Override
	String toString();
}
