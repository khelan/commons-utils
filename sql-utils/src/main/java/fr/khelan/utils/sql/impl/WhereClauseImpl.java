package fr.khelan.utils.sql.impl;

import java.util.ArrayList;
import java.util.List;

import fr.khelan.commons.utils.StringBuilderSeparator;
import fr.khelan.commons.utils.UtilsFactory;
import fr.khelan.utils.sql.ParameterValue;
import fr.khelan.utils.sql.WhereClause;
import fr.khelan.utils.sql.WhereGenerator;
import fr.khelan.utils.sql.type.Comparator;
import fr.khelan.utils.sql.type.Operator;

public class WhereClauseImpl implements WhereClause {
	private final UtilsFactory utilsFactory = new UtilsFactory();
	private final ParameterValue parameterValue = new NamedParameterValue();
	private List<WhereClause> whereClauses;
	private Operator operator;
	private String clause;
	private WhereGenerator whereGenerator;
	private WhereClause parentClause;

	protected WhereClauseImpl(final WhereGenerator whereGenerator) {
		this.whereGenerator = whereGenerator;
	}

	protected WhereClauseImpl(final WhereGenerator whereGenerator, final Operator operator) {
		this.whereGenerator = whereGenerator;
		this.operator = operator;
		whereClauses = new ArrayList<>();
	}

	protected WhereClauseImpl(final WhereClause whereClause) {
		parentClause = whereClause;
	}

	protected WhereClauseImpl(final WhereClause whereClause, final Operator operator) {
		parentClause = whereClause;
		this.operator = operator;
		whereClauses = new ArrayList<>();
	}

	private WhereClause setFieldClause(final String key) {
		return setFieldClause(key, parameterValue.getValue(key));
	}

	private WhereClause setFieldClause(final String key, final String value) {
		return setFieldClause(key, Comparator.EQUAL, value);
	}

	private WhereClause setFieldClause(final String key, final Comparator comparator) {
		if (comparator.isValueRequired()) {
			return setFieldClause(key, comparator, parameterValue.getValue(key));
		} else {
			return setFieldClause(key, comparator, null);
		}
	}

	private WhereClause setFieldClause(final String key, final Comparator comparator, final String value) {
		if (value != null) {
			clause = key + comparator.getComparatorKeyword() + (comparator.isValueInParenthesis() ? "(" : "") + value +
					 (comparator.isValueInParenthesis() ? ")" : "");
		} else {
			clause = key + comparator.getComparatorKeyword();
		}

		if (parentClause != null) {
			return parentClause;
		} else {
			return this;
		}
	}

	@Override
	public WhereClause startSubClause(final Operator operator) {
		if (this.operator == null || whereClauses == null) {
			throw new IllegalStateException("Clause initialization must define the operator.");
		}
		final WhereClause whereClause = new WhereClauseImpl(this, operator);
		whereClauses.add(whereClause);
		return whereClause;
	}

	@Override
	public WhereClause addFieldClause(final String field) {
		final WhereClauseImpl whereClause = addClause();
		whereClause.setFieldClause(field);
		return this;
	}

	@Override
	public WhereClause addFieldClause(final String field, final Comparator comparator) {
		final WhereClauseImpl whereClause = addClause();
		whereClause.setFieldClause(field, comparator);
		return this;
	}

	@Override
	public WhereClause addFieldClause(final String field, final String value) {
		final WhereClauseImpl whereClause = addClause();
		whereClause.setFieldClause(field, value);
		return this;
	}

	@Override
	public WhereClause addFieldClause(final String key, final Comparator comparator, final String value) {
		final WhereClauseImpl whereClause = addClause();
		whereClause.setFieldClause(key, comparator, value);
		return this;
	}

	@Override
	public WhereClause endSubClause() {
		WhereClause parent;
		if (parentClause != null) {
			parent = parentClause;
		} else {
			parent = this;
		}
		return parent;
	}

	@Override
	public WhereGenerator endWhere() {
		if (whereGenerator == null) {
			throw new IllegalStateException("There is no parent query generator. Please use endSubClause instead.");
		}
		return whereGenerator;
	}

	private WhereClauseImpl addClause() {
		WhereClauseImpl whereClause;
		if (operator == null) {
			whereClause = this;
		} else {
			whereClause = new WhereClauseImpl(this);
			whereClauses.add(whereClause);
		}
		return whereClause;
	}

	@Override
	public String toString() {
		if (operator != null && whereClauses != null) {
			final StringBuilderSeparator stringBuilderSeparator = utilsFactory.createStringBuilderSeparator(
					" " + operator.getOperatorKeyword() + " ");
			for (final WhereClause whereClause : whereClauses) {
				stringBuilderSeparator.append(whereClause.toString());
			}

			clause = stringBuilderSeparator.toString();
			if (parentClause != null) {
				clause = "(" + clause + ")";
			}
		}

		return clause;
	}
}
