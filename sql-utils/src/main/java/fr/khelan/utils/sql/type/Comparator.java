package fr.khelan.utils.sql.type;

/** Enumeration of WHERE symbols for query. */
public enum Comparator {
	/** Equals symbol */
	EQUAL("=", true, false),
	/** Different symbol */
	DIFFERENT("<>", true, false),
	/** Greater than symbol */
	GREATER(">", true, false),
	/** Greater or equals than symbol */
	GREATER_OR_EQUAL(">=", true, false),
	/** Lower than symbol */
	LOWER("<", true, false),
	/** Lower or equals than symbol */
	LOWER_OR_EQUAL("<=", true, false),
	/** Is null symbol */
	NULL(" IS NULL", false, false),
	/** Is not null symbol */
	NOT_NULL(" IS NOT NULL", false, false),
	/** Is like symbol */
	LIKE(" LIKE ", true, false),
	/** Is in symbol */
	IN(" IN ", true, true);

	private final String comparatorKeyword;

	private final boolean valueRequired;

	private final boolean valueInParenthesis;

	Comparator(final String comparatorKeyword, final boolean valueRequired, final boolean valueInParenthesis) {
		this.comparatorKeyword = comparatorKeyword;
		this.valueRequired = valueRequired;
		this.valueInParenthesis = valueInParenthesis;
	}

	/**
	 * @return the comparator
	 */
	public String getComparatorKeyword() {
		return comparatorKeyword;
	}

	/**
	 * @return the valueRequired
	 */
	public boolean isValueRequired() {
		return valueRequired;
	}

	/**
	 * @return the valueInParenthesis
	 */
	public boolean isValueInParenthesis() {
		return valueInParenthesis;
	}
}
