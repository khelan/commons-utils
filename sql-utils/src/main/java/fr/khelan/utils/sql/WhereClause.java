package fr.khelan.utils.sql;

import fr.khelan.utils.sql.type.Comparator;
import fr.khelan.utils.sql.type.Operator;

/**
 * Interface to generate Where clauses.
 */
public interface WhereClause {
	/**
	 * Creates a sub clause block.
	 *
	 * @param operator
	 * 		The operator between sub clauses.
	 *
	 * @return The new {@link WhereClause}.
	 */
	WhereClause startSubClause(final Operator operator);

	/**
	 * Add a field to the clause.
	 *
	 * @param field
	 * 		The field.
	 *
	 * @return The {@link WhereClause} itself.
	 */
	WhereClause addFieldClause(final String field);

	/**
	 * Add a field to the clause.
	 *
	 * @param field
	 * 		The field.
	 * @param comparator
	 * 		The comparator between the field and its value.
	 *
	 * @return The {@link WhereClause} itself.
	 */
	WhereClause addFieldClause(final String field, final Comparator comparator);

	/**
	 * Add a field to the clause.
	 *
	 * @param field
	 * 		The field.
	 * @param value
	 * 		The value of the field.
	 *
	 * @return The {@link WhereClause} itself.
	 */
	WhereClause addFieldClause(final String field, final String value);

	/**
	 * Add a field to the clause.
	 *
	 * @param field
	 * 		The field.
	 * @param comparator
	 * 		The comparator between the field and its value.
	 * @param value
	 * 		The value of the field.
	 *
	 * @return The {@link WhereClause} itself.
	 */
	WhereClause addFieldClause(final String field, final Comparator comparator, final String value);

	/**
	 * Ends the sub clause and continue with the parent clause.
	 *
	 * @return The parent {@link WhereClause}.
	 */
	WhereClause endSubClause();

	/**
	 * Ends the where clause and continue with the parent generator.
	 *
	 * @return The parent {@link WhereGenerator}.
	 */
	WhereGenerator endWhere();

	/**
	 * Creates the string representing this Where clause.
	 *
	 * @return The string of the {@link WhereClause}.
	 */
	@Override
	String toString();
}
