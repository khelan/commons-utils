package fr.khelan.utils.sql.type;

/** Enumeration of aggregation symbols for query. */
public enum Aggregation {
	/** Minimum */
	MINIMUM("MIN"),
	/** Maximum */
	MAXIMUM("MAX"),
	/** Average */
	AVERAGE("AVG"),
	/** Sum */
	SUM("SUM");

	private final String aggregationKeyword;

	Aggregation(final String aggregationKeyword) {
		this.aggregationKeyword = aggregationKeyword;
	}

	/**
	 * @return the aggregation keyword.
	 */
	public String getAggregationKeyword() {
		return aggregationKeyword;
	}
}
