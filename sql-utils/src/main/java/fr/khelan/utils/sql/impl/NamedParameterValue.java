package fr.khelan.utils.sql.impl;

import fr.khelan.utils.sql.ParameterValue;

/**
 * <p>
 * Named parameter value implementation.
 * </p>
 * <p>
 * Parameter values are like ":field".
 * </p>
 */
public class NamedParameterValue implements ParameterValue {

	/**
	 * {@inheritDoc}
	 * The parameter value is ":field".
	 */
	@Override
	public String getValue(final String field) {
		return ":" + field;
	}
}
