package fr.khelan.utils.sql.type;

/** Enumeration of ORDER symbols for query. */
public enum Order {
	/** Order ascending */
	ASCENDING("ASC"),
	/** Order descending */
	DESCENDING("DESC");

	private final String orderKeyword;

	Order(final String orderKeyword) {
		this.orderKeyword = orderKeyword;
	}

	/**
	 * @return the order
	 */
	public String getOrderKeyword() {
		return orderKeyword;
	}
}
