package fr.khelan.utils.sql;

import java.util.List;

import fr.khelan.utils.sql.type.Aggregation;

/**
 * Interface to generate queries.
 */
public interface QueryGenerator {
	/**
	 * Add a field to the Select part.
	 *
	 * @param field
	 * 		The field to add.
	 *
	 * @return The {@link QueryGenerator} itself.
	 */
	QueryGenerator addField(String field);

	/**
	 * Add an aggregated field to the Select part.
	 *
	 * @param field
	 * 		The field to add.
	 * @param aggregation
	 * 		The aggregation function to apply to the field.
	 *
	 * @return The {@link QueryGenerator} itself.
	 */
	QueryGenerator addField(String field, Aggregation aggregation);

	/**
	 * Add a value to the Select part.
	 *
	 * @param field
	 * 		The name of the value.
	 * @param value
	 * 		The value.
	 *
	 * @return The {@link QueryGenerator} itself.
	 */
	QueryGenerator addField(String field, String value);

	/**
	 * Creates a Select request.
	 *
	 * @return The {@link WhereGenerator} to manage Where clauses for the request.
	 */
	WhereGenerator createSelectRequest();

	/**
	 * Creates an Insert request.
	 *
	 * @return The {@link WhereGenerator} to manage Where clauses for the request.
	 */
	WhereGenerator createInsertRequest();

	/**
	 * Creates an Insert request.
	 *
	 * @param substitutedFields
	 * 		The fields for which a value is specified.
	 * @param substitutionValues
	 * 		The values of the substituted fields.
	 *
	 * @return The {@link WhereGenerator} to manage Where clauses for the request.
	 */
	WhereGenerator createInsertRequest(List<String> substitutedFields, List<String> substitutionValues);

	/**
	 * Creates an Update request.
	 *
	 * @return The {@link WhereGenerator} to manage Where clauses for the request.
	 */
	WhereGenerator createUpdateRequest();

	/**
	 * Creates an Update request.
	 *
	 * @param substitutedFields
	 * 		The fields for which a value is specified.
	 * @param substitutionValues
	 * 		The values of the substituted fields.
	 *
	 * @return The {@link WhereGenerator} to manage Where clauses for the request.
	 */
	WhereGenerator createUpdateRequest(List<String> substitutedFields, List<String> substitutionValues);

	/**
	 * Creates a Delete request.
	 *
	 * @return The {@link WhereGenerator} to manage Where clauses for the request.
	 */
	WhereGenerator createDeleteRequest();
}
