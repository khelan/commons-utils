package fr.khelan.utils.sql;

/**
 * Define how to manage parameter values.
 */
public interface ParameterValue {
	/**
	 * Get the parameter value for this field.
	 *
	 * @param field
	 * 		The field.
	 *
	 * @return The parameter value for this field.
	 */
	String getValue(final String field);
}
