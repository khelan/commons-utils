package fr.khelan.utils.sql.type;

/** Operators for Where clauses */
public enum Operator {
	/** AND SQL symbol */
	AND("AND"),
	/** OR SQL symbol */
	OR("OR");

	private final String operatorKeyword;

	Operator(final String operatorKeyword) {
		this.operatorKeyword = operatorKeyword;
	}

	/**
	 * @return the operator
	 */
	public String getOperatorKeyword() {
		return operatorKeyword;
	}
}
