package fr.khelan.utils.sql.type;

public enum RequestType {
	/** Select */
	SELECT("SELECT "),
	/** Insert */
	INSERT("INSERT "),
	/** Update */
	UPDATE("UPDATE "),
	/** Delete */
	DELETE("DELETE");

	private String value;

	private RequestType(final String value) {
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
}
